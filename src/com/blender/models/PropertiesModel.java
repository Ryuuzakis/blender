package com.blender.models;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Observable;

public class PropertiesModel extends Observable implements Serializable {
	private static final long serialVersionUID = 2541746481122137046L;
	
	/**
	 * Map contenant les propriétés de type boolean
	 */
	private HashMap<String, Boolean> booleanMap;
	
	/**
	 * Map contenant les propriétés de type couleur
	 */
	private HashMap<String, Color> colorsMap;
	
	/**
	 * Map contenant les propriétés de types integer
	 */
	private HashMap<String, Integer> integerMap;
	
	private final transient String SAVE_PATH = "ressources/save";
	private final transient String SAVE_FILE = "/save.ser";
	private final transient Color DEFAULT_OBJECT_COLOR = new Color(0, 102, 204);
	private final transient Color DEFAULT_LIGHT_COLOR = new Color(255, 255, 255);
	private final transient Color DEFAULT_BOX_COLOR = Color.ORANGE;
	private final transient Color DEFAULT_NORMAL_COLOR = Color.RED;
	private final transient Color DEFAULT_CENTER_COLOR = Color.GREEN;
	private final transient Color DEFAULT_SEGMENT_COLOR = Color.BLACK;

	public static final transient String O_VISIBLE = "Afficher l'objet";
	public static final transient String O_DRAW_DOTS = "Mode points";
	public static final transient String O_WIRED = "Mode fil de fer";
	public static final transient String O_PAINTER = "Algo Peintre";
	public static final transient String O_ZBUFFER = "ZBuffer";
	public static final transient String O_ZBUFFER_IMP = "ZBuffer amélioré";
	public static final transient String O_NORMALS = "Afficher normales";
	public static final transient String O_CENTER = "Afficher centre";
	public static final transient String O_DRAW_LIGHT = "Afficher source lumière";
	public static final transient String O_LIGHT_SUN = "Lumière mode soleil";
	public static final transient String O_ACTIVATE_LIGHT = "Activer lumière";
	public static final transient String O_COLOR = "Couleur objet";
	public static final transient String O_LIGHT_COLOR = "Couleur lumière";
	public static final transient String O_NORMALS_COLOR = "Couleur normales";
	public static final transient String O_CENTER_COLOR = "Couleur centre";
	public static final transient String O_SEGMENT_COLOR = "Couleur côtés";
	public static final transient String B_VISIBLE = "Afficher la boite";
	public static final transient String B_COLOR = "Couleur boite";
	public static final transient String A_VISIBLE = "Afficher l'axe";
	public static final transient String LIGHT_COLOR_INTENSITY = "Intensité lumineuse";
	public static final transient String OBJ_COLOR_INTENSITY = "Intensité de l'objet";
	
	private transient ObjectsModel objModel;

	public PropertiesModel(ObjectsModel objModel) {
		this.objModel = objModel;
		try {
			load();
		} catch (Exception e) {
			resetDefault();
		}
	}

	/**
	 * Initialise la map des couleurs avec les valeurs par défaut
	 */
	private void createColorsMap() {
		colorsMap = new HashMap<String, Color>();
		colorsMap.put(O_COLOR, DEFAULT_OBJECT_COLOR);
		colorsMap.put(O_LIGHT_COLOR, DEFAULT_LIGHT_COLOR);
		colorsMap.put(B_COLOR, DEFAULT_BOX_COLOR);
		colorsMap.put(O_NORMALS_COLOR, DEFAULT_NORMAL_COLOR);
		colorsMap.put(O_CENTER_COLOR, DEFAULT_CENTER_COLOR);
		colorsMap.put(O_SEGMENT_COLOR, DEFAULT_SEGMENT_COLOR);
	}

	/**
	 * Initialise la map des booleans avec les valeurs par défaut
	 */
	private void createBooleanMap() {
		booleanMap = new HashMap<String, Boolean>();
		booleanMap.put(O_VISIBLE, true);
		booleanMap.put(O_WIRED, false);
		booleanMap.put(O_PAINTER, true);
		booleanMap.put(O_ZBUFFER, false);
		booleanMap.put(B_VISIBLE, true);
		booleanMap.put(A_VISIBLE, true);
		booleanMap.put(O_NORMALS, false);
		booleanMap.put(O_DRAW_LIGHT, true);
		booleanMap.put(O_ACTIVATE_LIGHT, true);
		booleanMap.put(O_CENTER, true);
		booleanMap.put(O_ZBUFFER_IMP, false);
		booleanMap.put(O_LIGHT_SUN, false);
		booleanMap.put(O_DRAW_DOTS, false);
	}
	
	/**
	 * Initialise la map des int avec les valeurs par défaut
	 */
	private void createIntegerMap() {
		integerMap = new HashMap<String, Integer>();
		integerMap.put(LIGHT_COLOR_INTENSITY, 60);
		integerMap.put(OBJ_COLOR_INTENSITY, 40);
	}
	
	/**
	 * Permet de set une propriété integer avec la valeur value
	 * @param property propriété à modifier
	 * @param value nouvelle valeur de la propriété
	 */
	public void setProperty(String property, int value) {
		integerMap.put(property, value);
		save();
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.PROPERTY_CHANGED, property));
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Permet de set une propriété boolean avec la valeur value
	 * @param property propriété a modifier
	 * @param value nouvelle valeur de la propriété
	 */
	public void setProperty(String property, boolean value) {
		if (property.equals(O_ACTIVATE_LIGHT) && !value)
			setDrawingAlgorithm(O_PAINTER);
		else if (property.equals(O_LIGHT_SUN))
			setProperty(O_DRAW_LIGHT, !value);
		else if (property.equals(O_DRAW_DOTS) && value)
			setProperty(O_WIRED, !value);
		else if (property.equals(O_WIRED) && value)
			setProperty(O_DRAW_DOTS, !value);
		
		booleanMap.put(property, value);

		save();
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.PROPERTY_CHANGED, property));
		
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED, property));
	}

	/**
	 * Permet de set une propriété color avec la valeur color
	 * @param property propriété a modifier
	 * @param color nouvelle valeur de la propriété
	 */
	public void setProperty(String property, Color color) {
		colorsMap.put(property, color);
		save();
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Permet de récuperer la valeur de la propriété boolean property
	 * @param property propriété dont l'on souhaite la valeur
	 * @return la valeur de la propriété
	 */
	public boolean getProperty(String property) {
		return booleanMap.get(property);
	}

	/**
	 * Permet de récupérer la valeur de la propriété color property.
	 * @param property propriété dont l'on souhaite la valeur
	 * @param c paramètre color qui permet d'indiquer que l'on souhaite une propriété
	 * 			de type color. Sa valeur est quelconque.
	 * @return La couleur de la propriété
	 */
	public Color getProperty(String property, Color c) {
		return colorsMap.get(property);
	}
	
	/**
	 * Permet de récupérer la valeur de la propriété int property
	 * @param property propriété dont l'on souhaite la valeur
	 * @param i paramètre int qui permet d'indiquer que l'on souhaite une propriété
	 * 			de type int. Sa valeur est quelconque.
	 * @return La valeur de la propriété
	 */
	public int getProperty(String property, int i) {
		return integerMap.get(property);
	}

	/**
	 * Reset la couleur de la lumière
	 */
	public void resetLightColor() {
		setProperty(O_LIGHT_COLOR, DEFAULT_LIGHT_COLOR);
	}

	/**
	 * Reset la couleur de l'objet
	 */
	public void resetObjectColor() {
		setProperty(O_COLOR, DEFAULT_OBJECT_COLOR);
	}

	/**
	 * Reset la couleur de la boite
	 */
	public void resetBoxColor() {
		setProperty(B_COLOR, DEFAULT_BOX_COLOR);
	}

	/**
	 * Sauvegarde les propriétes actuelles
	 */
	public void save() {
		ObjectOutputStream out = null;
		File save = new File(SAVE_PATH);
		if (!save.exists())
			save.mkdirs();

		save = new File(save.getPath() + SAVE_FILE);
		try {
			out = new ObjectOutputStream(new FileOutputStream(save.getAbsolutePath()));
			out.writeObject(this);
		} catch (IOException e) {
		} finally {
			try {
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (IOException e) {}
		}
	}
	
	/**
	 * Charge les propriétes depuis un fichier
	 * @throws Exception
	 */
	private void load() throws Exception {
		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(new File(SAVE_PATH + SAVE_FILE)));
			PropertiesModel model = (PropertiesModel) in.readObject();
			this.booleanMap = model.booleanMap;
			this.colorsMap = model.colorsMap;
			this.integerMap = model.integerMap;
		} catch (Exception e) {
			throw e;
		} finally {
			if (in != null)
				in.close();
		}
	}

	/**
	 * Restaure les propriétes par defaut, puis les sauvegarde.
	 */
	public void resetDefault() {
		createBooleanMap();
		createColorsMap();
		createIntegerMap();
		
		save();
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.PROPERTIES_CHANGED));
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Set le nouvel algorithme de dessin choisi, et passe
	 * les autres à false
	 * @param property algo choisi
	 */
	public void setDrawingAlgorithm(String property) {
		boolean painter, zbuff, zbuffImproved;
		painter = property.equals(O_PAINTER);
		zbuff = property.equals(O_ZBUFFER);
		zbuffImproved = property.equals(O_ZBUFFER_IMP);
		
		if (painter != getProperty(O_PAINTER)) {
			if (objModel.getObject() != null)
				objModel.getObject().sortFaces();
			setProperty(O_PAINTER, painter);
		}
		
		if (zbuff != getProperty(O_ZBUFFER))
			setProperty(O_ZBUFFER, zbuff);
		
		if (zbuffImproved != getProperty(O_ZBUFFER_IMP))
			setProperty(O_ZBUFFER_IMP, zbuffImproved);
	}
}
