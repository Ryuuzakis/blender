package com.blender.models;

/**
 * Classe representant un message de notification entre le model et les vues
 *
 */
public class ModelNotification {
	/**
	 * Notification d'un message d'erreur
	 */
	public static final byte ERROR_MSG = 0;
	/**
	 * Notification lorsqu'un onglet est ajoute
	 */
	public static final byte TAB_ADDED = 1;
	/**
	 * Notification lorsqu'une caracteristique d'un objet est modifiee(position, taille...)
	 */
	public static final byte OBJECT_LF_MODIFIED = 2;
	/**
	 * Notification lorsqu'un onglet est retiré
	 */
	public static final byte TAB_REMOVED = 3;
	/**
	 * Notification lorsque l'on change d'outil
	 */
	public static final byte TOOL_CHANGED = 4;
	/**
	 * Notification lorsque l'on change le mode d'un outil (wired ou pas)
	 */
	public static final byte MODE_CHANGED = 5;
	/**
	 * Notification lorsque la recherche est change
	 */
	public static final byte SEARCH_CHANGED = 6;
	/**
	 * Notification indiquant que l'operation de gestion de fichier est terminée
	 */
	public static final byte DB_UPDATED = 7;
	/**
	 * Notification indiquant que l'on a change d'onglet
	 */
	public static final byte TAB_CHANGED = 9;
	/**
	 * Notification indiquant que l'on a cree un nouvel objet
	 */
	public static final byte OBJECT_CREATED = 10;
	/**
	 * Notification indiquant que le resultat de la recherche a change
	 */
	public static final byte SEARCH_RESULT_UPDATED = 11;
	/**
	 * Notification indiquant qu'une propriete a ete modifiee
	 */
	public static final byte PROPERTY_CHANGED = 12;
	/**
	 * Notification indiquant que plusieurs proprietes ont ete modifiees
	 */
	public static final byte PROPERTIES_CHANGED = 13;
	/**
	 * Notification indiquant que les previsualisations on été misent a jours
	 */
	public static final byte PREVIEW_CHANGED = 14;
	
	private Object data;
	private int type;
	private boolean treated;

	public ModelNotification(int type, Object data) {
		this.type = type;
		this.data = data;
		this.setTreated(false);
	}
	
	public ModelNotification(int type) {
		this(type, null);
	}

	/**
	 * Retourne les donnees associe a la notification
	 * @return Une instance d'un Object
	 */
	public Object getData() {
		return data;
	}
	
	/**
	 * Retourne le type de la notification
	 * Sert a differencier les messages
	 * @return Un entier correspondant au type
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * Retourne si la notification a deja été traité ou non (utile si
	 *  plusieurs vue sur le meme model)
	 * @return Vrai si la notification a été traité, faux sinon
	 */
	public boolean isTreated() {
		return treated;
	}

	/**
	 * Indique si l'evenement a deja été traité pour informer les autres vues
	 * Utile si l'evenement ne doit etre traité qu'une seul fois par une vue quelconque
	 * (par exemple : affichage d'un message d'erreur)
	 * @param treated Vrai pour indiquer que l'evenement est traité, faux sinon
	 */
	public void setTreated(boolean treated) {
		this.treated = treated;
	}
}
