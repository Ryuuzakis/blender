package com.blender.models;

import java.util.List;
import java.util.Observable;

import com.blender.utils.database.DBModel;
import com.blender.utils.io.FileHandler;

/**
 * Model permettant l'import/export, associe a FileHandlingView
 *
 */
public class FileHandlingModel extends Observable {

	/**
	 * Chemin du model a utiliser
	 */
	private String filePath;

	/**
	 * Nom des modeles
	 */
	private List<String> modelNames;

	public FileHandlingModel() {	}

	public void setModelName(List<String> modelNames) {
		this.modelNames = modelNames;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Methode qui realise l'import/export selon la valeur du parametre
	 * @param currentlyExporting true si l'on souhaite exporter, false si l'on souhaite importer.
	 * @param objModel objects model
	 */
	public void moveModel(boolean currentlyExporting, ObjectsModel objModel) {
		try {

			for (String modelName : modelNames) {
				if (currentlyExporting) {
					String sourcePath = DBModel.getPath(modelName);
					FileHandler.exportFile(filePath, FileHandler.getModel(sourcePath));
				} else
					FileHandler.importFile(filePath, modelName, null);
			}

			objModel.updateDB();
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.DB_UPDATED));
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.ERROR_MSG, e));
		}
		filePath = null;
		modelNames = null;
	}
}
