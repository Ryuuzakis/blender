package com.blender.models;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Observable;

import com.blender.utils.Tool;
import com.blender.utils.database.DBModel;
import com.blender.utils.database.DBUpdate;
import com.blender.utils.io.FileHandler;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.transformable.TransformableObject3D;

/**
 * Model representant les donnees des objets 3d
 *
 */
public class ObjectsModel extends Observable {
	/**
	 * Liste des objets crees
	 */
	private ArrayList<TransformableObject3D> objects;

	/**
	 * Indice de l'objet actuel dans la liste des objets
	 */
	private int showedObject;

	/**
	 * Outil actuel
	 */
	private Tool tool;

	/**
	 * Mode de l'outil : wired ou non
	 */
	private int toolMode;
	private int maxWidth, maxHeight;

	public static final int MODE_WIRE = 0;
	public static final int MODE_FACES = 1;

	/**
	 * Proprietes des objets
	 */
	private PropertiesModel properties;

	public ObjectsModel() {
		objects = new ArrayList<TransformableObject3D>();
		showedObject = 0;
		setTool(Tool.TRANSLATION);
		toolMode = MODE_FACES;
		properties = new PropertiesModel(this);
	}

	/**
	 * Ajoute un objet 3D a la liste
	 * @param name Le nom de l'objet a ajouter
	 */
	public void addObject() {
		try {
			objects.add(null);
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.TAB_ADDED));
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.ERROR_MSG, e));
		}
	}

	/**
	 * Definie l'objet a un certain index
	 * @param index L'index de l'objet
	 * @param name Le nom de l'objet
	 * @return Renvoie vrai si le changement d'objet a reussi, faux sinon
	 */
	public boolean setObject(int index, String name) {
		try {
			String path = DBModel.getPath(name);
			TransformableObject3D obj = FileHandler.getModel(path);
			obj.setProperties(properties);
			if (obj != null) {
				objects.set(index, obj);
				obj.setModelName(name);
				setChanged();
				notifyObservers(new ModelNotification(ModelNotification.OBJECT_CREATED, new Object[]{index, name}));
			}
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.ERROR_MSG, e));
			return false;
		}
		return true;
	}

	/**
	 * Change l'objet affiche
	 * @param i L'identifiant du nouvelle objet a afficher
	 */
	public void setShowedObject(int i) {
		if (i < 0 || i >= objects.size()) return;
		showedObject = i;
		if (objects.get(i) != null)
			objects.get(i).setProperties(properties);
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.TAB_CHANGED, objects.get(i)));
	}

	/**
	 * Supprime un objet de la liste
	 * @param i L'identifiant de l'objet a supprimer
	 */
	public void removeObject(int i) {
		objects.remove(i);
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.TAB_REMOVED, i));
	}

	/**
	 * Deplace l'objet en incrementant les coordonnees
	 * @param x La valeur a ajouter au x de l'objet
	 * @param y La valeur a ajouter au y de l'objet
	 * @param z La valeur a ajouter au z de l'objet
	 */
	public void move(float x, float y, float z) {
		getObject().move(x, y, z);
		getObject().getEnglobingBox().move(x, y, z);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Deplace l'objet au coordonees specifie
	 * @param x La nouvelle position x de l'objet
	 * @param y La nouvelle position y de l'objet
	 * @param z La nouvelle position z de l'objet
	 */
	public void moveTo(float x, float y, float z) {
		Point pt = getObject().getCenter();
		if (pt.getX() == x && pt.getY() == y && pt.getZ() == z) return;

		getObject().moveTo(x, y, z);
		getObject().getEnglobingBox().moveTo(x, y, z);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Effectue une rotation de l'objet
	 * Les valeurs sont en degrees
	 * @param ax Angle de rotation de l'axe x
	 * @param ay Angle de rotation de l'axe y
	 * @param az Angle de rotation de l'axe z
	 */
	public void rotate(float ax, float ay, float az) {
		getObject().rotate(ax, ay, az);
		getObject().getEnglobingBox().rotate(ax, ay, az);

		// On notifie la vue pour qu'elle s'actualise
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Met l'objet dans un angle
	 * Les valeurs sont en degrees
	 * @param ax Angle de l'axe x
	 * @param ay Angle de l'axe y
	 * @param az Angle de l'axe z
	 */
	public void rotateTo(float ax, float ay, float az) {
		getObject().rotateTo(ax, ay, az);
		getObject().getEnglobingBox().rotateTo(ax, ay, az);

		// On notifie la vue pour qu'elle s'actualise
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Redimensionne l'objet
	 * Si la valeur est inferieur a 1, l'objet est retreci
	 * Si la valeur est superieur a 1, l'objet est agrandi
	 * Si la valeur est egal a 1, l'objet reste inchange
	 * @param sx Le coefficient de redimensionnement en x
	 * @param sy Le coefficient de redimensionnement en y
	 * @param sz Le coefficient de redimensionnement en z
	 */
	public void scale(float sx, float sy, float sz) {
		getObject().scale(sx, sy, sz);
		getObject().getEnglobingBox().scale(sx, sy, sz);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Redimensionne l'objet a une dimension precise
	 * @param sx Le facteur de redimensionnement en x
	 * @param sy Le facteur de redimensionnement en y
	 * @param sz Le facteur de redimensionnement en z
	 */
	public void scaleTo(float sx, float sy, float sz) {
		if (sx == getObject().getScale().getX() && sy == getObject().getScale().getY()
				&& sz == getObject().getScale().getZ()) {
			return;
		}
		getObject().scaleTo(sx, sy, sz);
		getObject().getEnglobingBox().scaleTo(sx, sy, sz);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Deplace le centre de l'objet
	 * @param cx Le nouveau centre en x
	 * @param cy Le nouveau centre en y
	 * @param cz Le nouveau centre en z
	 */
	public void moveCenter(float cx, float cy, float cz) {
		getObject().moveCenter(cx, cy, cz);
		getObject().getEnglobingBox().moveCenter(cx, cy, cz);

		setChanged();
		notifyObservers();
	}

	/**
	 * Renvoie l'objet 3D du model
	 * @return Une instance d'un objet 3D
	 */
	public TransformableObject3D getObject() {
		if (showedObject < 0 || showedObject >= objects.size()) return null;
		return objects.get(showedObject);
	}

	/**
	 * Renvoie l'objet 3D associe a un index
	 * @param i L'index associe a l'objet
	 * @return Une instance d'un objet 3D
	 */
	public TransformableObject3D getObject(int i) {
		return objects.get(i);
	}

	/**
	 * Renvoie l'outil actuelement selectionne
	 * @return
	 */
	public Tool getTool() {
		return tool;
	}

	/**
	 * Change l'outil du logiciel
	 * @param tool L'identifiant du nouvelle outil
	 */
	public void setTool(Tool tool) {
		this.tool = tool;
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.TOOL_CHANGED, tool));
	}

	/**
	 * Change le mode dans lequel l'objet est (Fil de fer, avec des faces...)
	 * @param mode Le nouveau mode
	 */
	public void setMode(int mode) {
		this.toolMode = mode;
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.MODE_CHANGED, mode));
	}

	/**
	 * Adapte le model a une dimension et le centre
	 * @param width Largeur de la dimension
	 * @param height Hauteur de la dimension
	 */
	public void adaptTo(int width, int height) {
		getObject().getEnglobingBox().adaptTo(width, height);
		getObject().adaptTo(width, height);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	public int getMode() {
		return toolMode;
	}

	public void setMaxSize(int width, int height) {
		this.maxWidth = width;
		this.maxHeight = height;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public boolean isUsingTool(Tool tool) {
		return this.tool.equals(tool);
	}

	/**
	 * Change la couleur de l'objet
	 * @param color La nouvelle couleur
	 */
	public void setObjectColor(Color color) {
		getProperties().setProperty(PropertiesModel.O_COLOR, color);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Deplace la source de lumiere
	 * @param px La nouvelle position de la lumiere en x
	 * @param py La nouvelle position de la lumiere en y
	 * @param pz La nouvelle position de la lumiere en z
	 */
	public void moveLightPosition(float px, float py, float pz) {
		if (!properties.getProperty(PropertiesModel.O_LIGHT_SUN)) {
			getObject().moveLight(px, py, pz);
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
		}
	}

	/**
	 * Ramene a la configuration par defaut la position de la lumiere, sa couleur et la couleur de l'objet
	 */
	public void resetLight() {
		getObject().resetLightPosition();
		getProperties().resetObjectColor();
		getProperties().resetLightColor();

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	/**
	 * Change la couleur de la lumiere
	 * @param color la nouvelle couleur
	 */
	public void setLightColor(Color color) {
		getProperties().setProperty(PropertiesModel.O_LIGHT_COLOR, color);

		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.OBJECT_LF_MODIFIED));
	}

	public PropertiesModel getProperties() {
		return properties;
	}

	/**
	 * Indique aux observateurs de ce model que la db a ete mise a jour
	 */
	public void updateDB() {
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.DB_UPDATED));
	}

	/**
	 * Creer un objet contenant toutesl informations sur un model
	 * @param objectName Le nom du model
	 * @return Une instance de ObjectInformations
	 */
	public ObjectInformations getObjectInformations(String objectName) {
		return new ObjectInformations(objectName);
	}

	/**
	 * Recharge les previsualisations des modeles
	 */
	public void reloadPreviews() {
		DBUpdate.generatePreviews(this);
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.PREVIEW_CHANGED));
	}

}
