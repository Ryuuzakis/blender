package com.blender.models;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.blender.utils.database.DBModel;
import com.blender.utils.database.DBUpdate;

/**
 * Objet representant toutes les informations stockée sur un model
 *
 */
public class ObjectInformations {
	private String previewPath, modelName, author, keywords, shapes, description;
	private Date date;
	private DateFormat viewDateFmt = new SimpleDateFormat("dd/MM/yyyy");
	private DateFormat dbDateFmt = new SimpleDateFormat("yyyyMMdd");
	private boolean exist;
	
	/**
	 * Creer l'objet contenant les informations d'un model
	 * @param modelName Le nom du model sur lequel on devra recuperer les informations dans la BDD
	 */
	public ObjectInformations(String modelName) {
		this.modelName = modelName;
		this.author = "";
		this.date = getTodaysDate();
		this.description = "";
		this.shapes = "Inconnu";
		this.keywords = modelName;
		try {
			DBModel.getPath(modelName);
			exist = true;
		} catch (Exception e1) {
			exist = false;
		}
		if (exist) {
			try {
				previewPath = DBModel.getPreview(modelName);
				author = DBModel.getMetaData(modelName, "author");
				String dateStr = DBModel.getMetaData(modelName, "date");
				if (dateStr != null) date = dbDateFmt.parse(dateStr);
				description = DBModel.getMetaData(modelName, "comment");
				shapes = "";
				for (String shape : DBModel.getIdentifications(modelName, "formes")) {
					shapes += shape + " ";
				}
				keywords = "";
				for (String keyword : DBModel.getIdentifications(modelName, "mots_cles")) {
					keywords += keyword + " ";
				}
			} catch (Exception e) {}
		}
	}
	
	public void saveToDB() throws Exception {
		DBUpdate.setMetaData(modelName, "author", author);
		DBUpdate.setMetaData(modelName, "date", dbDateFmt.format(date));
		DBUpdate.setMetaData(modelName, "comment", description);
		
		DBUpdate.clearIdentifications(modelName, "formes");
		for (String word : shapes.split(" ")) {
			DBUpdate.setIdentification(modelName, "formes", word);
		}
		
		DBUpdate.clearIdentifications(modelName, "mots_cles");
		for (String word : keywords.split(" ")) {
			DBUpdate.setIdentification(modelName, "mots_cles", word);
		}
	}
	
	private Date getTodaysDate() {
		return new Date();
	}
	
	/**
	 * @return Le nom du model
	 */
	public String getName() {
		return modelName;
	}

	/**
	 * @return Le chemin d'acces vers l'image de previsualisation 
	 */
	public String getPreviewPath() {
		return previewPath;
	}

	public String getAuthor() {
		return author;
	}

	public String getDate() {
		return viewDateFmt.format(date);
	}

	public String getDescription() {
		return description;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}

	public void setDate(String date) throws ParseException {
		this.date = viewDateFmt.parse(date);
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getShape() {
		return this.shapes;
	}
	
	public void setShape(String shape) {
		this.shapes = shape;
	}
	
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public String getKeywords() {
		return this.keywords;
	}
	
	public boolean isNewModel() {
		return !exist;
	}
	
}
