package com.blender.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map.Entry;
import java.util.Observable;

import com.blender.utils.AdvancedSearchParser;
import com.blender.utils.database.DBModel;
import com.blender.utils.database.DBSearch;

/**
 * Model representant les recherches d'objet dans la BDD
 *
 */
public class SearchModel extends Observable {
	
	private DateFormat searchDateFmt = new SimpleDateFormat("dd/MM/yyyy");
	private DateFormat dbDateFmt = new SimpleDateFormat("yyyyMMdd");
	private List<String> results;
	private String search;
	
	public SearchModel() {
		search = "";
		try {
			results = DBModel.getModelsNames();
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.ERROR_MSG, e));
		}
	}
	
	/**
	 * Effectue une recherche pour un nom d'objet
	 * @param name Le nom d'objet de la recherche
	 */
	public void search(String name) {
		try {
			AdvancedSearchParser parser = new AdvancedSearchParser(name);
			DBSearch search = new DBSearch();
			
			for (Entry<String, String> entry : parser.getFields().entrySet()) {
				String value = entry.getValue();
				if (entry.getKey().equals("date")) {
					try {
						value = dbDateFmt.format(searchDateFmt.parse(value));
					} catch (Exception e) {}
				}
				search.withAttribute(entry.getKey(), "%"+value+"%");
			}
			search.withName("%"+parser.getName()+"%");
			search.orderBy(parser.getSortField(), parser.isSortedInAsc());
			
			results = search.getResult();
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ModelNotification(ModelNotification.ERROR_MSG, e));
		}
		search = name;
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.SEARCH_RESULT_UPDATED));
	}
	
	/**
	 * Renvoie les resultats de la recherche
	 * @return La liste avec les noms des modeles trouv�s
	 */
	public List<String> getResults() {
		return results;
	}
	
	/**
	 * Renvoie la recherche analyse
	 * @return Une instance de l'objet qui analyse la recherche
	 */
	public AdvancedSearchParser getParsedSearch() {
		return new AdvancedSearchParser(search);
	}
	
	/**
	 * Modifie la valeur de la recherche
	 * @param search La nouvelle chaine contenant la recherche
	 */
	public void setSearch(String search) {
		this.search = search;
		setChanged();
		notifyObservers(new ModelNotification(ModelNotification.SEARCH_CHANGED, search));
	}
}
