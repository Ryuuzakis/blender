package com.blender.test.utils;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blender.utils.AdvancedSearchParser;

public class AdvancedSearchParserTest {

	@Test
	public void testAdvancedSearchParser() {
		//On teste si les champs sont bien reconnus
		
		String[] fields = AdvancedSearchParser.FIELDS_NAME;
		String[] words = new String[] {"tretgre", "feqzscfez", "fazfze", "efzfezpfze", "afpaezfpa", "fafpaezf,e", "zeogizrjepg,jzrespz"};
		
		for (int i = 0; i < 200; i++) {
			boolean takeField = Math.random() > 0.5 ? true : false;
			int idx = (int) (Math.random() * words.length);
			int idx2 = 0;
			String s = "";
			if (takeField) {
				idx2 = (int) (Math.random() * fields.length);
				s += fields[idx2] + "=";
			}
			s += words[idx];
			AdvancedSearchParser parser = new AdvancedSearchParser(s);
			if (takeField) {
				if (fields[idx2].equals(fields[4]))
					assertEquals(words[idx], parser.getSortField());
				else
					assertEquals(words[idx], parser.getFields().get(fields[idx2]));
				assertEquals("", parser.getName());
			} else
				assertEquals(words[idx], parser.getName());
		}
	}
	
	@Test
	public void testAdvancedSearchParser2() {
		//On teste des recherches bizarres
		String[] fields = AdvancedSearchParser.FIELDS_NAME;
		
		String s = "feznofeoz = fezfnoeznfez";
		AdvancedSearchParser parser = new AdvancedSearchParser(s);
		assertEquals("Test 1", s, parser.getName());
		
		s = "feznofeoz = fezfnoeznfez=fezfezfe = fezoifnezo,fez";
		parser = new AdvancedSearchParser(s);
		assertEquals("Test 2", null, parser.getFields().get("fezfnoeznfez"));
		assertEquals("Test 3", s, parser.getName());
		
		s = "feznofeoz = " + fields[2] + "=fezfezfe fezfze";
		parser = new AdvancedSearchParser(s);
		assertEquals("Test 4", "fezfezfe fezfze", parser.getFields().get(fields[2]));
		assertEquals("Test 5", "feznofeoz =", parser.getName());
		
	}

}
