package com.blender.test.maths;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blender.utils.maths.Matrix;

public class MatrixTest {

	@Test
	public void testMatrixConstructorAndDimensions() {
		Matrix m1 = new Matrix(5, 4);
		Matrix m2 = new Matrix(new float[][]{{0, 0, 0}});
		
		assertEquals(5, m1.getNbColumns());
		assertEquals(4, m1.getNbRows());
		
		assertEquals(3, m2.getNbColumns());
		assertEquals(1, m2.getNbRows());


	}
	
	@Test(expected=NegativeArraySizeException.class)
	public void testMatrixConstructor1() {
		new Matrix(-1, -1);
	}
	
	@Test(expected=NegativeArraySizeException.class)
	public void testMatrixConstructor2() {
		new Matrix(-42, 0);
	}
	
	@Test(expected=NegativeArraySizeException.class)
	public void testMatrixConstructor3() {
		new Matrix(0, -50);
	}
	
	@Test
	public void testMatrixIdentity() {
		Matrix m1 = new Matrix(2, 2).getIdentity();
		
		assertEquals(1, (int)m1.get(0, 0));
		assertEquals(0, (int)m1.get(1, 0));
		assertEquals(0, (int)m1.get(0, 1));
		assertEquals(1, (int)m1.get(1, 1));
	}
	
	
	@Test
	public void testMatrixToString() {
		Matrix m1 = new Matrix(new float[][]{{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9}});
		
		String res = "1.0\t2.0\t3.0\t\n4.0\t5.0\t6.0\t\n7.0\t8.0\t9.0\t\n";
		
		assertEquals(res, m1.toString());
	}
	
	@Test
	public void testMatrixMultiplication() throws Exception {
		Matrix m1 = new Matrix(new float[][]{{1, 2, 3},
				{4, 5, 6},
				{7, 8, 9}});
		
		Matrix m2 = new Matrix(new float[][]{{10, 11, 12},
				{13, 14, 15},
				{16, 17, 18}});
		
		
		String res = "84.0\t90.0\t96.0\t\n201.0\t216.0\t231.0\t\n318.0\t342.0\t366.0\t\n";
		
		assertEquals(res, m1.multiplyBy(m2).toString());
	}
	
	@Test(expected=Exception.class)
	public void testMatrixMultiplicationException1() throws Exception {
		Matrix m1 = new Matrix(new float[][]{{1, 2},
				{4, 5},
				{7, 8}});
		
		Matrix m2 = new Matrix(new float[][]{{10, 11, 12},
				{13, 14, 15},
				{16, 17, 18}});
		
		m1.multiplyBy(m2);
	}
	
	@Test(expected=Exception.class)
	public void testMatrixMultiplicationException2() throws Exception {
		Matrix m1 = new Matrix(new float[][]{{1, 2},
				{4, 5},
				{7, 8}});
		
		Matrix m2 = new Matrix(new float[][]{{10, 11, 12}});
		
		m1.multiplyBy(m2);
	}
}
