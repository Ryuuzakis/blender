package com.blender.test.maths;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blender.utils.maths.Point;

public class PointTest {

	@Test
	public void testEqualsObject() {
		Point p1 = new Point(5, -5, 42);
		Point p2 = new Point(42, 80, -75);
		
		assertFalse(p1.equals(p2));
		
		p2.setX(p1.getX());
		p2.setY(p1.getY());
		p2.setZ(p1.getZ());
		assertTrue(p1.equals(p2));
	}

}
