package com.blender.test.maths;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Vector;

public class SegmentTest {

	@Test
	public void testHaveOnePointInCommon() {
		ArrayList<Point> points = new ArrayList<Point>();
		for (int i = 0; i < 10; i++) {
			points.add(new Point((float) Math.random() * 100, (float) Math.random() * 100,  (float) Math.random() * 100));
		}

		for (int i = 0; i < 500; i++) {
			int idx1 = (int) (Math.random() * 10), idx2 = (int) (Math.random() * 10), 
					idx3 = (int) (Math.random() * 10), idx4 = (int) (Math.random() * 10);
			Segment s1 = new Segment(points.get(idx1), points.get(idx2));
			Segment s2 = new Segment(points.get(idx3), points.get(idx4));
			if (s1.getP1().equals(s2.getP1()) || s1.getP1().equals(s2.getP2())
					|| s1.getP2().equals(s2.getP1()) || s1.getP2().equals(s2.getP2()))
				if (s1.equals(s2))
					assertFalse(Segment.haveOneAndOnlyPointInCommon(s1, s2));
				else
					assertTrue(Segment.haveOneAndOnlyPointInCommon(s1, s2));
			else
				assertFalse(Segment.haveOneAndOnlyPointInCommon(s1, s2));
		}
	}

	@Test
	public void testGetMiddlePoint() {
		for (int i = 0; i < 200; i++) {
			Point[] points = new Point[2];
			for (int j = 0; j < 2; j++) {
				byte sign = Math.random() > 0.5 ? (byte) -1 : (byte) 1;
				float x = (float) (Math.random() * 150) * sign;

				sign = Math.random() > 0.5 ? (byte) -1 : (byte) 1;
				float y = (float) (Math.random() * 150) * sign;

				sign = Math.random() > 0.5 ? (byte) -1 : (byte) 1;
				float z = (float) (Math.random() * 150) * sign;
				points[j] = new Point(x, y, z);
			}
			
			Point middle = Segment.getMiddlePoint(points[0], points[1]);
			
			double norm = new Vector(points[0], points[1]).getNorm(), halfNorm1 = new Vector(points[0], middle).getNorm(),
					halfNorm2 = new Vector(points[1], middle).getNorm();
			
			assertEquals(norm / 2, halfNorm1, 0.1);
			assertEquals(norm / 2, halfNorm2, 0.1);
		}
	}

}
