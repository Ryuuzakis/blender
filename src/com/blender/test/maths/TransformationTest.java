package com.blender.test.maths;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blender.utils.maths.Matrix;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Transformation;

public class TransformationTest {

	@Test
	public void testTransformationConstructor() {
		Transformation t1 = new Transformation();
		
		String res = "1.0\t0.0\t0.0\t0.0\t\n"
				   + "0.0\t1.0\t0.0\t0.0\t\n"
				   + "0.0\t0.0\t1.0\t0.0\t\n"
				   + "0.0\t0.0\t0.0\t1.0\t\n";
		
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	@Test
	public void testTransformationMultiplyBy() {
		Transformation t1 = new Transformation();
		
		String res = "1.0\t0.0\t0.0\t0.0\t\n"
				   + "0.0\t1.0\t0.0\t0.0\t\n"
				   + "0.0\t0.0\t1.0\t0.0\t\n"
				   + "0.0\t0.0\t0.0\t1.0\t\n";
		
		t1.multiplyBy(new Matrix(4, 4).getIdentity());
		
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	@Test
	public void testTransformationTranslation() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.translate(13, 37, 42);
		
		String res = "34.0\t\n47.0\t\n44.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationTranslationNegative() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.translate(-13, -37, -42);
		
		String res = "8.0\t\n-27.0\t\n-40.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	@Test
	public void testTransformationTranslationNull() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.translate(0, 0, 0);
		
		String res = "21.0\t\n10.0\t\n2.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationScale() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.scale(13, 37, 42);
		
		String res = "273.0\t\n370.0\t\n84.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationScaleNegative() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.scale(-13, -37, -42);
		
		String res = "-273.0\t\n-370.0\t\n-84.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	@Test
	public void testTransformationScaleNull() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.scale(0, 0, 0);
		
		String res = "0.0\t\n0.0\t\n0.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationRotation() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.rotate(13, 37, 42);
		
		String res = "8.122381\t\n19.819466\t\n-9.285242\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationRotationNegative() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.rotate(-13, -37, -42);
		
		String res = "19.418907\t\n-3.768018\t\n12.397909\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	@Test
	public void testTransformationRotationNull() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 2);
		
		t1.rotate(0, 0, 0);
		
		String res = "21.0\t\n10.0\t\n2.0\t\n1.0\t\n";
		
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(res, t1.getMatrix().toString());
	}
	
	
	@Test
	public void testTransformationRotationCanceling() {
		Transformation t1 = new Transformation();
		Point pt = new Point(21, 10, 5);
		
		t1.rotate(50, 4, 28);
		t1.cancelRotation(-50, -4, -28);
		t1.multiplyBy(pt.getMatrix());
		
		assertEquals(pt.getX(), t1.getMatrix().get(0, 0), 0.5);
		assertEquals(pt.getY(), t1.getMatrix().get(0, 1), 0.5);
		assertEquals(pt.getZ(), t1.getMatrix().get(0, 2), 0.5);
	}
	
	
}
