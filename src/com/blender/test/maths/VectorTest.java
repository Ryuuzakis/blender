package com.blender.test.maths;

import static org.junit.Assert.*;

import org.junit.Test;

import com.blender.utils.maths.Point;
import com.blender.utils.maths.Vector;

public class VectorTest {

	@Test
	public void testScalarProduct() {
		Vector v1 = new Vector(1, 0, 0);
		Vector v2 = new Vector(0, 1, 0);
		assertEquals(Vector.scalarProduct(v1, v2), 0, 0.01);
		
		v2 = new Vector(5, 0, 0);
		assertEquals(Vector.scalarProduct(v1, v2), 5, 0.1);
		
		v2 = new Vector(-5, 0, 0);
		assertEquals(Vector.scalarProduct(v1, v2), -5, 0.1);
		
	}

	@Test
	public void testGetAngleBetween() {
		Vector v1 = new Vector(1, 0, 0);
		Vector v2 = new Vector(0, 1, 0);
		assertEquals(Math.PI / 2, Vector.getAngleBetween(v1, v2), 0.1);
		
		v2 = new Vector(-1, 0, 0);
		assertEquals(Math.PI, Vector.getAngleBetween(v1, v2), 0.1);
		
		v2 = new Vector(5, 0, 0);
		assertEquals(Vector.getAngleBetween(v1, v2), 0, 0.1);
		
		v2 = new Vector(1, 1, 0);
		assertEquals(Vector.getAngleBetween(v1, v2), Math.PI / 4, 0.1);
		
		for (int i = 0; i < 100; i++) {
			v1 = new Vector( (float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200));
			v2 = new Vector( (float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200));
			double angle = Vector.getAngleBetween(v1, v2);
			assertTrue(angle <= Math.PI && angle >= -1 * Math.PI);
		}
	}

	@Test
	public void testGetNormalizedVector() {
		for (int i = 0; i < 100; i++) {
			Vector v = new Vector( (float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200));
			assertEquals(1, v.getNormalizedVector().getNorm(), 1);
		}
	}

	@Test
	public void testGetNormal() {
		for (int i = 0; i < 100; i++) {
			Vector u = new Vector((float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200), 0);
			Vector v = new Vector((float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200), 0);
			Vector normal = Vector.getNormal(u, v);
			Point p1 = u.getTwoPointsOnVector()[0], p2 = v.getTwoPointsOnVector()[0];
			Vector planVector = new Vector(p1, p2);
			
			assertEquals(Math.PI / 2, Vector.getAngleBetween(normal, planVector), 0.1);
		}
		for (int i = 0; i < 100; i++) {
			Vector u = new Vector((float) (Math.random() * 200 - Math.random() * 200), 0,
					(float) (Math.random() * 200 - Math.random() * 200));
			Vector v = new Vector((float) (Math.random() * 200 - Math.random() * 200), 0,
					(float) (Math.random() * 200 - Math.random() * 200));
			Vector normal = Vector.getNormal(u, v);
			Point p1 = u.getTwoPointsOnVector()[0], p2 = v.getTwoPointsOnVector()[0];
			Vector planVector = new Vector(p1, p2);
			
			assertEquals(Math.PI / 2, Vector.getAngleBetween(normal, planVector), 0.1);
		}
		for (int i = 0; i < 100; i++) {
			Vector u = new Vector(0, (float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200));
			Vector v = new Vector(0, (float) (Math.random() * 200 - Math.random() * 200),
					(float) (Math.random() * 200 - Math.random() * 200));
			Vector normal = Vector.getNormal(u, v);
			Point p1 = u.getTwoPointsOnVector()[0], p2 = v.getTwoPointsOnVector()[0];
			Vector planVector = new Vector(p1, p2);
			
			assertEquals(Math.PI / 2, Vector.getAngleBetween(normal, planVector), 0.1);
		}
	}

}
