package com.blender.test.database;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.blender.utils.database.DBModel;
import com.blender.utils.database.DBUpdate;
import com.blender.utils.database.Database;

public class DBModelTest {

	@BeforeClass
	public static void testInit() {
		try {
			Database.initDb();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGetByName() throws Exception {
		assertEquals("tie", DBModel.getByName("tie", true).get(0));
		assertEquals(new ArrayList<String>(), DBModel.getByName("GABUZOMEU", false));
	}

	@Test
	public void testGetByKeyword() throws Exception {
		DBUpdate.setIdentification("Yoda", "nom", "yoda");
		assertEquals("Yoda", DBModel.getByKeywords("nom", "yoda", true).get(0));
		assertEquals("Yoda", DBModel.getByKeywords("nom", "od", false).get(0));
	}

	@Test
	public void testGetCategories() throws Exception {
		DBUpdate.setIdentification("Sword", "utilite", "trancher");
		assertEquals("utilite", DBModel.getCategories("tili").get(0));
	}

	@Test
	public void testGetKeywordValue() throws Exception {
		assertEquals("trancher", DBModel.getKeywordValue("Sword", "utilite"));
		assertEquals(null, DBModel.getKeywordValue("tie", "utilite"));
	}

	@Test (expected=Exception.class)
	public void testGetPath() throws Exception {
		assertEquals("data/models/bunny.gts", DBModel.getPath("bunny"));
		assertEquals("", DBModel.getPath("GABUZOMEU"));
	}

	@Test
	public void testGetModelsNames() throws Exception {
		ArrayList<String> sampleNames = new ArrayList<String>();
		sampleNames.add("tie");sampleNames.add("bunny");sampleNames.add("Yoda");sampleNames.add("tetrahedron");sampleNames.add("1MBO.0800");

		assertTrue(DBModel.getModelsNames().containsAll(sampleNames));
	}

	@Test (expected=Exception.class)
	public void testGetPreview() throws Exception {
		assertEquals("ressources/previews/Yoda_preview.png", DBModel.getPreview("Yoda"));
		assertEquals("", DBModel.getPreview("GABUZOMEU"));
	}
}
