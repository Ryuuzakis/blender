package com.blender.test.database;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.blender.utils.database.DBSearch;
import com.blender.utils.database.DBUpdate;
import com.blender.utils.database.Database;

public class DBSearchTest {

	@BeforeClass
	public static void init() {
		try {
			Database.initDb();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testwithName() throws Exception {
		DBSearch s = new DBSearch();
		s.withName("tie");
		assertEquals("tie", s.getResult().get(0));

		s.withName("GABUZOMEU");
		assertEquals(new ArrayList<String>(), s.getResult());
	}

	@Test
	public void testWithoutName() throws Exception {
		DBSearch s = new DBSearch();
		s.withName("an");
		s.withoutName("triangle");
		s.withoutName("panda");
		assertEquals("Banane", s.getResult().get(0));

		s.withoutName("Avion");
		assertEquals("Banane", s.getResult().get(1));

		s = new DBSearch();
		s.withName("Banane");
		s.withoutName("banane");
		assertEquals(new ArrayList<String>(), s.getResult());
	}

	@Test
	public void testWithAttribute() throws Exception {
		DBSearch s = new DBSearch();
		DBUpdate.setIdentification("tie", "forme", "truc");
		DBUpdate.setIdentification("triangle", "forme", "truc");

		s.withAttribute("forme", "truc");
		assertEquals("triangle", s.getResult().get(1));

		s = new DBSearch();
		s.withAttribute("auteur", "justin bieber");
		assertEquals(new ArrayList<String>(), s.getResult());
	}
}
