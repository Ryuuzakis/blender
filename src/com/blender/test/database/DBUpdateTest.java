package com.blender.test.database;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.blender.utils.database.DBModel;
import com.blender.utils.database.DBUpdate;
import com.blender.utils.database.Database;

public class DBUpdateTest {

	@BeforeClass
	public static void init() throws Exception {
		if (new File("blender.db").exists())
			java.nio.file.Files.delete(new File("data/blender.db").toPath());
		Database.initDb();
	}

	@Test
	public void testSaveModel() throws Exception {
		DBUpdate.saveModel("testModelA", "data/models/triangle.gts", null);
		DBUpdate.saveModel("testModelB", "data/models/sphere5.gts", "triangle");
	}

	@Test (expected=Exception.class)
	public void testExceptionSaveModel() throws Exception {
		DBUpdate.saveModel("testModelA", "data/models/triange.gts", null);
		DBUpdate.saveModel("testModelC", "data/models/sphere5.gts", null);
		DBUpdate.saveModel("testModelD", "data/models/sphere5.gts", "testModelZ");
		DBUpdate.saveModel("testModelE", "chezmoi", null);
		assertEquals(new ArrayList<String>(), DBModel.getByName("testModelC", true));
		assertEquals(new ArrayList<String>(), DBModel.getByName("testModelD", true));
		assertEquals(new ArrayList<String>(), DBModel.getByName("testModelE", true));
	}

	@Test
	public void testSetMetaData() throws Exception {
		DBUpdate.setMetaData("triangle", "author", "machin");
		assertEquals("machin", DBModel.getMetaData("triangle", "author"));
	}

	@Test
	public void testSetImage() throws Exception {
		DBUpdate.setImage("triangle", "data/previews/Yoda_preview.png");
		assertEquals("data/previews/Yoda_preview.png", DBModel.getImages("triangle").get(0));
		
	}

	@Test
	public void testSetIdentification() throws Exception {
		DBUpdate.setIdentification("triangle", "forme", "triangulaire");
		assertEquals("triangulaire", DBModel.getKeywordValue("triangle", "forme"));
	}

	@Test
	public void testRemoveIdentification() throws Exception {
		DBUpdate.removeIdentification("triangle", "forme", "triangulaire");
		assertEquals(null, DBModel.getKeywordValue("triangle", "forme"));
	}

}
