package com.blender.views;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

/**
 * Vue affichant un message et une barre de chargement pour les opérations longues
 */
public class LoadingView extends JDialog {
	private static final long serialVersionUID = 1L;
	
	public LoadingView(String message) {
		this.setTitle("Chargement...");
		
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JLabel messageLabel = new JLabel(message, SwingConstants.CENTER);
		messageLabel.setAlignmentX(CENTER_ALIGNMENT);
		messageLabel.setPreferredSize(new Dimension(300, 20));
		messageLabel.setMinimumSize(new Dimension(300, 20));
		messageLabel.setMaximumSize(new Dimension(300, 20));
		panel.add(messageLabel);
		
		panel.add(Box.createRigidArea(new Dimension(0, 10)));
		
		JProgressBar bar = new JProgressBar();
		bar.setPreferredSize(new Dimension(300, 20));
		bar.setIndeterminate(true);
		panel.add(bar);
		
		add(panel);
		
		pack();
		setAlwaysOnTop(true);
		setResizable(false);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(null);
	}
	
	/**
	 * Ouvre la fenêtre de chargement
	 */
	public void start() {
		setVisible(true);
		Frame.getFrames()[0].setEnabled(false);
	}
	
	
	/**
	 * Ferme la fenetre de chargement
	 * A appeler a la fin de l'operation longue
	 */
	public void close() {
		Frame.getFrames()[0].setEnabled(true);
		dispose();
	}
}
