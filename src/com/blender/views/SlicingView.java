package com.blender.views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import com.blender.controllers.SlicingController;
import com.blender.models.ObjectsModel;

/**
 * Vue de la fenêtre de découpe en tranches
 *
 */
public class SlicingView extends JDialog {
	private static final long serialVersionUID = 1L;
	
	private ObjectsModel model;
	private SlicingController controller;
	
	private JLabel label1 = new JLabel("Création d'une découpe en tranches de l'objet courant.");
	private JLabel label2 = new JLabel("La découpe se fera dans l'axe choisi "
			+ "et partira de la disposition actuelle de l'objet.");
	
	/**
	 * Spinner du nombre de tranches
	 */
	private JSpinner nbSpinn;
	
	private JTextField name;
	private JTextField path;
	
	/**
	 * Label indiquant l'épaisseur des tranches
	 */
	private JLabel sliceThickness;
	
	private JFileChooser chooser;
	
	private JButton submit;
	private JButton cancel;
	private JButton browse;
	
	private JPanel mainPane = new JPanel();
	private JPanel contentPane = new JPanel();
	
	private LoadingView loadingView;
	
	private ArrayList<JRadioButton> axisList = new ArrayList<JRadioButton>();
	
	/**
	 * Format permettant de formater l'affichage de la dimension d'une tranche
	 */
	DecimalFormat format = new DecimalFormat("0.000");
	
	public static final byte X_AXIS = 0;
	public static final byte Y_AXIS = 1;
	public static final byte Z_AXIS = 2;
	
	private final String X_AXIS_LABEL = "Axe X";
	private final String Y_AXIS_LABEL = "Axe Y";
	private final String Z_AXIS_LABEL = "Axe Z";
	
	public SlicingView(ObjectsModel model) {
		super(Frame.getFrames()[0]);
		this.model = model;
		loadingView = new LoadingView("Génération de la découpe en cours...");
		controller = new SlicingController(this, loadingView);
		
		setModalityType(DEFAULT_MODALITY_TYPE);
		setTitle("Découpage en tranches d'un modèle");
		setLayout(new BorderLayout());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		setComponents();
		addComponents();
		
		setContentPane(contentPane);
		pack();
		
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	private void addComponents() {
		final int SPACE = 5;
		JPanel labelPanel = new JPanel();
		labelPanel.setLayout(new GridLayout(2, 1, SPACE, SPACE));
		labelPanel.add(label1);
		labelPanel.add(label2);
		
		JPanel labelFieldsPanel = new JPanel();
		labelFieldsPanel.setLayout(new GridLayout(5, 1, SPACE, SPACE));
		labelFieldsPanel.add(new JLabel("Axe de coupe :"));
		labelFieldsPanel.add(new JLabel("Epaisseur d'une tranche : "));
		labelFieldsPanel.add(new JLabel("Nombre de tranches :"));
		labelFieldsPanel.add(new JLabel("Nom du fichier :"));
		labelFieldsPanel.add(new JLabel("Destination :"));
		
		JPanel axisPanel = new JPanel();
		axisPanel.setLayout(new FlowLayout(FlowLayout.CENTER, SPACE, SPACE));
		setAxisPanel(axisPanel);
		
		JPanel fields = new JPanel();
		fields.setLayout(new GridLayout(5, 1, SPACE, SPACE));
		
		JPanel tmp = new JPanel();
		tmp.setLayout(new FlowLayout(FlowLayout.RIGHT));
		tmp.add(sliceThickness);
		fields.add(axisPanel);
		fields.add(tmp);
		fields.add(nbSpinn);
		fields.add(name);
		fields.add(path);
		
		JPanel browsePanel = new JPanel();
		browsePanel.setLayout(new GridLayout(3, 1, SPACE, SPACE));
		browsePanel.add(new JPanel());
		browsePanel.add(new JPanel());
		browsePanel.add(browse);
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, SPACE, SPACE));
		buttonsPanel.add(submit);
		buttonsPanel.add(cancel);
		
		getRootPane().setDefaultButton(submit);
		submit.requestFocus();
		
		mainPane.setLayout(new BorderLayout(SPACE, SPACE));
		mainPane.setBorder(BorderFactory.createTitledBorder("Découper des modèles"));
		mainPane.add(labelPanel, BorderLayout.NORTH);
		mainPane.add(labelFieldsPanel, BorderLayout.WEST);
		mainPane.add(fields, BorderLayout.CENTER);
		mainPane.add(browsePanel, BorderLayout.EAST);
		
		contentPane.setLayout(new BorderLayout(SPACE, SPACE));
		contentPane.add(mainPane, BorderLayout.CENTER);
		contentPane.add(buttonsPanel, BorderLayout.SOUTH);
		
		updateSliceThickness();
	}
	
	private void setAxisPanel(JPanel panel) {
		ButtonGroup bg = new ButtonGroup();
		JRadioButton rb1 = new JRadioButton(X_AXIS_LABEL, false);
		JRadioButton rb2 = new JRadioButton(Y_AXIS_LABEL, false);
		JRadioButton rb3 = new JRadioButton(Z_AXIS_LABEL, true);
		
		axisList.add(rb1);
		axisList.add(rb2);
		axisList.add(rb3);
		for (JRadioButton rb : axisList) {
			bg.add(rb);
			panel.add(rb);
		}
		rb1.addActionListener(controller.getAxisListener(X_AXIS));
		rb2.addActionListener(controller.getAxisListener(Y_AXIS));
		rb3.addActionListener(controller.getAxisListener(Z_AXIS));
		rb3.setSelected(true);
	}

	private void setComponents() {
		int depth = (int) model.getObject().getRect()[5];
		nbSpinn = new JSpinner(new SpinnerNumberModel(10, 0, depth, 1));
		nbSpinn.setToolTipText("Détermine en combien de tranches sera découpé l'objet");
		nbSpinn.addChangeListener(controller.getSpinnerListener());
		
		sliceThickness = new JLabel("");
		sliceThickness.setAlignmentX(JComponent.RIGHT_ALIGNMENT);
		
		name = new JTextField();
		name.setToolTipText("Nom du fichier produit");
		
		path = new JTextField();
		path.setToolTipText("Chemin ou seront crées les tranches");
		
		chooser = new JFileChooser();
		
		submit = new JButton("Valider");
		submit.setToolTipText("Valider la découpe");
		submit.addActionListener(controller.getSendListener(model));
		
		cancel = new JButton("Annuler");
		cancel.setToolTipText("Annuler la découpe");
		cancel.addActionListener(controller.getCancelListener());
		
		browse = new JButton("Parcourir");
		browse.addActionListener(controller.getBrowseController());
	}

	public void updateSliceThickness() {
		float dimension = getCurrentDimension();

		dimension = dimension / getNbSlices();
		sliceThickness.setText(format.format(dimension) + " cm");
	}
	
	public float getCurrentDimension() {
		JRadioButton rb = null;
		for (JRadioButton button : axisList)
			if (button.isSelected())
				rb = button;
		
		float dimension = 0;
		float[] rect = model.getObject().getRect();
		if (rb.getText().equals(X_AXIS_LABEL))
			dimension = rect[3];
		else if (rb.getText().equals(Y_AXIS_LABEL))
			dimension = rect[4];
		else //rb.getText().equals(Z_AXIS_LABEL)
			dimension = rect[5];
		return dimension;
	}

	public void showFileChooser() {
		chooser.setDialogTitle(getTitle());
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int returnValue = chooser.showDialog(getContentPane(), "Valider");
		if (returnValue == JFileChooser.APPROVE_OPTION)
			path.setText(chooser.getSelectedFile().getAbsolutePath());
	}

	public String getNameField() {
		String nameField = name.getText();
		if (nameField.isEmpty()) {
			showError("Veuillez entrer un nom pour le PDF");
			return null;
		}
		return nameField;
	}

	public String getPathField() {
		String pathField = path.getText();
		if (pathField.isEmpty()) {
			showError("Veuillez entrer une destination pour le PDF");
			return null;
		}
		File f = new File(pathField);
		if (!f.exists()) {
			showError("Destination incorrecte");
			return null;
		}
		return pathField;
	}
	
	public int getNbSlices() {
		return (int) nbSpinn.getValue();
	}
	
	private void showError(String error) {
		loadingView.close();
		JOptionPane.showMessageDialog(this, error, "Erreur", JOptionPane.ERROR_MESSAGE);
	}
	
	public JSpinner getNbSpinner() {
		return nbSpinn;
	}
}
