package com.blender.views;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.blender.models.ObjectsModel;

/**
 * Vue representant la fenetre principale de l'application
 *
 */
public class MainWindowView extends JFrame {
	private static final long serialVersionUID = 1L;

	public MainWindowView() {
		super("Blender++");
		setLayout(new BorderLayout());
		
		ObjectsModel model = new ObjectsModel();
		setJMenuBar(new MenuBarView(model));
		
		// Vue de previsualisation du modele 3D
		add(new TabsView(model), BorderLayout.CENTER);
		add(new ToolBarView(this, model), BorderLayout.LINE_START);
		add(new StatusBarView(model), BorderLayout.SOUTH);
		
		pack();
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
