package com.blender.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import com.blender.controllers.ToolBarController;
import com.blender.controllers.ToolButtonController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;

/**
 * Vue de la barre d'outils, comprenant la selection d'outils et 
 * la vue de l'outil selectionné
 *
 */
public class ToolBarView extends JToolBar implements Observer {
	private static final long serialVersionUID = 1L;
	private JPanel toolsPanel;
	private ToolView toolView;
	private JScrollPane scrollPane;
	private ObjectsModel model;
	private boolean enabled = true;
	private MainWindowView mainWnd;
	private HashMap<Tool, JToggleButton> buttons;
	
	public ToolBarView(MainWindowView mainWnd, ObjectsModel model) {
		this.mainWnd = mainWnd;
		this.model = model;
		model.addObserver(this);
		ToolBarController controller = new ToolBarController();
		
		
		JToolBar fillToolBar = new JToolBar(HORIZONTAL);
		mainWnd.add(fillToolBar, BorderLayout.BEFORE_LINE_BEGINS);
		
		buttons = new HashMap<Tool, JToggleButton>();
		toolsPanel = new JPanel();
		toolsPanel.setBorder(BorderFactory.createTitledBorder("Outils"));
		
		setOrientation(VERTICAL);
		//this.setPreferredSize(new Dimension(170, 500));
		this.addPropertyChangeListener(controller.getPropertyChangeListener(this));
		
		setToolButtons(controller);
		scrollPane = new JScrollPane(toolView = new ToolView(model),
				JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		add(toolsPanel);
		add(scrollPane);
		
		resizeVertically();
		updateEnabledState();
	}

	/**
	 * Cree les boutons des outils
	 * @param controller controleur de la vue
	 */
	private void setToolButtons(ToolBarController controller) {
		ButtonGroup group = new ButtonGroup();
		Tool[] tools = Tool.values();
		for (int i = 0; i < tools.length; ++i) {
			JToggleButton button = new JToggleButton(new ImageIcon(
					getClass().getResource("/" + tools[i % tools.length].getValue() + ".png")));
			
			// Raccourci clavier
			button.getInputMap(JToolBar.WHEN_IN_FOCUSED_WINDOW).put
			(KeyStroke.getKeyStroke(KeyEvent.VK_F1+i, 0), "select");
			
			button.setToolTipText("Outil de " + tools[i].getValue());
			
			button.getActionMap().put("select", controller.getButtonListener(button));
			button.setFocusable(false);
			button.setMargin(new Insets(0, 0, 0, 0));
			button.setPreferredSize(new Dimension(40, 40));
			button.setMaximumSize(new Dimension(40, 40));
			button.setMinimumSize(new Dimension(40, 40));
			
			ToolButtonController btnController = new ToolButtonController(model, tools[i % tools.length]);
			button.addChangeListener(btnController.getChangeListener());
			if (i == 0) button.setSelected(true);
			group.add(button);
			buttons.put(tools[i], button);
			toolsPanel.add(button);
		}
	}

	public void resizeHorizontally() {
		removeAll();
		setVisible(false);
		setEnabled(false);
		mainWnd.add(new ToolBarView(mainWnd, model), BorderLayout.LINE_START);
	}
	
	public void resizeVertically() {
		this.setPreferredSize(new Dimension(190, 500));
		toolsPanel.setPreferredSize(new Dimension(190, 125));
		toolsPanel.setMinimumSize(new Dimension(190, 125));
		toolsPanel.setMaximumSize(new Dimension(190, 125));
		toolView.setPreferredSize(new Dimension(160, 500));
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;

		if (notif.getType() == ModelNotification.TAB_CHANGED || 
				notif.getType() == ModelNotification.OBJECT_CREATED) 
			updateEnabledState();
		else if (notif.getType() == ModelNotification.TOOL_CHANGED || notif.getType() == ModelNotification.DB_UPDATED) {
			JToggleButton btn = buttons.get((Tool)notif.getData());
			if (btn != null && !btn.isSelected()) btn.setSelected(true);
			if (toolView == null) return;
			int size = model.getTool() == Tool.INFORMATION ? toolView.getContentWidth() + 10 : 160;
			toolView.setPreferredSize(new Dimension(size, 125));
		}
	}
	
	private void updateEnabledState() {
		if (model.getObject() == null) {
			if (enabled) {
				enableComponents(this, false);
				enabled = false;
			}
		} else {
			if (!enabled) {
				enableComponents(this, true);
				enabled = true;
			}
		}
	}
	
	private void enableComponents(Container container, boolean enable) {
        Component[] components = container.getComponents();
        for (Component component : components) {
            component.setEnabled(enable);
            if (component instanceof Container) enableComponents((Container)component, enable);
        }
    }

}
