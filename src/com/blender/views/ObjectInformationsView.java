package com.blender.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.blender.controllers.ObjectsInformationsController;
import com.blender.models.ObjectInformations;
import com.blender.models.ObjectsModel;

/**
 * Vue de modification des informations sur un model
 */
public class ObjectInformationsView extends JDialog {
	private static final long serialVersionUID = 1L;
	private ObjectsInformationsController controller;
	private JPanel labelsPanel;
	private JPanel fieldsPanel;
	private JTextField nameField, authorField, dateField, shapeField, keywordsField;
	private JTextArea descriptionArea;
	private ObjectInformations info;
	
	/**
	 * Affiche une popup permettant d'editer les informations sur un objet
	 * @param model Le model
	 * @param objectName Le nom de l'objet qui va etre edité
	 */
	public ObjectInformationsView(ObjectsModel model, String objectName) {
		super(Frame.getFrames()[0]);
		this.setTitle("Informations sur l'objet");
		this.controller = new ObjectsInformationsController(model);
		
		setLayout(new BorderLayout());
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(BorderFactory.createTitledBorder("Modifier les informations de l'objet"));
		mainPanel.setLayout(new BorderLayout());
		add(mainPanel, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		labelsPanel = new JPanel();
		labelsPanel.setLayout(new BoxLayout(labelsPanel, BoxLayout.Y_AXIS));
		fieldsPanel = new JPanel();
		fieldsPanel.setLayout(new BoxLayout(fieldsPanel, BoxLayout.Y_AXIS));
		
		panel.add(labelsPanel, BorderLayout.WEST);
		panel.add(Box.createRigidArea(new Dimension(5, 0)));
		panel.add(fieldsPanel, BorderLayout.EAST);
		
		this.info = model.getObjectInformations(objectName);
		addPreview(info, mainPanel);
		addLines(info);
		mainPanel.add(panel, BorderLayout.CENTER);
		
		addBtns();
		
		pack();
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setModalityType(DEFAULT_MODALITY_TYPE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	/**
	 * Ajoute l'image de preview du model
	 * @param info Les informations sur le model
	 * @param panel Le panel sur lequel ajouter la preview
	 */
	private void addPreview(ObjectInformations info, JPanel panel) {
		BufferedImage icon;
		try {
			icon = ImageIO.read(new File(info.getPreviewPath()));
			Image img = icon.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
			JLabel lbl = new JLabel(new ImageIcon(img));
			panel.add(lbl, BorderLayout.NORTH);
		} catch (IOException e) {}
	}
	
	/**
	 * Crée un champs de texte
	 * @param str La valeur par défaut du champs de texte
	 * @return Une nouvelle instance de JTextField
	 */
	private JTextField createTextField(String str) {
		JTextField field = new JTextField(str);
		field.setPreferredSize(new Dimension(200, 24));
		return field;
	}
	
	/**
	 * Crée un champs de texte defilable
	 * @param str Le valeur par défaut du champs de texte
	 * @return Une instance de JSrollPane contenant le champs de texte
	 */
	private JScrollPane createScrollableTextArea(String str) {
		descriptionArea = new JTextArea(str);
		descriptionArea.setPreferredSize(new Dimension(200, 80));
		JScrollPane scrollPane = new JScrollPane (descriptionArea);
		return scrollPane;
	}
	
	/**
	 * Ajoute les parametres editable de l'objet
	 * @param info L'objet contenant les informations sur le model
	 */
	private void addLines(ObjectInformations info) {
		addLine("Nom :", nameField = createTextField(info.getName()));
		addLine("Auteur :", authorField = createTextField(info.getAuthor()));
		addLine("Date :", dateField = createTextField(info.getDate()));
		addLine("Forme(s) :", shapeField = createTextField(info.getShape()));
		addLine("Mots-clés :", keywordsField = createTextField(info.getKeywords()));
		addLine("Description :", createScrollableTextArea(info.getDescription()));
		
		nameField.setEnabled(false);
		if (!info.isNewModel()) dateField.setEnabled(false);
	}

	/**
	 * Ajoute une "ligne" avec un libelle et un champs
	 * @param label Le libelle associe au champs
	 * @param field Le champs a remplir par l'utilisateur
	 */
	private void addLine(String label, JComponent field) {
		labelsPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		fieldsPanel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		JLabel jlabel = new JLabel(label);
		jlabel.setAlignmentX(LEFT_ALIGNMENT);
		
		Dimension dim = new Dimension((int) jlabel.getPreferredSize().getWidth(), (int) field.getPreferredSize().getHeight());
		jlabel.setPreferredSize(dim);
		jlabel.setMinimumSize(dim);
		jlabel.setMaximumSize(dim);
		
		field.setAlignmentX(LEFT_ALIGNMENT);
		
		labelsPanel.add(jlabel);
		fieldsPanel.add(field);
	}
	
	/**
	 * Ajoute les boutons de validation/annulation
	 */
	private void addBtns() {
		JPanel btnsPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton submit = new JButton("Valider");
		JButton cancel = new JButton("Annuler");
		
		getRootPane().setDefaultButton(submit);
		submit.requestFocus();
		
		btnsPanel.add(submit);
		btnsPanel.add(cancel);
		this.add(btnsPanel, BorderLayout.SOUTH);
		submit.addActionListener(controller.getSubmitListener(this));
		cancel.addActionListener(controller.getCancelListener(this));
	}
	
	/**
	 * Met a jour l'objet contenant les informations sur l'objet
	 * @throws ParseException si la date saisie est incorrecte
	 */
	public void updateInfos() throws ParseException {
		info.setAuthor(authorField.getText());
		info.setDate(dateField.getText());
		info.setDescription(descriptionArea.getText());
		info.setKeywords(keywordsField.getText());
		info.setShape(shapeField.getText());
	}

	/**
	 * Renvoie l'objet contenant les informations sur le model
	 * @return Une instance d'un objet contenant toutes les infos sur le model
	 */
	public ObjectInformations getInfos() {
		return info;
	}
	
	/**
	 * Affiche une popup d'erreur
	 * @param e L'exception a afficher
	 */
	public void showError(Exception e) {
		JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
	}
}
