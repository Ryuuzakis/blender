package com.blender.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import com.blender.controllers.ObjectController;
import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.Tool;
import com.blender.utils.gui.ObjectRenderer;

/**
 * Vue représentant un modèle 3D
 */
public class ObjectView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;

	/**
	 *  Modèle associé à cette vue
	 */
	private ObjectsModel model;
	
	/**
	 * Instance permettant de dessiner
	 */
	private ObjectRenderer objRenderer;

	/**
	 * Crée un JPanel contenant le dessin d'un modèle 3D
	 * @param model Le modèle 3D à dessiner
	 * @param controller Le controleur associé à cette vue
	 */
	public ObjectView(ObjectsModel model, ObjectController controller) {
		this.model = model;
		objRenderer = new ObjectRenderer();
		model.addObserver(this);
		model.getProperties().addObserver(this);
		setFocusable(true);
		setPreferredSize(new Dimension(640, 480));

		// Configuration du controleur
		addKeyListener(controller.getKeyListener());
		addMouseWheelListener(controller.getMouseWheelListener());
		addMouseMotionListener(controller.getMouseMotionListener());
		addMouseListener(controller.getMouseListener());
		addComponentListener(controller.getComponentListener());
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		PropertiesModel prop = model.getProperties();
		
		if (prop.getProperty(PropertiesModel.O_VISIBLE)) {
			// Dessine l'objet 3D (soit en fil de fer, soit avec les faces)
			if (prop.getProperty(PropertiesModel.O_WIRED) || model.getMode() == ObjectsModel.MODE_WIRE) 
				objRenderer.drawWires(model.getObject(), g2, prop.getProperty(PropertiesModel.O_SEGMENT_COLOR, new Color(0)));
			else if (prop.getProperty(PropertiesModel.O_DRAW_DOTS))
				objRenderer.drawPoints(g2, model.getObject().getPoints(), 
						prop.getProperty(PropertiesModel.O_COLOR, new Color(0)));
			else
				objRenderer.drawFaces(model, g2);
		}
		
		// Dessine les normales
		if (prop.getProperty(PropertiesModel.O_NORMALS))
			objRenderer.drawNormals(model.getObject(), g2, 
					prop.getProperty(PropertiesModel.O_NORMALS_COLOR, new Color(0)));
		
		//Dessine la boite englobante
		if (prop.getProperty(PropertiesModel.B_VISIBLE))
			objRenderer.drawBox(g2, model.getObject().getEnglobingBox(), 
					prop.getProperty(PropertiesModel.B_COLOR, new Color(0)));
		
		// Dessine le centre de l'objet
		if (prop.getProperty(PropertiesModel.O_CENTER)) {
			g2.setColor(prop.getProperty(PropertiesModel.O_CENTER_COLOR, new Color(0)));
			g2.drawOval((int)model.getObject().getCenter().getX() - 3, (int)model.getObject().getCenter().getY() - 3, 7, 7);
		}
		
		// Dessine la position de la lumiere
		if (model.isUsingTool(Tool.LIGHT) && prop.getProperty(PropertiesModel.O_DRAW_LIGHT) && !prop.getProperty(PropertiesModel.O_LIGHT_SUN)) 
			objRenderer.drawLightSource(model.getObject(), g2, prop.getProperty(PropertiesModel.O_LIGHT_COLOR, new Color(0)));
		
		// Dessine l'axe de rotation
		if (prop.getProperty(PropertiesModel.A_VISIBLE))
			objRenderer.drawAxis(model.getObject(), g2, getHeight());
	}
	
	@Override
	public void update(Observable arg0, Object n) {
		repaint();
	}
}
