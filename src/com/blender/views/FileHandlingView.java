package com.blender.views;

import java.awt.Dialog;
import java.awt.Frame;
import java.io.File;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.blender.controllers.FileHandlingController;
import com.blender.models.FileHandlingModel;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.views.filehandling.AbstractHandling;
import com.blender.views.filehandling.FactoryFileHandlingView;

/**
 * Classe gérant les vues d'import et d'export
 *
 */
public class FileHandlingView extends JDialog implements Observer{
	private static final long serialVersionUID = 9139997814114083752L;
	
	/**
	 * Panel contenant la vue d'import
	 */
	private AbstractHandling importHandling;
	
	/**
	 * Panel contenant la vue d'export
	 */
	private AbstractHandling exportHandling;
	
	/**
	 * Vue actuellement affichee
	 */
	private AbstractHandling currentPane;

	private FileHandlingController controller;
	private FileHandlingModel model;

	private JFileChooser chooser;
	
	private LoadingView loadingView;

	public FileHandlingView(ObjectsModel objModel) {
		super(Frame.getFrames()[0]);
		model = new FileHandlingModel();
		model.addObserver(this);
		controller = new FileHandlingController(this, model, objModel);
		loadingView = new LoadingView("Transfert en cours...");
		importHandling = FactoryFileHandlingView.getView(false, loadingView, controller);
		exportHandling = FactoryFileHandlingView.getView(true, loadingView, controller);
		
		setResizable(false);
		setVisible(false, false);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
	}

	/**
	 * Liste contenant les modèles sélectionnés pour l'opération en cours
	 * @return liste de chaines
	 */
	public List<String> getModelsNames() {
		List<String> modelNames = currentPane.getModelNames(); 
		if (modelNames == null || modelNames.isEmpty()) {
			showError(AbstractHandling.NAME_ERROR);
			return null;
		}
		return modelNames;
	}

	/**
	 * Permet de faire apparaitre/disparaitre cette vue
	 * @param visible fais apparaitre la vue si true, la fais disparaitre si false
	 * @param export true si l'utilisateur veut exporter un fichier, false s'il veut importer
	 */
	public void setVisible(boolean visible, boolean export) {
		if (visible) {
			if (export)
				currentPane = exportHandling;
			else
				currentPane = importHandling;

			setTitle(currentPane.getTitle());
			setContentPane(currentPane);
			
			JButton send = currentPane.getSendButton();
			getRootPane().setDefaultButton(send);
			send.requestFocus();
			
			pack();
			setLocationRelativeTo(null);
		}
		setVisible(visible);
	}

	/**
	 * Affiche l'explorateur de fichiers avec les paramètres nécessaires à la vue courante
	 * @param title titre de l'explorateur de fichiers
	 * @param approveButtonText texte du bouton de validation de l'explorateur
	 */
	public void showFileChooser(String title, String approveButtonText) {
		String path = currentPane.getFilePath();
		if (path.isEmpty())
			chooser = new JFileChooser();
		else
			chooser = new JFileChooser(path);

		chooser.setDialogTitle(title);
		chooser.setFileSelectionMode(currentPane.getSelectionMode());

		int returnValue = chooser.showDialog(getContentPane(), approveButtonText);
		if (returnValue == JFileChooser.APPROVE_OPTION)
			currentPane.setPath(chooser.getSelectedFile());
	}

	@Override
	public void update(Observable obs, Object object) {
		ModelNotification notif = (ModelNotification) object;
		if (notif == null)
			return;
		if (!notif.isTreated() && notif.getType() == ModelNotification.ERROR_MSG) {
			showError(((Exception) notif.getData()).getMessage());
			notif.setTreated(true);
		} else if (this.isVisible() && notif.getType() == ModelNotification.DB_UPDATED) {
			loadingView.close();
			String message = currentPane.getSuccessMessage();
			JOptionPane.showMessageDialog(this, message, "Succès", JOptionPane.INFORMATION_MESSAGE);
			setVisible(false, false);
		}
	}

	/**
	 * Recupere le chemin entre par l'utilisateur dans la vue courante
	 * @return chemin
	 */
	public String getFilePath() {
		String path = currentPane.getFilePath();
		if (path.isEmpty()) {
			showError(AbstractHandling.PATH_ERROR);
			return null;
		}
		File f = new File(path);
		if (!f.exists()) {
			showError(AbstractHandling.PATH_ERROR);
			return null;
		}
		return path;
	}
	
	/**
	 * Affiche une popup d'erreur
	 * @param error paramètre correspondant au type de l'erreur
	 */
	public void showError(byte error) {
		showError(currentPane.getErrorMessage(error));
	}
	
	/**
	 * Affiche une popup d'erreur
	 * @param message le message d'erreur
	 */
	public void showError(String message) {
		loadingView.close();
		JOptionPane.showMessageDialog(this, message, "Erreur", JOptionPane.ERROR_MESSAGE);
	}
}
