package com.blender.views;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.blender.controllers.ObjectController;
import com.blender.controllers.TabController;
import com.blender.controllers.TabsController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;

/**
 * Vue des onglets
 *
 */
public class TabsView  extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	
	private JTabbedPane tabbedPane;
	private ObjectsModel objModel;
	
	public TabsView(ObjectsModel model) {
		this.objModel = model;
		
		TabsController controller = new TabsController(objModel);
		
		objModel.addObserver(this);
		
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("+", new SearchView(objModel, tabbedPane));
		tabbedPane.addChangeListener(controller.getChangeListener());
		
		objModel.addObject();
		
		setLayout(new BorderLayout());
		add(tabbedPane, BorderLayout.CENTER);
	}
	
	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		int index = tabbedPane.getTabCount() - 1;
		if (notif.getType() == ModelNotification.TAB_ADDED) {
			tabbedPane.setTitleAt(index, (String)notif.getData());
			TabView view = new TabView(objModel, "Sans titre", new TabController(objModel));
			tabbedPane.setTabComponentAt(index, view);
			view.updateLabelColor();
			tabbedPane.addTab("+", new SearchView(objModel, tabbedPane));
		} else if (notif.getType() == ModelNotification.OBJECT_CREATED) {
			Object[] datas = (Object[])notif.getData();
			tabbedPane.setComponentAt((Integer)datas[0], new ObjectView(objModel, new ObjectController(objModel)));
		} else if (notif.getType() == ModelNotification.TAB_REMOVED) {
			// Si on a selectionne le dernier onglet et que on souhaite le fermer, on change la selection
			if ((Integer)notif.getData() == index-1 && tabbedPane.getSelectedIndex() == index-1) tabbedPane.setSelectedIndex(index-2);
			tabbedPane.remove((Integer)notif.getData());
			// S'il n'y a plus d'onglet, on en recrée un nouveau
			if (index - 2 < 0) {
				objModel.addObject();
				tabbedPane.setSelectedIndex(0);
			}
		} else if (!notif.isTreated() && notif.getType() == ModelNotification.ERROR_MSG) {
			Exception e = (Exception)notif.getData();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			notif.setTreated(true);
		}
	}

}
