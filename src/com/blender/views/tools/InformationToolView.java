package com.blender.views.tools;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.blender.controllers.InformationsToolController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectInformations;
import com.blender.models.ObjectsModel;
import com.blender.utils.maths.transformable.TransformableObject3D;

/**
 * Vue des options de l'outil des informations
 *
 */
public class InformationToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private InformationsToolController controller;
	private ObjectsModel model;
	private JLabel previewImage, nameLbl, authorLbl, dateLbl, nbFacesLbl;
	private JButton editInfoBtn;
	
	public InformationToolView(ObjectsModel model) {
		this.model = model;
		this.controller = new InformationsToolController(model);
		
		this.setBorder(BorderFactory.createTitledBorder("Informations"));
		model.addObserver(this);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		previewImage = new JLabel();
		nameLbl = new JLabel("Nom : ");
		authorLbl = new JLabel("Auteur : ");
		dateLbl = new JLabel("Date : ");
		nbFacesLbl = new JLabel("Nombre de faces : ");
		editInfoBtn = new JButton("Modifier les infos");
		editInfoBtn.setPreferredSize(new Dimension(100, 26));
		editInfoBtn.setMinimumSize(new Dimension(150, 26));
		editInfoBtn.setMaximumSize(new Dimension(150, 26));
		editInfoBtn.setToolTipText("Ouvre une fenêtre permettant de modifier les informations du modèle courant");
		updateInformations();
		
		panel.add(nameLbl);
		panel.add(authorLbl);
		panel.add(dateLbl);
		panel.add(nbFacesLbl);
		panel.add(Box.createRigidArea(new Dimension(0, 5)));
		panel.add(editInfoBtn);
		add(previewImage);
		add(panel);
		
		editInfoBtn.addActionListener(controller.getModifyInformationsListener());
	}
	

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		
		if (notif.getType() == ModelNotification.TAB_CHANGED 
				|| notif.getType() == ModelNotification.OBJECT_CREATED
				|| notif.getType() == ModelNotification.DB_UPDATED) {
			updateInformations();
		}
	}
	
	private void updateInformations() {
		TransformableObject3D obj = model.getObject();
		if (obj == null) return;
		ObjectInformations infos = model.getObjectInformations(obj.getModelName());
		
		BufferedImage icon;
		try {
			icon = ImageIO.read(new File(infos.getPreviewPath()));
			Image img = icon.getScaledInstance(100, 100, Image.SCALE_SMOOTH);
			previewImage.setIcon(new ImageIcon(img));
		} catch (IOException e) {}
		
		nameLbl.setText("<html><b>Nom :</b> " + infos.getName() + "</html>");
		authorLbl.setText("<html><b>Auteur :</b> " + infos.getAuthor() + "</html>");
		dateLbl.setText("<html><b>Date :</b> " + infos.getDate() + "</html>");
		nbFacesLbl.setText("<html><b>Nombre de faces :</b> " + obj.getFaces().size() + "</html>");
	}
}
