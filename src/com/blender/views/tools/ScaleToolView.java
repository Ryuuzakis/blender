package com.blender.views.tools;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.blender.controllers.ScaleToolController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;

/**
 * Vue des options de l'outil de zoom
 *
 */
public class ScaleToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private ScaleToolController controller;
	private ObjectsModel model;
	private JSpinner spinnerx;
	private JSpinner spinnery;
	private JSpinner spinnerz;
	
	public ScaleToolView(ObjectsModel model) {
		this.model = model;
		model.addObserver(this);
		controller = new ScaleToolController(model);
		
		this.setBorder(BorderFactory.createTitledBorder("Homothétie"));
		
		JLabel info = new JLabel("Molette", JLabel.CENTER);
		info.setToolTipText("Commande permettant d'utiliser cet outil");
		info.setPreferredSize(new Dimension(200, 20));
		Font f = info.getFont();
		info.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		add(info);
		
		createAxisPanel();
		
		JButton button = new JButton("Adapter");
		button.setPreferredSize(new Dimension(135, 25));
		button.setToolTipText("Adapte l'objet à la taille optimale pour l'espace disponible");
		button.addActionListener(controller.getActionListener());
		add(button);
	}
	
	/**
	 * Crée les JSpinner de redimensionnement pour chaque axe
	 */
	private void createAxisPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JPanel panelx = new JPanel();
		panelx.add(new JLabel("X"));
		spinnerx = new JSpinner(new SpinnerNumberModel(0f, 0f, 9999f, 0.2f));
		spinnerx.setPreferredSize(new Dimension(120, 22));
		spinnerx.setToolTipText("Redimensionne l'objet en X");
		panelx.add(spinnerx);
		panel.add(panelx);
		
		JPanel panely = new JPanel();
		panely.add(new JLabel("Y"));
		spinnery = new JSpinner(new SpinnerNumberModel(0f, 0f, 9999f, 0.2f));
		spinnery.setPreferredSize(new Dimension(120, 22));
		spinnery.setToolTipText("Redimensionne l'objet en Y");
		panely.add(spinnery);
		panel.add(panely);
		
		JPanel panelz = new JPanel();
		panelz.add(new JLabel("Z"));
		spinnerz = new JSpinner(new SpinnerNumberModel(0f, 0f, 9999f, 0.2f));
		spinnerz.setPreferredSize(new Dimension(120, 22));
		spinnerz.setToolTipText("Redimensionne l'objet en Z");
		panelz.add(spinnerz);
		panel.add(panelz);
		
		spinnerx.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		spinnery.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		spinnerz.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		
		add(panel);
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		
		if (notif.getType() == ModelNotification.OBJECT_LF_MODIFIED || notif.getType() == ModelNotification.TAB_CHANGED  && model.getObject() != null) {
			spinnerx.setValue((double)model.getObject().getScale().getX());
			spinnery.setValue((double)model.getObject().getScale().getY());
			spinnerz.setValue((double)model.getObject().getScale().getZ());
		}
	}
}
