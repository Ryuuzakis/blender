package com.blender.views.tools;

import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import com.blender.controllers.OptionsController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;

/**
 * Vue des options de l'outil options
 *
 */
public class OptionToolView extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;
	
	private PropertiesModel properties;
	private OptionsController controller;
	
	private JTabbedPane tabs;
	private ButtonGroup drawingAlgoGroup = new ButtonGroup();
	
	private JPanel visibilityPanel;
	private JPanel colorPanel;
	private JPanel drawPanel;
	
	private Dimension buttonDimension = new Dimension(135, 25);
	
	private HashMap<String, JCheckBox> checkboxMap = new HashMap<String, JCheckBox>();
	private HashMap<String, JRadioButton> radioButtonMap = new HashMap<String, JRadioButton>();
	private HashMap<String, JSlider> sliderMap = new HashMap<String, JSlider>();

	public OptionToolView(ObjectsModel model) {
		model.addObserver(this);
		properties = model.getProperties();
		properties.addObserver(this);
		controller = new OptionsController(this, properties);
		this.setBorder(BorderFactory.createTitledBorder("Options"));
		
		setComponents();
	}
	
	/**
	 * Dispose les différents éléments graphiques
	 */
	private void setComponents() {
		tabs = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
		this.add(tabs);
		setVisibilityPanel();
		setColorPanel();
		setDrawPanel();
		tabs.addTab("Affichages", visibilityPanel);
		tabs.addTab("Couleurs", colorPanel);
		tabs.addTab("Dessin", drawPanel);
	}
	
	/**
	 * Initialise le panel de l'onglet "Dessin"
	 */
	private void setDrawPanel() {
		drawPanel = new JPanel();
		drawPanel.setLayout(new BoxLayout(drawPanel, BoxLayout.PAGE_AXIS));
		
		drawPanel.add(Box.createRigidArea(new Dimension(1, 3)));
		drawPanel.add(new JLabel("Algorithme de dessin"));
		drawPanel.add(Box.createRigidArea(new Dimension(1, 3)));
		createJRadioButton(PropertiesModel.O_PAINTER, drawPanel, "Active l'algorithme du peintre",
				properties.getProperty(PropertiesModel.O_PAINTER));
		createJRadioButton(PropertiesModel.O_ZBUFFER, drawPanel, "Active le Z-Buffer", 
				properties.getProperty(PropertiesModel.O_ZBUFFER));
		createJRadioButton(PropertiesModel.O_ZBUFFER_IMP, drawPanel, "Active le Z-Buffer amélioré, les faces ont un dégradé de couleur", 
				properties.getProperty(PropertiesModel.O_ZBUFFER_IMP));
		drawPanel.add(Box.createRigidArea(new Dimension(1, 8)));
		createCheckbox(PropertiesModel.O_ACTIVATE_LIGHT, drawPanel, "Active l'effet de lumiere sur l'objet (algorithme du peintre uniquement si désactivé)");
		createCheckbox(PropertiesModel.O_LIGHT_SUN, drawPanel, "Passe la lumière en mode soleil : elle est fixée en haut à gauche de l'écran");
		createCheckbox(PropertiesModel.O_WIRED, drawPanel, "Passe l'objet en mode fil de fer");
		createCheckbox(PropertiesModel.O_DRAW_DOTS, drawPanel, "Passe l'objet en mode nuage de points");
		
		drawPanel.add(Box.createRigidArea(new Dimension(1, 8)));
		drawPanel.add(new JLabel("Intensité lumière"));
		createJSlider(PropertiesModel.LIGHT_COLOR_INTENSITY, drawPanel, "Règle l'intensité du spot de lumière");
		drawPanel.add(Box.createRigidArea(new Dimension(1, 3)));
		drawPanel.add(new JLabel("Intensité objet"));
		createJSlider(PropertiesModel.OBJ_COLOR_INTENSITY, drawPanel, "Règle l'intensité de la lumière ambiante (couleur de l'objet)");
	}
	
	/**
	 * Initialise le panel de l'onglet "Couleurs"
	 */
	private void setColorPanel() {
		colorPanel = new JPanel();
		colorPanel.setLayout(new BoxLayout(colorPanel, BoxLayout.PAGE_AXIS));

		JPanel tmp = new JPanel();
		tmp.setLayout(new BoxLayout(tmp, BoxLayout.PAGE_AXIS));

		createColorButton(PropertiesModel.O_COLOR, tmp, "Choisir la couleur de l'objet");
		createColorButton(PropertiesModel.O_LIGHT_COLOR, tmp, "Choisir la couleur de la lumiere");
		createColorButton(PropertiesModel.B_COLOR, tmp, "Choisir la couleur de la boite englobante");
		createColorButton(PropertiesModel.O_NORMALS_COLOR, tmp, "Choisir la couleur des normales");
		createColorButton(PropertiesModel.O_SEGMENT_COLOR, tmp, "Choisir la couleur des segments (lumiere desactivee uniquement)");
		createColorButton(PropertiesModel.O_CENTER_COLOR, tmp, "Choisir la couleur du centre de l'objet");
		
		colorPanel.add(Box.createVerticalGlue());
		colorPanel.add(tmp);
		colorPanel.add(Box.createVerticalGlue());
	}
	
	/**
	 * Initialise le panel de l'onglet "Affichages"
	 */
	private void setVisibilityPanel() {
		visibilityPanel = new JPanel();
		visibilityPanel.setLayout(new BoxLayout(visibilityPanel, BoxLayout.PAGE_AXIS));
		
		visibilityPanel.add(Box.createRigidArea(new Dimension(45, 30)));
		createCheckbox(PropertiesModel.O_VISIBLE, visibilityPanel, "Affiche l'objet");
		createCheckbox(PropertiesModel.B_VISIBLE, visibilityPanel, "Affiche la boite englobante"); 
		createCheckbox(PropertiesModel.O_CENTER, visibilityPanel, "Affiche le centre de l'objet");
		createCheckbox(PropertiesModel.A_VISIBLE, visibilityPanel, "Affiche l'axe de rotation");
		createCheckbox(PropertiesModel.O_NORMALS, visibilityPanel, "Affiche les normales des faces de l'objet");
		
	}
	
	/**
	 * Méthode permettant de créer des checkbox ayant le même formatage
	 * @param label label de la proprieté
	 * @param panel panel cible
	 * @param toolTip toolTip du bouton
	 */
	private void createCheckbox(String label, JPanel panel, String toolTip) {
		panel.add(Box.createRigidArea(new Dimension(0, 3)));
		JCheckBox checkbox = new JCheckBox(label, properties.getProperty(label));
		checkbox.addItemListener(controller.getCheckboxBooleanListener(checkbox));
		checkbox.setToolTipText(toolTip);
		checkboxMap.put(checkbox.getText(), checkbox);
		panel.add(checkbox);
	}
	
	/**
	 * Crée des JRadioButton ayant le même formatage
	 * @param label label du bouton
	 * @param panel panel cible
	 * @param toolTip toolTip du bouton
	 * @param selected true si le bouton doit être sélectionné, false sinon
	 */
	private void createJRadioButton(String label, JPanel panel, String toolTip, boolean selected) {
		JRadioButton rb = new JRadioButton(label, selected);
		rb.addActionListener(controller.getDrawingAlgoListener(rb));
		rb.setToolTipText(toolTip);
		radioButtonMap.put(rb.getText(), rb);
		drawingAlgoGroup.add(rb);
		panel.add(rb);
	}
	
	/**
	 * Methode permettant de creer un JButton avec un nom de propriete de couleur, et
	 * le panel auquel appartient la checkbox.
	 * @param label label de la propriete
	 * @param panel panel cible
	 * @param toolTip toolTip du slider
	 */
	private void createColorButton(String label, JPanel panel, String toolTip) {
		panel.add(Box.createRigidArea(new Dimension(0, 3)));
		JButton button = new JButton(label);
		button.addActionListener(controller.getButtonColorListener(button));
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		button.setToolTipText(toolTip);
		button.setPreferredSize(buttonDimension);
		button.setMinimumSize(buttonDimension);
		button.setMaximumSize(buttonDimension);
		panel.add(button);
	}
	
	/**
	 * Méthode permettant de créer un JSlider avec un nom de propriété d'integer, et le panel
	 * auquel il appartient
	 * @param label label de la propriete
	 * @param panel panel cible
	 * @param toolTip toolTip du slider
	 */
	private void createJSlider(String label, JPanel panel, String toolTip) {
		JSlider slider = new JSlider(SwingConstants.HORIZONTAL, 0, 100, properties.getProperty(label, 0));
		slider.setName(label);
		slider.addChangeListener(controller.getSliderListener(slider));
		slider.setToolTipText(toolTip);
		slider.setPreferredSize(new Dimension(50, 20));
		sliderMap.put(label, slider);
		panel.add(slider);
	}
	
	@Override
	public void update(Observable o, Object n) {
		if (n == null)
			return;
		
		ModelNotification notif = (ModelNotification) n;
		if (notif.getType() == ModelNotification.PROPERTY_CHANGED) {
			String data = (String) notif.getData();
			if (checkboxMap.containsKey(data)) {
				JCheckBox box = checkboxMap.get((String) notif.getData());
				box.setSelected(properties.getProperty((String) notif.getData()));
			}
			if (data.equals(PropertiesModel.O_ACTIVATE_LIGHT)) {
				boolean activate = properties.getProperty(PropertiesModel.O_ACTIVATE_LIGHT);
				radioButtonMap.get(PropertiesModel.O_ZBUFFER).setEnabled(activate);
				radioButtonMap.get(PropertiesModel.O_ZBUFFER_IMP).setEnabled(activate);
				if (!activate)
					radioButtonMap.get(PropertiesModel.O_PAINTER).setSelected(true);
			}
			
		} else if (notif.getType() == ModelNotification.PROPERTIES_CHANGED) {
			for (JCheckBox box : checkboxMap.values()) 
				box.setSelected(properties.getProperty(box.getText()));
			for (JSlider slider : sliderMap.values())
				slider.setValue(properties.getProperty(slider.getName(), 0));
			for (JRadioButton rb : radioButtonMap.values()) {
				boolean property = properties.getProperty(rb.getText());
				if (property) {
					rb.setSelected(true);
					return;
				}
			}
		}
	}

}
