package com.blender.views.tools;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import com.blender.controllers.RotationToolController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;

/**
 * Vue des options de l'outil de rotation
 *
 */
public class RotationToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private ObjectsModel model;
	private RotationToolController controller;
	private JSlider sliderx, slidery, sliderz;

	public RotationToolView(ObjectsModel model) {
		this.model = model;
		this.controller = new RotationToolController(model);
		model.addObserver(this);

		setComponents();
	}

	/**
	 * Initialise les composants de GUI
	 */
	private void setComponents() {
		this.setBorder(BorderFactory.createTitledBorder("Rotation"));

		JLabel info = new JLabel("Clic gauche", JLabel.CENTER);
		info.setToolTipText("Commande permettant d'utiliser cet outil");
		info.setPreferredSize(new Dimension(200, 20));
		Font f = info.getFont();
		info.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		add(info);

		JCheckBox wireMod = new JCheckBox("Mode fil de fer", false);
		wireMod.addActionListener(controller.getWireModeCheckboxListener());
		wireMod.setToolTipText("Utilise la vue fils de fer lors de la rotation");
		add(wireMod);

		createAxisPanel();
		JButton resetViewButton = new JButton("Vue initiale");
		JButton addXViewButton = new JButton("+90° en X");
		JButton addYViewButton = new JButton("+90° en Y");

		resetViewButton.setToolTipText("Ramène l'objet à son positionnement initial");
		addXViewButton.setToolTipText("Ajoute 90° horizontalement");
		addYViewButton.setToolTipText("Ajoute 90° verticalement");

		resetViewButton.setPreferredSize(new Dimension(135, 25));
		addXViewButton.setPreferredSize(new Dimension(135, 25));
		addYViewButton.setPreferredSize(new Dimension(135, 25));

		resetViewButton.addActionListener(controller.getFrontButtonActionListener());
		addXViewButton.addActionListener(controller.getAddXButtonActionListener());
		addYViewButton.addActionListener(controller.getAddYButtonActionListener());

		add(resetViewButton);
		add(addXViewButton);
		add(addYViewButton);
	}

	/**
	 * Crée les JSpinner de rotation pour chaque axe
	 */
	private void createAxisPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panelx = new JPanel();
		panelx.add(new JLabel("X"));
		sliderx = new JSlider(0, 180, 0);
		sliderx.setPreferredSize(new Dimension(120, 22));
		panelx.add(sliderx);
		panel.add(panelx);

		JPanel panely = new JPanel();
		panely.add(new JLabel("Y"));
		slidery = new JSlider(0, 180, 0);
		slidery.setPreferredSize(new Dimension(120, 22));
		panely.add(slidery);
		panel.add(panely);

		JPanel panelz = new JPanel();
		panelz.add(new JLabel("Z"));
		sliderz = new JSlider(0, 180, 0);
		sliderz.setPreferredSize(new Dimension(120, 22));
		panelz.add(sliderz);
		panel.add(panelz);

		sliderx.addChangeListener(controller.getXChangeListener(sliderx, slidery, sliderz));
		slidery.addChangeListener(controller.getYChangeListener(sliderx, slidery, sliderz));
		sliderz.addChangeListener(controller.getZChangeListener(sliderx, slidery, sliderz));

		sliderx.setToolTipText("Fait tourner l'objet sur son axe X (axe rouge)");
		slidery.setToolTipText("Fait tourner l'objet sur son axe Y (axe vert)");
		sliderz.setToolTipText("Fait tourner l'objet sur son axe Z (axe bleu)");

		add(panel);
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;

		if (notif.getType() == ModelNotification.OBJECT_LF_MODIFIED || notif.getType() == ModelNotification.TAB_CHANGED  && model.getObject() != null) {
			controller.disableSlidersListener(true);
			sliderx.setValue(Math.round(model.getObject().getRotation().getX()));
			slidery.setValue(Math.round(model.getObject().getRotation().getY()));
			sliderz.setValue(Math.round(model.getObject().getRotation().getZ()));
			controller.disableSlidersListener(false);
		}
	}
}
