package com.blender.views.tools;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import com.blender.controllers.LightController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.maths.Point;

/**
 * Vue des options de l'outil lumière
 *
 */
public class LightToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private JSpinner spinnerx;
	private JSpinner spinnery;
	private JSpinner spinnerz;
	private LightController controller = null;
	private ObjectsModel objModel;

	private JButton resetButton;
	private JButton colorButton;
	private JButton lightColorButton;
	
	private JCheckBox sunCheckbox;

	public LightToolView(ObjectsModel objectsModel) {
		objModel = objectsModel;
		objectsModel.addObserver(this);
		objModel.getProperties().addObserver(this);
		controller = new LightController(objModel, this);

		this.setBorder(BorderFactory.createTitledBorder("Lumiere"));
		JLabel info = new JLabel("Clic gauche", JLabel.CENTER);
		info.setToolTipText("Commande permettant d'utiliser cet outil");
		info.setPreferredSize(new Dimension(200, 20));
		Font f = info.getFont();
		info.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		add(info);
		
		setComponents();
		enableSpinners(!objModel.getProperties().getProperty(PropertiesModel.O_LIGHT_SUN));
	}

	/**
	 * Initialise les éléments de GUI
	 */
	private void setComponents() {
		
		PropertiesModel prop = objModel.getProperties();
		sunCheckbox = new JCheckBox(PropertiesModel.O_LIGHT_SUN, prop.getProperty(PropertiesModel.O_LIGHT_SUN));
		sunCheckbox.addItemListener(controller.getCheckBoxListener(sunCheckbox));
		sunCheckbox.setToolTipText("Passe la lumière en mode soleil : elle est fixée en haut à gauche de l'écran");
		
		add(sunCheckbox);

		createSpinnerPanel();
		
		spinnerx.addChangeListener(controller.getSpinnerXListener(spinnerx));
		spinnery.addChangeListener(controller.getSpinnerYListener(spinnery));
		spinnerz.addChangeListener(controller.getSpinnerZListener(spinnerz));

		colorButton = new JButton("Couleur objet");
		lightColorButton = new JButton("Couleur lumiere");
		resetButton = new JButton("Reset");
		addButton(resetButton, "Rétablis la position de la lumiere et les couleurs d'origine");
		addButton(colorButton, "Change la couleur de l'objet");
		addButton(lightColorButton, "Change la couleur de la source de lumiere");

		resetButton.addActionListener(controller.getResetListener());
		colorButton.addActionListener(controller.getColorListener(PropertiesModel.O_COLOR, "Couleur du modele"));
		lightColorButton.addActionListener(controller.getColorListener(PropertiesModel.O_LIGHT_COLOR, "Couleur de la lumiere"));
	}

	/**
	 * Permet d'ajouter les boutons avec le même formatage pour tous
	 * @param button le boutton à modifier
	 * @param toolTip toolTip du bouton
	 */
	private void addButton(JButton button, String toolTip) {
		button.setPreferredSize(new Dimension(135, 25));
		button.setToolTipText(toolTip);
		add(button);
	}

	/**
	 * Crée le panel contenant les spinners
	 */
	private void createSpinnerPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panelx = new JPanel();
		panelx.add(new JLabel("X"));
		spinnerx = new JSpinner();
		spinnerx.setPreferredSize(new Dimension(120, 22));
		spinnerx.setToolTipText("Déplace la source de lumière sur l'axe X");
		panelx.add(spinnerx);
		panel.add(panelx);

		JPanel panely = new JPanel();
		panely.add(new JLabel("Y"));
		spinnery = new JSpinner();
		spinnery.setPreferredSize(new Dimension(120, 22));
		spinnery.setToolTipText("Déplace la source de lumière sur l'axe Y");
		panely.add(spinnery);
		panel.add(panely);

		JPanel panelz = new JPanel();
		panelz.add(new JLabel("Z"));
		spinnerz = new JSpinner();
		spinnerz.setPreferredSize(new Dimension(120, 22));
		spinnerz.setToolTipText("Déplace la source de lumière sur l'axe Z");
		panelz.add(spinnerz);
		panel.add(panelz);

		add(panel);
	}

	/**
	 * Affiche le sélecteur de couleurs
	 * @param property nom de la proprieté couleur que l'on souhaite changer
	 * @param title titre de la fenêtre du selecteur de couleurs
	 * @return nouvelle couleur choisie
	 */
	public Color showColorChooser(String property, String title) {
		Color color = objModel.getProperties().getProperty(property, new Color(0));
		return JColorChooser.showDialog(this, title, color);
	}

	/**
	 * Permet d'activer ou désactiver les spinners de la position de la lumiere
	 * @param enable true si on les active, false sinon
	 */
	private void enableSpinners(boolean enable) {
		spinnerx.setEnabled(enable);
		spinnery.setEnabled(enable);
		spinnerz.setEnabled(enable);
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null)
			return;

		if ((notif.getType() == ModelNotification.TAB_CHANGED 
				|| notif.getType() == ModelNotification.OBJECT_LF_MODIFIED)
				&& objModel.getObject() != null) {
			boolean sunMode = objModel.getProperties().getProperty(PropertiesModel.O_LIGHT_SUN);
			enableSpinners(!sunMode);
			if (sunMode != sunCheckbox.isSelected())
				sunCheckbox.setSelected(sunMode);
			
			Point light = objModel.getObject().getLight();
			if ((int) light.getX() != (Integer) spinnerx.getValue())
				spinnerx.setValue((int)light.getX());
			if ((int) light.getY() != (Integer) spinnery.getValue())
				spinnery.setValue((int)light.getY());
			if ((int) light.getZ() != (Integer) spinnerz.getValue())
				spinnerz.setValue((int)light.getZ());
		}
	}
}
