package com.blender.views.tools;

import java.awt.Dimension;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.blender.controllers.MoveToolController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;

/**
 * Vue des options de l'outil de translation
 *
 */
public class MoveToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private JSpinner spinnerx;
	private JSpinner spinnery;
	private JSpinner spinnerz;
	private ObjectsModel model;
	private MoveToolController controller;
	
	public MoveToolView(ObjectsModel model) {
		this.model = model;
		controller = new MoveToolController(model);
		model.addObserver(this);
		
		setComponents();
	}
	
	/**
	 * Initialise les composants de GUI
	 */
	private void setComponents() {
		this.setBorder(BorderFactory.createTitledBorder("Translation"));
		
		JLabel info = new JLabel("Clic gauche", JLabel.CENTER);
		info.setPreferredSize(new Dimension(200, 20));
		info.setToolTipText("Commande permettant d'utiliser cet outil");
		Font f = info.getFont();
		info.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		add(info);
		
		JCheckBox wireMod = new JCheckBox("Mode fil de fer", true);
		wireMod.addActionListener(controller.getWireModeCheckboxListener());
		wireMod.setToolTipText("Utilise la vue fil de fer lors de la rotation");
		add(wireMod);
		
		createAxisPanel();
		
		JButton button = new JButton("Centrer");
		button.setPreferredSize(new Dimension(135, 25));
		button.setToolTipText("Centre l'objet");
		button.addActionListener(controller.getCenterButtonListener());
		add(button);
	}


	/**
	 * Crée les JSpinner de translation pour chaque axe
	 */
	private void createAxisPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panelx = new JPanel();
		panelx.add(new JLabel("X"));
		spinnerx = new JSpinner(new SpinnerNumberModel(0, -999999, 999999, 1));
		spinnerx.setPreferredSize(new Dimension(120, 22));
		spinnerx.setToolTipText("Déplace l'objet sur l'axe X");
		panelx.add(spinnerx);
		panel.add(panelx);
		
		JPanel panely = new JPanel();
		panely.add(new JLabel("Y"));
		spinnery = new JSpinner(new SpinnerNumberModel(0, -999999, 999999, 1));
		spinnery.setPreferredSize(new Dimension(120, 22));
		spinnery.setToolTipText("Déplace l'objet sur l'axe Y");
		panely.add(spinnery);
		panel.add(panely);
		
		JPanel panelz = new JPanel();
		panelz.add(new JLabel("Z"));
		spinnerz = new JSpinner(new SpinnerNumberModel(0, -999999, 999999, 1));
		spinnerz.setPreferredSize(new Dimension(120, 22));
		spinnerz.setToolTipText("Déplace l'objet sur l'axe Z");
		panelz.add(spinnerz);
		panel.add(panelz);
		
		spinnerx.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		spinnery.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		spinnerz.addChangeListener(controller.getChangeListener(spinnerx, spinnery, spinnerz));
		
		add(panel);
	}


	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		
		if (notif.getType() == ModelNotification.OBJECT_LF_MODIFIED || notif.getType() == ModelNotification.TAB_CHANGED  && model.getObject() != null) {
			spinnerx.setValue((int)model.getObject().getCenter().getX());
			spinnery.setValue((int)model.getObject().getCenter().getY());
			spinnerz.setValue((int)model.getObject().getCenter().getZ());
		}
	}
}
