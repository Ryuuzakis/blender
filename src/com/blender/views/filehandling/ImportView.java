package com.blender.views.filehandling;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.blender.controllers.FileHandlingController;
import com.blender.views.LoadingView;

/**
 * Vue d'import des modeles
 *
 */
public class ImportView extends AbstractHandling {
	private static final long serialVersionUID = -4782712548805153898L;
	
	protected JTextField name = new JTextField();
	
	public ImportView(LoadingView loadingView, FileHandlingController controller) {
		super("Importer un modèle", false, controller, loadingView);
		
		setContentPanels();
	}

	private void setContentPanels() {
		JPanel importPanel = new JPanel();
		importPanel.setLayout(new BorderLayout(5, 5));
		importPanel.setBorder(BorderFactory.createTitledBorder("Import de fichiers"));
		
		JPanel labelFieldsPanel = new JPanel();
		labelFieldsPanel.setLayout(new GridLayout(2, 1, 5, 5));
		labelFieldsPanel.add(new JLabel("Nom : "));
		labelFieldsPanel.add(new JLabel("Source : "));
		
		importPanel.add(labelFieldsPanel, BorderLayout.WEST);
		
		JPanel fields = new JPanel();
		fields.setLayout(new GridLayout(2, 1, 5, 5));
		fields.add(name);
		fields.add(path);
		
		importPanel.add(fields, BorderLayout.CENTER);
		importPanel.add(browse, BorderLayout.EAST);
		
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		buttonsPanel.add(submit);
		buttonsPanel.add(cancel);
		
		this.setLayout(new BorderLayout(5, 5));
		this.add(importPanel, BorderLayout.CENTER);
		this.add(buttonsPanel, BorderLayout.SOUTH);
		this.setPreferredSize(new Dimension(400, 100));
	}

	@Override
	public int getSelectionMode() {
		return JFileChooser.FILES_ONLY;
	}

	@Override
	public String getErrorMessage(byte errorType) {
		if (errorType == NAME_ERROR)
			return "Vous devez preciser un nom pour le modele";
		else // errorType == PATH_ERROR
			return "Vous devez selectionner un fichier a importer";
	}

	@Override
	public List<String> getModelNames() {
		List<String> list = new ArrayList<String>();
		list.add(name.getText());
		return list;
	}

	@Override
	public String getFilePath() {
		return path.getText();
	}

	@Override
	public String getSuccessMessage() {
		return "Import du modele reussi !";
	}

}
