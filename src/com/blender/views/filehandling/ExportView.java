package com.blender.views.filehandling;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import com.blender.controllers.FileHandlingController;
import com.blender.controllers.SearchController;
import com.blender.models.ModelNotification;
import com.blender.models.SearchModel;
import com.blender.views.LoadingView;

/**
 * Vue d'export des modeles
 *
 */
public class ExportView extends AbstractHandling implements Observer {
	private static final long serialVersionUID = -935321979969929643L;
	
	private JTextField searchField;
	private JList<String> modelList;
	
	private SearchModel searchModel;

	public ExportView(LoadingView loadingView, FileHandlingController controller) {
		super("Export de modèle(s)", true, controller, loadingView);
		
		this.setLayout(new BorderLayout(5, 5));
		this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		searchModel = new SearchModel();
		searchModel.addObserver(this);
		
		SearchController searchController = new SearchController(searchModel);
		
		JPanel searchPanel = new JPanel();
		searchPanel.setBorder(BorderFactory.createTitledBorder("Export de modèle(s)"));
		searchPanel.setLayout(new BorderLayout(5, 5));
		searchField = new JTextField();
		JButton advancedSearchBtn = new JButton("Recherche avancée");
		searchField.setPreferredSize(new Dimension(240, 20));
		
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel.add(searchField);
		panel.add(advancedSearchBtn);
		
		searchPanel.add(panel, BorderLayout.NORTH);
		
		modelList = new JList<String>();
		searchPanel.add(new JScrollPane(modelList), BorderLayout.CENTER);
		updateModelList();
		
		JPanel browsePanel = new JPanel();
		browsePanel.setLayout(new BorderLayout(5, 5));
		browsePanel.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
		browsePanel.add(new JLabel("Destination :"), BorderLayout.WEST);
		browsePanel.add(path, BorderLayout.CENTER);
		browsePanel.add(browse, BorderLayout.EAST);
		
		searchPanel.add(browsePanel, BorderLayout.SOUTH);
		
		this.add(searchPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		buttonsPanel.add(submit);
		buttonsPanel.add(cancel);
		
		this.add(buttonsPanel, BorderLayout.SOUTH);
		
		this.setPreferredSize(new Dimension(400, 300));
		
		searchField.getDocument().addDocumentListener(searchController.getDocumentListener(searchField));
		advancedSearchBtn.addActionListener(searchController.getAdvancedSearchActionListener());
	}

	@Override
	public int getSelectionMode() {
		return JFileChooser.DIRECTORIES_ONLY;
	}

	@Override
	public String getErrorMessage(byte errorType) {
		if (errorType == NAME_ERROR)
			return "Vous devez selectionner un (des) modèle(s) à exporter";
		else // errorType == PATH_ERROR
			return "Vous devez selectionner un dossier de destination";
	}

	@Override
	public List<String> getModelNames() {
		return modelList.getSelectedValuesList();
	}

	@Override
	public String getFilePath() {
		return path.getText();
	}

	@Override
	public String getSuccessMessage() {
		return "Export du modele reussi !";
	}
	
	private void updateModelList() {
		String[] models = null;
		models = new String[searchModel.getResults().size()];
		searchModel.getResults().toArray(models);
		modelList.setListData(models);
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		
		if (notif.getType() == ModelNotification.SEARCH_RESULT_UPDATED) {
			updateModelList();
		} else if (notif.getType() == ModelNotification.SEARCH_CHANGED) {
			searchField.setText((String) notif.getData());
		} else if (!notif.isTreated() && notif.getType() == ModelNotification.ERROR_MSG) {
			Exception e = (Exception)notif.getData();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			notif.setTreated(true);
		}
	}

}
