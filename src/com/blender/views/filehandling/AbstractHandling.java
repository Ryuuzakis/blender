package com.blender.views.filehandling;

import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.blender.controllers.FileHandlingController;
import com.blender.views.LoadingView;

/**
 * Vue regroupant les elements communs aux vues d'import et d'export 
 *
 */
public abstract class AbstractHandling extends JPanel {
	private static final long serialVersionUID = -105042461665892040L;
	
	protected JButton browse;
	protected JTextField path;
	protected JButton submit;
	protected JButton cancel;
	
	protected FileHandlingController controller;
	
	public static final byte NAME_ERROR = 0;
	public static final byte PATH_ERROR = 1;
	
	public AbstractHandling(String title, boolean currentlyExporting, FileHandlingController controller, LoadingView loadingView) {
		this.controller = controller;
		
		this.setName(title);
		
		this.path = new JTextField();
		this.browse = new JButton("Parcourir");
		
		String buttonMessage = currentlyExporting ? "Exporter" : "Importer";
		this.browse.addActionListener(controller.getBrowseController(getName(), buttonMessage));
		
		this.submit = new JButton("Valider");
		this.cancel = new JButton("Annuler");
		
		this.submit.addActionListener(controller.getSubmitController(loadingView, currentlyExporting));
		this.cancel.addActionListener(controller.getCancelController());
	}
	
	public void setPath(File file) {
		path.setText(file.getAbsolutePath());
	}
	
	public String getTitle() {
		return this.getName();
	}
	public abstract int getSelectionMode();
	public abstract String getErrorMessage(byte errorType);
	public abstract List<String> getModelNames();
	public abstract String getFilePath();
	public abstract String getSuccessMessage();

	public JButton getSendButton() {
		return submit;
	}
}
