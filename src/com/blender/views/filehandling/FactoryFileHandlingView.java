package com.blender.views.filehandling;

import com.blender.controllers.FileHandlingController;
import com.blender.views.LoadingView;

public class FactoryFileHandlingView {
	public static AbstractHandling getView(boolean export, LoadingView lview, FileHandlingController controller) {
		AbstractHandling view = null;
		
		if (export) {
			view = new ExportView(lview, controller);
		} else {
			view = new ImportView(lview, controller);
		}
		
		return view;
	}
}
