package com.blender.views;

import java.awt.CardLayout;
import java.awt.Component;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;
import com.blender.views.tools.InformationToolView;
import com.blender.views.tools.LightToolView;
import com.blender.views.tools.MoveToolView;
import com.blender.views.tools.OptionToolView;
import com.blender.views.tools.RotationToolView;
import com.blender.views.tools.ScaleToolView;

/**
 * Vue affichant les options de l'outil selectionne
 *
 */
public class ToolView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	
	public ToolView(ObjectsModel model) {
		super(new CardLayout());
		model.addObserver(this);
		
		add(new MoveToolView(model), Tool.TRANSLATION.getValue());
		add(new RotationToolView(model), Tool.ROTATION.getValue());
		add(new ScaleToolView(model), Tool.SCALE.getValue());
		add(new LightToolView(model), Tool.LIGHT.getValue());
		add(new OptionToolView(model), Tool.OPTIONS.getValue());
		add(new InformationToolView(model), Tool.INFORMATION.getValue());
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		if (notif.getType() == ModelNotification.TOOL_CHANGED) {
			((CardLayout)getLayout()).show(this, ((Tool)notif.getData()).getValue());
		}
	}
	
	public int getContentWidth() {
		for (Component comp : this.getComponents() ) {
	        if (comp.isVisible() == true) {
	        	int maxx = 0;
	        	for (Component cp : ((JPanel)comp).getComponents()) {
	        		if (cp.getPreferredSize().getWidth() > maxx)
	        			maxx = (int) cp.getPreferredSize().getWidth();
	        	}
	            return maxx;
	        }
	    }
		return (int) this.getPreferredSize().getWidth();
	}
}
