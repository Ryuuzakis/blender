package com.blender.views;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.blender.controllers.StatusBarController;
import com.blender.models.ObjectsModel;
import com.blender.utils.maths.transformable.TransformableBox;

/**
 * Vue representant une barre affichant des informations
 */
public class StatusBarView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;

	private ObjectsModel model;
	
	private StatusBarController controller;
	
	private JLabel informationLbl, informationRight;
	
	/**
	 * Crée un panel contenant des informations generales :
	 *  - Informations sur les elements
	 *  - Outil séléctionné
	 *  - Dimension de l'objet
	 *  - Volume de l'objet
	 * @param model Le model
	 */
	public StatusBarView(ObjectsModel model) {
		this.model = model;
		this.controller = new StatusBarController();
		model.addObserver(this);
		
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(1, 20));
		setBorder(BorderFactory.createEtchedBorder());
		
		informationLbl = new JLabel("");
		informationRight = new JLabel(getRightInformations());
		add(informationLbl, BorderLayout.LINE_START);
		add(informationRight, BorderLayout.LINE_END);
		
		Toolkit.getDefaultToolkit().addAWTEventListener(controller.getMouseMotionListener(this),
				AWTEvent.MOUSE_MOTION_EVENT_MASK);
	}
	
	/**
	 * Change le texte d'information (sur les élements)
	 * @param info Le nouveau texte
	 */
	public void setInformation(String info) {
		informationLbl.setText(info);
	}
	
	/**
	 * Renvoie les textes d'information de la partie droite de la barre
	 * @return Une chaine contenant les informations
	 */
	private String getRightInformations() {
		String tool = (model.getTool().getValue().charAt(0)+"").toUpperCase() + model.getTool().getValue().substring(1);
		float volume = 0;
		String size = "0 x 0 x 0";
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		if (model.getObject() != null) {
			TransformableBox box = model.getObject().getEnglobingBox();
			volume = box.getVolume();
			size = df.format(box.getDimensions()[0]) + " x " + df.format(box.getDimensions()[1]) +
					" x " + df.format(box.getDimensions()[2]);
		}
		
		return "Outil : " + tool + "         Volume : " +  df.format(volume) + " cm3   Taille : " + size + " cm";
	}
	
	@Override
	public void update(Observable arg0, Object n) {
		informationRight.setText(getRightInformations());
	}
}
