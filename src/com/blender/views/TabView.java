package com.blender.views;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.blender.controllers.TabController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;

/**
 * Vue d'un onglet
 *
 */
public class TabView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;

	private JLabel label;
	private ObjectsModel model;
	private boolean overCloseButton;

	public TabView(ObjectsModel model, String name, TabController controller) {
		super(new FlowLayout(FlowLayout.LEFT, 0, 2));
		this.model = model;
		overCloseButton = false;
		model.addObserver(this);
		addMouseListener(controller.getMiddleClickMouseListener());
		addMouseMotionListener(controller.getMouseMotionListener());
		
		add(label = new JLabel(name));
		add(Box.createHorizontalStrut(14));
		setOpaque(false);
	}

	@Override
	public void update(Observable o, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;

		if (notif.getType() == ModelNotification.OBJECT_CREATED) {
			Object[] datas = (Object[])notif.getData();
			if (this.getParent() == null) {
				model.deleteObserver(this);
				return;
			}
			JTabbedPane tabbedPane = (JTabbedPane) this.getParent().getParent();
			if (tabbedPane.getTabComponentAt((Integer)datas[0]) == this) {
				label.setText((String)datas[1]);
			}
		}
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		JTabbedPane tabbedPane = (JTabbedPane) this.getParent().getParent();
		Rectangle rect = g.getClipBounds();
		int cx = (int) (rect.getWidth() + rect.getX() - 10);
		int cy = (int) (rect.getHeight() - 5);

		if (tabbedPane.getTabComponentAt(tabbedPane.getSelectedIndex()) == this) {
			g.setColor(Color.WHITE);
		} else {
			g.setColor(new Color(73, 73, 73));
		}
		if (isOverCloseButton()) {
			g.setColor(new Color(242, 100, 100));
			int size = 13;
			g.fillOval(cx - 3, cy - size + 2, size, size);
			g.setColor(Color.WHITE);
		}
			
		g.setFont(new Font("Arial", Font.BOLD, 14));
		g.drawString("x", cx , cy);
	}

	public boolean isOverCloseButton() {
		return overCloseButton;
	}

	public void setOverCloseButton(boolean overCloseButton) {
		if (this.overCloseButton != overCloseButton) {
			this.overCloseButton = overCloseButton;
			repaint();
		}
	}
	
	public void updateLabelColor() {
		JTabbedPane tabbedPane = (JTabbedPane) this.getParent().getParent();
		int idx = tabbedPane.getSelectedIndex();
		if (idx < 0) return;
		if (tabbedPane.getTabComponentAt(idx) == this) {
			label.setForeground(Color.WHITE);
		} else {
			label.setForeground(Color.BLACK);
		}
	}

}
