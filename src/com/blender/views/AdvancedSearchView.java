package com.blender.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.blender.controllers.AdvancedSearchController;
import com.blender.models.SearchModel;
import com.blender.utils.AdvancedSearchParser;

/**
 * Vue permettant d'effectuer une recherche avancée dans les modèles
 *
 */
public class AdvancedSearchView extends JDialog {
	private static final long serialVersionUID = 1L;
	private DateFormat viewDateFmt = new SimpleDateFormat("dd/MM/yyyy");
	private HashMap<String, JTextField> textFields;
	private JComboBox<String> comboBox;
	private JCheckBox checkBox;
	private AdvancedSearchController controller;
	private SearchModel model;
	
	/**
	 * Crée une popup de recherche avancée
	 * @param model Le model de la recherche
	 */
	public AdvancedSearchView(SearchModel model) {
		super(Frame.getFrames()[0]);
		this.setTitle("Recherche avancée");
		this.model = model;
		
		controller = new AdvancedSearchController(model);
		textFields = new HashMap<String, JTextField>();
		setModal(true);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		add(panel);
		
		createOptions(panel);
		createButtons(panel);
		
		setResizable(false);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	/**
	 * Crée toutes les options de la recherche avancée
	 * @param panel Le panel sur lequel ajouter les options
	 */
	private void createOptions(JPanel panel) {
		JPanel pane = new JPanel();
		pane.setBorder(BorderFactory.createTitledBorder("Options"));
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		
		addOption(pane, "Nom de l'objet :", "name");
		addOption(pane, "Mots-clés", "mots_cles");
		addOption(pane, "auteur");
		addOption(pane, "date");
		addOption(pane, "Forme(s) : ", "formes");
		addSortOption(pane);
		
		AdvancedSearchParser parsedSearch = model.getParsedSearch();
		for (Entry<String, String> entry : parsedSearch.getFields().entrySet()) {
			JTextField textField = textFields.get(entry.getKey());
			if (textField != null) textField.setText(entry.getValue());
		}
		textFields.get("name").setText(parsedSearch.getName());
		comboBox.setSelectedItem(parsedSearch.getSortField());
		checkBox.setSelected(parsedSearch.isSortedInAsc());
		panel.add(pane, BorderLayout.CENTER);
	}
	
	/**
	 * Ajoute l'option de tri (combobox + checkbox pour l'ordre croissant ou non)
	 * @param panel Le panel sur lequel ajouter l'option
	 */
	private void addSortOption(JPanel panel) {
		JPanel pane = new JPanel();
		pane.setPreferredSize(new Dimension(355, 30));
		pane.setLayout(null);
		
		JLabel label = new JLabel("Trier par :");
		Font f = label.getFont();
		label.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		label.setBounds(5, 0, 300, 20);
		
		comboBox = new JComboBox<String>(new String[]{"nom", "auteur", "date"});
		comboBox.setBounds(100, 0, 100, 22);
		
		checkBox = new JCheckBox("Par ordre croissant");
		checkBox.setSelected(true);
		checkBox.setBounds(205, 0, 170, 22);
		
		pane.add(label);
		pane.add(comboBox);
		pane.add(checkBox);
		panel.add(pane);
	}
	
	/**
	 * Cree les boutons OK et Annuler et les ajoutes a un JPanel
	 * @param panel Le JPanel sur lequel mettre les boutons
	 */
	private void createButtons(JPanel panel) {
		JPanel pane = new JPanel();
		JButton cancelBtn = new JButton("Annuler");
		JButton okBtn = new JButton("OK");
		cancelBtn.setPreferredSize(new Dimension(80, 25));
		okBtn.setPreferredSize(new Dimension(80, 25));
		okBtn.addActionListener(controller.getOkActionListener(this));
		cancelBtn.addActionListener(controller.getCancelActionListener(this));
		getRootPane().setDefaultButton(okBtn);
		okBtn.requestFocus();
		
		pane.add(okBtn); 
		pane.add(cancelBtn);
		panel.add(pane, BorderLayout.SOUTH);
	}
	
	/**
	 * Ajoute une option a un jpanel (JLabel + JTextField)
	 * @param panel Le panel sur lequel ajouter l'option
	 * @param optionName Le nom de l'option
	 */
	private void addOption(JPanel panel, String optionName) {
		String text = (optionName.charAt(0) + "").toUpperCase() + optionName.substring(1) + " : ";
		addOption(panel, text, optionName);
	}
	
	/**
	 * Ajoute une option a un jpanel (JLabel + JTextField)
	 * @param panel Le panel sur lequel ajouter l'option
	 * @param text Le text associé aux champs
	 * @param optionName Le nom de l'option
	 */
	private void addOption(JPanel panel, String text, String optionName) {
		JPanel pane = new JPanel();
		pane.setPreferredSize(new Dimension(355, 30));
		pane.setLayout(null);
		JLabel label = new JLabel(text);
		Font f = label.getFont();
		label.setFont(f.deriveFont(f.getStyle() & ~Font.BOLD));
		pane.add(label);
		label.setBounds(5, 0, 300, 20);
		JTextField field = new JTextField();
		field.setPreferredSize(new Dimension(300, 25));
		field.setBounds(100, 0, 250, 22);
		pane.add(field);
		panel.add(pane);
		textFields.put(optionName, field);
	}
	
	/**
	 * Creer la chaine de recherche correspondant aux champs de saisie
	 * @return La chaine utilisable pour la recherche
	 * @throws ParseException si la date entrée est incorrecte
	 */
	public String getResult() throws ParseException {
		String res = "";
		for (Map.Entry<String, JTextField> entry : textFields.entrySet()) {
			String val = entry.getValue().getText();
			if (val.isEmpty()) continue;
			if (entry.getKey().equals("name")) {
				res = val + res;
			} else {
				if (entry.getKey().equals("date")) {
					try {
						viewDateFmt.parse(entry.getValue().getText());
					} catch (Exception e) {
						JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
						throw e;
					}
				}
				res += " " + entry.getKey() + "=" + entry.getValue().getText();
			}
		}
		String n = (String) comboBox.getSelectedItem();
		return res + (" tri=" + (checkBox.isSelected() ? "" : "!") + n).toLowerCase();
	}
}
