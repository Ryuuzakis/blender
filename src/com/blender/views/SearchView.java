package com.blender.views;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.blender.controllers.SearchController;
import com.blender.models.ModelNotification;
import com.blender.models.ObjectsModel;
import com.blender.models.SearchModel;
import com.blender.utils.database.DBModel;

/**
 * Vue permettant d'effectuer des recherches parmi les modèles et 
 * d'afficher les résultats
 *
 */
public class SearchView extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	public static final int BUTTONS_SIZE = 115;
	private static final int BUTTONS_GAP = 5;
	
	private ObjectsModel model;
	private SearchModel searchModel;
	private JPanel resultsPanel;
	private SearchController controller;
	private JTabbedPane tabbedPane;
	private JTextField textField;
	private List<String> modelNames;
	
	public SearchView(ObjectsModel model, JTabbedPane tabbedPane) {
		super();
		this.model = model;
		model.addObserver(this);
		searchModel = new SearchModel();
		searchModel.addObserver(this);
		controller = new SearchController(searchModel);
		this.tabbedPane = tabbedPane;
		
		initializeComponents();
		setComponents();
	}

	/**
	 * Initialise les différents composants de cette vue
	 */
	private void initializeComponents() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setPreferredSize(new Dimension(640, 480));
		
		textField = new JTextField("");
		textField.getDocument().addDocumentListener(controller.getDocumentListener(textField));
		textField.setMinimumSize(new Dimension(200, 27));
		textField.setPreferredSize(new Dimension(200, 27));
		textField.setToolTipText("Recherche d'un modèle 3D");
		
		resultsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, BUTTONS_GAP, BUTTONS_GAP));
		resultsPanel.setPreferredSize(new Dimension(20, 100));
		resultsPanel.addComponentListener(controller.getComponentListener(this));
	}
	
	/**
	 * Organise graphiquement les composants
	 */
	private void setComponents() {
		JPanel panel1 = new JPanel();
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));
		panel1.setBorder(BorderFactory.createEmptyBorder(30, 10, 30, 10));
		panel1.setMinimumSize(new Dimension(200, 80));
		panel1.setMaximumSize(new Dimension(800, 80));
		
		panel1.add(textField);
		panel1.add(Box.createRigidArea(new Dimension(10, 1)));
		
		JButton btn = new JButton("Recherche avancée");
		btn.setToolTipText("Ouvre la fenêtre de saisie de critères de recherche");
		btn.addActionListener(controller.getAdvancedSearchActionListener());
		panel1.add(btn);
		
		modelNames = searchModel.getResults();
		for (String n : modelNames) {
			addResultButton(n);
		}
		adaptToSize();
		
		JScrollPane scrollPane = new JScrollPane(resultsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(15);
		
		add(panel1);
		add(scrollPane);
	}

	/**
	 * Crée un nouveau bouton correspondant à un objet
	 * @param name nom de l'objet
	 */
	private void addResultButton(String name) {
		ImageIcon image = null;
		try {
			String path = DBModel.getPreview(name);
			if (path == null) path = "data/icons/nopreview.png";
			image = new ImageIcon(path);
		} catch (Exception e) {}
		JButton button = new JButton(name, image);
		button.setVerticalTextPosition(SwingConstants.BOTTOM);
		button.setHorizontalTextPosition(SwingConstants.CENTER);
		button.setIconTextGap(-10);
		button.addActionListener(controller.getButtonActionListener(model, name, tabbedPane, this));
		button.setPreferredSize(new Dimension(BUTTONS_SIZE, BUTTONS_SIZE));
		button.setToolTipText("Clic gauche pour afficher le modèle \""+name+"\"");
		resultsPanel.add(button);
	}
	
	/**
	 * Adapte la taille du panel en fonction de la place disponible
	 */
	public void adaptToSize() {
		int nbButtonsPerLine = (int) Math.floor(resultsPanel.getSize().getWidth() * 1f/(BUTTONS_SIZE+ BUTTONS_GAP));
		int nbLines = (int) Math.ceil(resultsPanel.getComponentCount() * 1f / nbButtonsPerLine);
		resultsPanel.setPreferredSize(new Dimension(20, nbLines * (BUTTONS_SIZE + BUTTONS_GAP) + BUTTONS_GAP));
		resultsPanel.revalidate();
	}

	@Override
	public void update(Observable arg0, Object n) {
		ModelNotification notif = (ModelNotification)n;
		if (n == null) return;
		
		if (notif.getType() == ModelNotification.SEARCH_RESULT_UPDATED) {
			resultsPanel.removeAll();
			for (String name : searchModel.getResults()) {
				addResultButton(name);
			}
			adaptToSize();
			resultsPanel.validate();
			resultsPanel.repaint();
		} else if (notif.getType() == ModelNotification.SEARCH_CHANGED) {
			textField.setText((String) notif.getData());
		} else if (!notif.isTreated() && notif.getType() == ModelNotification.ERROR_MSG) {
			Exception e = (Exception)notif.getData();
			JOptionPane.showMessageDialog(this, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			notif.setTreated(true);
		} else if (notif.getType() == ModelNotification.DB_UPDATED) {
			searchModel.search(textField.getText());
		} else if (notif.getType() == ModelNotification.PREVIEW_CHANGED) {
			for (Component comp : resultsPanel.getComponents()) {
				JButton btn = (JButton) comp;
				((ImageIcon)btn.getIcon()).getImage().flush();
			}
		}
	}

}
