package com.blender.views;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import com.blender.controllers.MenuBarController;
import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;

/**
 * Classe de la barre de menus
 *
 */
public class MenuBarView extends JMenuBar {
	private static final long serialVersionUID = 1L;
	
	private JMenu fileMenu;
	private JMenu toolMenu;
	private JMenu optionsMenu;
	private JMenu helpMenu;
	
	private JMenuItem itemImport;
	private JMenuItem itemExport;
	private JMenuItem itemExit;
	private JMenuItem[] toolItems;
	private JMenuItem itemReset;
	private JMenuItem itemAbout;
	private JMenuItem itemRegenPreviews;
	private JMenuItem itemSlices;
	
	private MenuBarController controller;

	public MenuBarView(ObjectsModel model) {
		controller = new MenuBarController(model);
		
		initializeMenuItems();
		initializeMenus();
		initializeView();
	}

	private void initializeView() {
		add(fileMenu);
		add(toolMenu);
		add(optionsMenu);
		add(helpMenu);
	}

	private void initializeMenus() {
		fileMenu = new JMenu("Fichier");
		toolMenu = new JMenu("Outils");
		optionsMenu = new JMenu("Options");
		helpMenu = new JMenu("Aide");
		
		fileMenu.add(itemImport);
		fileMenu.add(itemExport);
		fileMenu.addSeparator();
		fileMenu.add(itemExit);
		
		helpMenu.add(itemAbout);
		
		for (JMenuItem item : toolItems) {
			toolMenu.add(item);
		}
		optionsMenu.add(itemReset);
		optionsMenu.add(itemRegenPreviews);
		optionsMenu.add(itemSlices);
	}

	private void initializeMenuItems() {
		itemImport = new JMenuItem("Importer un modèle...");
		itemExport = new JMenuItem("Exporter un modèle...");
		itemAbout = new JMenuItem("A propos...");
		itemReset = new JMenuItem("Rétablir les options par défaut");
		itemExit = new JMenuItem("Fermer");
		itemRegenPreviews = new JMenuItem("Régenerer les apercus des modèles");
		itemSlices = new JMenuItem("Découper le modèle en tranches");
		
		toolItems = new JMenuItem[Tool.values().length];
		for (int i = 0; i < toolItems.length; ++i) {
			String v = Tool.values()[i].getValue();
			v = (v.charAt(0) + "").toUpperCase() + v.substring(1);
			toolItems[i] = new JMenuItem(v);
			toolItems[i].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1 + i, 0));
			toolItems[i].addActionListener(controller.getToolItemActionListener(Tool.values()[i]));
			toolItems[i].setToolTipText("Ouvre la vue de l'outil " + toolItems[i].getText().toLowerCase());
		}
		
		int mask = Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask();
		
		itemImport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, mask));
		itemExport.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, mask));
		itemReset.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, mask));
		itemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_DOWN_MASK));
		itemRegenPreviews.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, mask));
		itemSlices.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, mask));
		
		itemImport.addActionListener(controller.getImportItemActionListener());
		itemExport.addActionListener(controller.getExportItemActionListener());
		itemExit.addActionListener(controller.getExitItemActionListener());
		itemReset.addActionListener(controller.getResetActionListener());
		itemAbout.addActionListener(controller.getAboutItemActionListener());
		itemRegenPreviews.addActionListener(controller.getRegenPreviewsActionListener());
		itemSlices.addActionListener(controller.getSlicesActionListener());
		
		itemImport.setToolTipText("Importe un modèle 3D dans la base de données");
		itemExport.setToolTipText("Exporte un modèle 3D de la base de données vers un fichier");
		itemExit.setToolTipText("Ferme le logiciel");
		itemReset.setToolTipText("Rétablit toutes les options de configuration par défaut du logiciel");
		itemAbout.setToolTipText("Affiche les auteurs et informations relatives au logiciel");
		itemRegenPreviews.setToolTipText("Régénère les apercus des modèles avec les couleurs actuelles");
		itemSlices.setToolTipText("Permet de découper en tranches le modèle pour impression");
	}

}
