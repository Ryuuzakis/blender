package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.utils.maths.Point;

/**
 * Controleur de l'outil de translation
 *
 */
public class MoveToolController {
	private ObjectsModel model;
	
	public MoveToolController(ObjectsModel model) {
		this.model = model;
	}
	
	/**
	 * Ecoute le changement d'un JSpinner permettant de translater l'objet
	 * @param x Le JSpinner des x
	 * @param y Le JSpinner des y
	 * @param z Le JSpinner des z
	 * @return
	 */
	public ChangeListener getChangeListener(final JSpinner x, final JSpinner y, final JSpinner z) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				float nx = ((Integer) x.getValue()).floatValue();
				float ny = ((Integer) y.getValue()).floatValue();
				float nz = ((Integer) z.getValue()).floatValue();
				Point pt = model.getObject().getCenter();
				if (e.getSource() != x) nx = pt.getX();
				if (e.getSource() != y) ny = pt.getY();
				if (e.getSource() != z) nz = pt.getZ();
				model.moveTo(nx, ny, nz);
			}
		};
	}
	
	/**
	 * Ecoute le bouton pour centrer l'objet
	 * @return
	 */
	public ActionListener getCenterButtonListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.moveTo(model.getMaxWidth()/2, model.getMaxHeight()/2, 0);
			}
		};
	}

	/**
	 * Ecoute la checkbox pour la vue en fil de fer
	 * @return
	 */
	public ActionListener getWireModeCheckboxListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.getTool().enableWireMode(!model.getTool().useWireMode());
			}
		};
	}
	
}
