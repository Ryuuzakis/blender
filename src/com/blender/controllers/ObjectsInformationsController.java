package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.blender.models.ObjectsModel;
import com.blender.views.ObjectInformationsView;

/**
 * Controleur de l'outil lumiere
 *
 */
public class ObjectsInformationsController {
	
	ObjectsModel objectsModel;
	
	public ObjectsInformationsController(ObjectsModel objectsModel) {
		this.objectsModel = objectsModel;
	}

	public ActionListener getSubmitListener(final ObjectInformationsView view) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					view.updateInfos();
					view.getInfos().saveToDB();
					objectsModel.updateDB();
				} catch (Exception e1) {
					view.showError(e1);
				}
				view.dispose();
			}
		};
	}
	
	
	public ActionListener getCancelListener(final ObjectInformationsView view) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.dispose();
			}
		};
	}
}
