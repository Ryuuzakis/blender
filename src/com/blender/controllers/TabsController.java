package com.blender.controllers;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.views.TabView;

/**
 * Controleur associe a TabsView
 *
 */
public class TabsController {
	private ObjectsModel model;
	
	public TabsController(ObjectsModel model) {
		this.model = model;
	}
	
	/**
	 * Ecoute la selection des onglets pour mettre a jour les donnees et detecter l'ajout d'onglet
	 * @return
	 */
	public ChangeListener getChangeListener() {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				JTabbedPane tabbedPane = ((JTabbedPane) e.getSource());
				int sel = tabbedPane.getSelectedIndex();
				// Si c'est le bouton pour ajouter un onglet
				if (sel == tabbedPane.getTabCount()-1) {
					model.addObject();
				}
				
				for (int i = 0; i < tabbedPane.getTabCount()-1; ++i) {
					((TabView)tabbedPane.getTabComponentAt(i)).updateLabelColor();
				}
				model.setShowedObject(sel);
			}
		};
	}

}
