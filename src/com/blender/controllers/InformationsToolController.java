package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.blender.models.ObjectsModel;
import com.blender.utils.maths.transformable.TransformableObject3D;
import com.blender.views.ObjectInformationsView;

/**
 * Controleur de l'outil lumiere
 *
 */
public class InformationsToolController {
	
	ObjectsModel objectsModel;
	
	public InformationsToolController(ObjectsModel objectsModel) {
		this.objectsModel = objectsModel;
	}
	
	public ActionListener getModifyInformationsListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				TransformableObject3D obj = objectsModel.getObject();
				if (obj == null) return;
				new ObjectInformationsView(objectsModel, obj.getModelName());
			}
		};
	}
}
