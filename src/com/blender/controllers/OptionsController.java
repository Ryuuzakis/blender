package com.blender.controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.PropertiesModel;
import com.blender.views.tools.OptionToolView;

/**
 * Controleur de l'outil d'options
 *
 */
public class OptionsController {
	
	private PropertiesModel properties;
	private OptionToolView view;

	public OptionsController(OptionToolView view, PropertiesModel properties) {
		this.properties = properties;
		this.view = view;
	}
	
	public ActionListener getDrawingAlgoListener(final JRadioButton button) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				properties.setDrawingAlgorithm(button.getText());
			}
		};
	}

	public ItemListener getCheckboxBooleanListener(final JCheckBox checkbox) {
		return new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				properties.setProperty(checkbox.getText(), checkbox.isSelected());
			}
		};
	}

	public ActionListener getButtonColorListener(final JButton color) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Color c = properties.getProperty(color.getText(), new Color(0));
				Color newColor = JColorChooser.showDialog(view, color.getText(), c);
				if (newColor == null)
					newColor = c;
				properties.setProperty(color.getText(), newColor);
			}
		};
	}

	public ActionListener getButtonSaveListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				properties.save();
			}
		};
	}
	
	public ActionListener getButtonResetListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				properties.resetDefault();
			}
		};
	}

	public ChangeListener getSliderListener(final JSlider slider) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				properties.setProperty(slider.getName(), slider.getValue());
			}
		};
	}
}
