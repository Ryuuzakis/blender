package com.blender.controllers;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;
import com.blender.utils.maths.Point;

/**
 * Controleur associe a ObjectView
 *
 */
public class ObjectController {
	/**
	 * Le model a controler
	 */
	private ObjectsModel model;
	
	public ObjectController(ObjectsModel model) {
		this.model = model;
	}
	
	/**
	 * Ecoute les touches du clavier pour gerer le deplacement de l'objet
	 * @return
	 */
	public KeyListener getKeyListener() {
		return new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) { }
			
			@Override
			public void keyReleased(KeyEvent e) { }
			
			@Override
			public void keyPressed(KeyEvent e) {
				int x = (e.getKeyCode() == KeyEvent.VK_LEFT ? -1 : 0);
				if (e.getKeyCode() == KeyEvent.VK_RIGHT) x = 1;
				
				int y = (e.getKeyCode() == KeyEvent.VK_UP ? -1 : 0);
				if (e.getKeyCode() == KeyEvent.VK_DOWN) y = 1;
				model.move(x, y, 0);
			}
		};
	}
	
	/**
	 * Ecoute le scroll de la souris pour gerer le redimensionnement de l'objet
	 * @return
	 */
	public MouseWheelListener getMouseWheelListener() {
		return new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (model.isUsingTool(Tool.SCALE) || e.isControlDown()) {
					float coef = (e.getUnitsToScroll() < 0 ? 1.1f : 0.9f);
					model.scale(coef, coef, coef);
				}
			}
		};
	}
	
	/**
	 * Ecoute la traine de la souri pour gerer la rotation de l'objet
	 * @return
	 */
	public MouseMotionListener getMouseMotionListener() {
		return new MouseMotionListener() {
			private float lastMouseX = 0;
			private float lastMouseY = 0;
			@Override
			public void mouseMoved(MouseEvent e) {
				if (!SwingUtilities.isLeftMouseButton(e)) {
					lastMouseX = e.getX();
					lastMouseY = e.getY();
				}
				
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
				if (e.isControlDown()) {
					model.setMode(ObjectsModel.MODE_WIRE);
					if (SwingUtilities.isRightMouseButton(e)) {
						model.move(e.getX() - lastMouseX, e.getY() - lastMouseY, 0);
					} else if (SwingUtilities.isLeftMouseButton(e)) {
						float dx = (lastMouseX - e.getX());
						float dy = -(lastMouseY - e.getY());
						model.rotate(dy, dx, 0);
					}
					lastMouseX = e.getX();
					lastMouseY = e.getY();
					return;
				}
				if (!SwingUtilities.isLeftMouseButton(e)) return;
				if (model.getTool().useWireMode()) model.setMode(ObjectsModel.MODE_WIRE);
				if (model.isUsingTool(Tool.TRANSLATION)) {
					model.move(e.getX() - lastMouseX, e.getY() - lastMouseY, 0);
				} else if (model.isUsingTool(Tool.ROTATION)) {
					float dx = (lastMouseX - e.getX());
					float dy = -(lastMouseY - e.getY());
					model.rotate(dy, dx, 0);
				} else if (model.isUsingTool(Tool.LIGHT)) {
					Point p = model.getObject().getLight();
					float dx = p.getX() + (e.getX() - p.getX());
					float dy = p.getY() + (e.getY() - p.getY());
					model.moveLightPosition(dx, dy, p.getZ());
				}
				
				lastMouseX = e.getX();
				lastMouseY = e.getY();
					
			}
		};
	}
	
	/**
	 * Ecoute le clique de la souris pour gerer le deplacement
	 * @return
	 */
	public MouseListener getMouseListener() {
		return new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1 && model.getTool().useWireMode() || e.isControlDown())
					model.setMode(ObjectsModel.MODE_FACES);
			}
			
			@Override
			public void mousePressed(MouseEvent e) { }
			
			@Override
			public void mouseExited(MouseEvent arg0) { }
			
			@Override
			public void mouseEntered(MouseEvent arg0) { }
			
			@Override
			public void mouseClicked(MouseEvent arg0) { }
		};
	}
	
	/**
	 * Ecoute le redimensionnement de la zone de dessin pour adapter le model a sa taille
	 * @return
	 */
	public ComponentListener getComponentListener() {
		return new ComponentListener() {
			//private boolean firstResize = true;
			
			@Override
			public void componentShown(ComponentEvent arg0) { }
			
			@Override
			public void componentResized(ComponentEvent e) {
				// TODO : Conserver un ratio ?
				
				int width = e.getComponent().getWidth();
				int height = e.getComponent().getHeight();
				
				model.setMaxSize(width, height);
				/*
				// Si c'est le premier redimensionnement, on adapte le model au panel
				if (firstResize) {
					model.adaptTo(width, height);
					firstResize = false;
				}*/
			}
			
			@Override
			public void componentMoved(ComponentEvent arg0) { }
			
			@Override
			public void componentHidden(ComponentEvent arg0) { }
		};
	}

}
