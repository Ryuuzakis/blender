package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import com.blender.models.FileHandlingModel;
import com.blender.models.ObjectsModel;
import com.blender.views.FileHandlingView;
import com.blender.views.LoadingView;
import com.blender.views.ObjectInformationsView;

/**
 * Controleur de la vue d'import/export
 *
 */
public class FileHandlingController {

	private FileHandlingView view;
	private FileHandlingModel model;
	private ObjectsModel objModel;

	public FileHandlingController(FileHandlingView view, FileHandlingModel model, 
			ObjectsModel objModel) {
		this.view = view;
		this.model = model;
		this.objModel = objModel;
	}

	public ActionListener getBrowseController(final String title, final String approveButtonText) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				view.showFileChooser(title, approveButtonText);
			}
		};
	}

	public ActionListener getSubmitController(final LoadingView loadingView, final boolean currentlyExporting) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadingView.start();
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						String path = view.getFilePath();
						if (path == null) 
							return;
						
						List<String> modelNames = view.getModelsNames();
						if (modelNames == null) 
							return;
						
						model.setFilePath(path);
						model.setModelName(modelNames);
						model.moveModel(currentlyExporting, objModel);
						loadingView.close();
						if (!currentlyExporting) new ObjectInformationsView(objModel, modelNames.get(0));
					}
				}).start();
			}
		};
	}

	public ActionListener getCancelController() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				view.setVisible(false, false);
			}
		};
	}
}
