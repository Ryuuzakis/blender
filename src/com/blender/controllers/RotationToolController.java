package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;

/**
 * Controleur de l'outil de rotation
 *
 */
public class RotationToolController {
	private ObjectsModel model;
	private boolean slidersDisabled = false;
	
	public RotationToolController(ObjectsModel model) {
		this.model = model;
	}

	/**
	 * Ecoute le bouton pour remettre l'objet a son etat initial
	 * @return
	 */
	public ActionListener getFrontButtonActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.rotateTo(0, 0, 0);
			}
		};
	}
	
	/**
	 * Ecoute le bouton pour ajouter 90° horizontalement
	 * @return
	 */
	public ActionListener getAddXButtonActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.rotate(0, 90, 0);
			}
		};
	}
	
	/**
	 * Ecoute le bouton pour ajouter 90° verticalement
	 * @return
	 */
	public ActionListener getAddYButtonActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.rotate(90, 0, 0);
			}
		};
	}

	/**
	 * Ecoute les JSlider permettant de modifier la rotation
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public ChangeListener getXChangeListener(final JSlider x, final JSlider y, final JSlider z) {
		return new ChangeListener() {
			private int ox;
			@Override
			public void stateChanged(ChangeEvent e) {
				int nx = ((Integer) x.getValue());
				int ny = ((Integer) y.getValue());
				int dx = nx - ox;
				ox = nx;
				if (slidersDisabled) return;
				if (!x.getValueIsAdjusting() && !y.getValueIsAdjusting() && !z.getValueIsAdjusting())
					model.setMode(ObjectsModel.MODE_FACES);
				if (dx != 0) {
					if (model.getTool().useWireMode())
						model.setMode(ObjectsModel.MODE_WIRE);
					model.rotateTo(0, nx, 0);
					model.rotate(ny, 0, 0);
				}
			}
		};
	}
	
	/**
	 * Ecoute les JSlider permettant de modifier la rotation
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public ChangeListener getYChangeListener(final JSlider x, final JSlider y, final JSlider z) {
		return new ChangeListener() {
			private int oy;
			@Override
			public void stateChanged(ChangeEvent e) {
				int nx = ((Integer) x.getValue());
				int ny = ((Integer) y.getValue());
				int dy = ny - oy;
				oy = ny;
				if (slidersDisabled) return;
				if (!x.getValueIsAdjusting() && !y.getValueIsAdjusting() && !z.getValueIsAdjusting())
					model.setMode(ObjectsModel.MODE_FACES);
				if (dy != 0) {
					if (model.getTool().useWireMode())
						model.setMode(ObjectsModel.MODE_WIRE);
					model.rotateTo(ny, 0, 0);
					model.rotate(0, nx, 0);
				}
			}
		};
	}
	
	/**
	 * Ecoute les JSlider permettant de modifier la rotation
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public ChangeListener getZChangeListener(final JSlider x, final JSlider y, final JSlider z) {
		return new ChangeListener() {
			private int oz;
			@Override
			public void stateChanged(ChangeEvent e) {
				int nx = ((Integer) x.getValue());
				int nz = ((Integer) z.getValue());
				int dz = nz - oz;
				oz = nz;
				if (slidersDisabled) return;
				if (!x.getValueIsAdjusting() && !y.getValueIsAdjusting() && !z.getValueIsAdjusting())
					model.setMode(ObjectsModel.MODE_FACES);
				if (dz != 0) {
					if (model.getTool().useWireMode())
						model.setMode(ObjectsModel.MODE_WIRE);
					model.rotateTo(nz, 0, 0);
					model.rotate(0, 0, nx);
				}
			}
		};
	}

	/**
	 * Ecoute la checkbox pour la vue en fil de fer
	 * @return
	 */
	public ActionListener getWireModeCheckboxListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.getTool().enableWireMode(!model.getTool().useWireMode());
			}
		};
	}

	public void disableSlidersListener(boolean b) {
		slidersDisabled = b;
		
	}
	
}
