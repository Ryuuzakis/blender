package com.blender.controllers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JTabbedPane;

import com.blender.models.ObjectsModel;
import com.blender.views.TabView;

/**
 * Controleur associe a TabView
 *
 */
public class TabController {
	private ObjectsModel model;

	public TabController(ObjectsModel model) {
		this.model = model;
	}

	/**
	 * Ecoute l'appui sur la croix d'un onglet pour le fermer
	 * @return
	 */
	/*
	public ActionListener getCloseButtonActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton)e.getSource();
				TabView tab = (TabView) button.getParent();
				JTabbedPane tabbedPane = (JTabbedPane) tab.getParent().getParent();
				model.removeObject(tabbedPane.indexOfTabComponent(tab));
			}
		};
	}*/
	
	/**
	 * Ecoute la pression sur la molette (pour fermer l'onglet)
	 * @return
	 */
	public MouseListener getMiddleClickMouseListener() {
		return new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				TabView tab = (TabView) e.getComponent();
				JTabbedPane tabbedPane = (JTabbedPane) tab.getParent().getParent();
				if (e.getButton() == MouseEvent.BUTTON2) {
					int idx = tabbedPane.indexOfTabComponent(tab);
					model.removeObject(idx);
				} else {
					if (tab.isOverCloseButton()) 
						model.removeObject(tabbedPane.indexOfTabComponent(tab));
					else
						tabbedPane.setSelectedIndex(tabbedPane.indexOfTabComponent(tab));
				}
			}
			@Override
			public void mouseExited(MouseEvent e) {
				TabView tab = (TabView) e.getComponent();
				tab.setOverCloseButton(false);
				JTabbedPane tabbedPane = (JTabbedPane) tab.getParent().getParent();
				tabbedPane.dispatchEvent(e);
			}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {}
		};
	}

	public MouseMotionListener getMouseMotionListener() {
		return new MouseMotionListener() {
			private MouseEvent createFakeMouseEvent(MouseEvent e, JTabbedPane pane) {
				MouseEvent event = new MouseEvent(e.getComponent(),
						e.getID(), e.getWhen(), e.getModifiers(), 
						(int) (e.getXOnScreen() - pane.getLocationOnScreen().getX()),
						(int) (e.getYOnScreen() - pane.getLocationOnScreen().getY()),
						e.getXOnScreen(), e.getYOnScreen(),
						e.getClickCount(), e.isPopupTrigger(), e.getButton());
				
				return event;
			}
			
			@Override
			public void mouseMoved(MouseEvent e) {
				TabView tab = (TabView) e.getComponent();
				JTabbedPane tabbedPane = (JTabbedPane) tab.getParent().getParent();
				tabbedPane.dispatchEvent(createFakeMouseEvent(e, tabbedPane));
				
				tab.setOverCloseButton((e.getX() > tab.getBounds().getWidth() - 10));
			}
			
			@Override
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		};
	}

}
