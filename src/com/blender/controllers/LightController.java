package com.blender.controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.maths.Point;
import com.blender.views.tools.LightToolView;

/**
 * Controleur de l'outil lumiere
 *
 */
public class LightController {
	
	ObjectsModel objectsModel;
	LightToolView lightToolView;
	
	public LightController(ObjectsModel objectsModel, LightToolView tool) {
		this.objectsModel = objectsModel;
		lightToolView = tool;
	}
	
	public ActionListener getResetListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				objectsModel.resetLight();
			}
		};
	}
	
	public ChangeListener getSpinnerXListener(final JSpinner x) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int nx = (Integer) x.getValue();
				Point p = objectsModel.getObject().getLight();
				if (nx != (int) p.getX())
					objectsModel.moveLightPosition(nx, p.getY(), p.getZ());
			}
		};
	}
	public ChangeListener getSpinnerYListener(final JSpinner y) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				int ny = (Integer) y.getValue();
				Point p = objectsModel.getObject().getLight();
				if (ny != (int) p.getX())
					objectsModel.moveLightPosition(p.getX(), ny, p.getZ());
			}
		};
	}
	public ChangeListener getSpinnerZListener(final JSpinner z) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				float nz = (Integer) z.getValue();
				Point p = objectsModel.getObject().getLight();
				if (nz != (int) p.getZ())
					objectsModel.moveLightPosition(p.getX(), p.getY(), nz);
			}
		};
	}

	public ActionListener getColorListener(final String property, final String title) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Color tmp = lightToolView.showColorChooser(property, title);
				if (tmp != null)
					objectsModel.getProperties().setProperty(property, tmp);
			}
		};
	}

	public ItemListener getCheckBoxListener(final JCheckBox sunCheckbox) {
		return new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				objectsModel.getProperties().setProperty(PropertiesModel.O_LIGHT_SUN, sunCheckbox.isSelected());
			}
		};
	}
}
