package com.blender.controllers;

import java.awt.AWTEvent;
import java.awt.event.AWTEventListener;

import javax.swing.JComponent;

import com.blender.views.StatusBarView;

/**
 * Controleur associe a StatusBarView
 *
 */
public class StatusBarController {
	
	public StatusBarController() { }
	
	/**
	 * Ecoute le mouvement de la souri pour mettre a jour l'information sur le composant survol�
	 * @param statusBarView 
	 * @return
	 */
	public AWTEventListener getMouseMotionListener(final StatusBarView statusBarView) {
		return new AWTEventListener() {
			@Override
			public void eventDispatched(AWTEvent e) {
				String str = "";
				if (e.getSource() instanceof JComponent) {
					str = ((JComponent)e.getSource()).getToolTipText();
				}
				statusBarView.setInformation(str);
			}
		};
	}
}
