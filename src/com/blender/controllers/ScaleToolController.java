package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.utils.maths.Point;

/**
 * Controleur de l'outil d'homothetie
 *
 */
public class ScaleToolController {
	private ObjectsModel model;
	
	public ScaleToolController(ObjectsModel model) {
		this.model = model;
	}
	
	/**
	 * Ecoute les JSpinner permettant de redimensionner l'objet
	 * @param x Le JSpinner en x
	 * @param y Le JSpinner en y
	 * @param z Le JSpinner en z
	 * @return
	 */
	public ChangeListener getChangeListener(final JSpinner x, final JSpinner y, final JSpinner z) {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				float nx = ((Double) x.getValue()).floatValue();
				float ny = ((Double) y.getValue()).floatValue();
				float nz = ((Double) z.getValue()).floatValue();
				
				Point pt = model.getObject().getScale();
				if (e.getSource() != x) nx = pt.getX();
				if (e.getSource() != y) ny = pt.getY();
				if (e.getSource() != z) nz = pt.getZ();
				model.scaleTo(nx, ny, nz);
			}
		};
	}
	
	/**
	 * Ecoute le bouton permettant de redimensionner l'objet pour qu'il corresponde a l'espace
	 * @return
	 */
	public ActionListener getActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.adaptTo(model.getMaxWidth(), model.getMaxHeight());
			}
		};
	}
	
}
