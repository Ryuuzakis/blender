package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.blender.models.SearchModel;
import com.blender.views.AdvancedSearchView;

/**
 * Controleur associe a AdvancedSearchView
 *
 */
public class AdvancedSearchController {
	/**
	 * Le model a controler
	 */
	private SearchModel searchModel;
	
	public AdvancedSearchController(SearchModel searchModel) {
		this.searchModel = searchModel;
	}

	public ActionListener getOkActionListener(final AdvancedSearchView view) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					searchModel.setSearch(view.getResult());
					view.dispose();
				} catch (Exception ex) {}
			}
		};
	}
	
	public ActionListener getCancelActionListener(final AdvancedSearchView view) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.dispose();
			}
		};
	}

}
