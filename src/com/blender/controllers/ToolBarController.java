package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.AbstractAction;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import com.blender.views.ToolBarView;

/**
 * Controleur associe a TabsView
 *
 */
public class ToolBarController {
	
	public ToolBarController() { }
	
	/**
	 * Ecoute les changements d'orientation de la toolbar pour l'adapter
	 * @param view La vue contenant la toolbar
	 * @return
	 */
	public PropertyChangeListener getPropertyChangeListener(final ToolBarView view) {
		return new PropertyChangeListener() {
			
			@Override
			public void propertyChange(PropertyChangeEvent e) {
				if (e.getPropertyName().equals("orientation")) {
					if ((Integer) (e.getNewValue()) == JToolBar.HORIZONTAL) {
						view.resizeHorizontally();
					} else {
						view.resizeVertically();
					}
				}
					
			}
		};
	}
	
	/**
	 * Ecoute la pression sur un raccourci clavier pour selectioner l'outil correspondant
	 * @param btn Le bouton correspondant au raccourci
	 * @return
	 */
	public AbstractAction getButtonListener(final JToggleButton btn) {
		return new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btn.setSelected(true);
			}
		};
	}

}
