package com.blender.controllers;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.gui.ObjectRenderer;
import com.blender.views.LoadingView;
import com.blender.views.SlicingView;

public class SlicingController {
	
	private SlicingView view;
	private LoadingView loadingView;
	private byte axis;

	public SlicingController(SlicingView slicingView, LoadingView loadingView) {
		view = slicingView;
		this.loadingView = loadingView;
		axis = SlicingView.Z_AXIS;
	}

	public ActionListener getBrowseController() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.showFileChooser();
			}
		};
	}

	public ActionListener getCancelListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				view.dispose();
			}
		};
	}

	public ActionListener getSendListener(final ObjectsModel model) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loadingView.start();
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						String name = view.getNameField();
						if (name == null)
							return;
						String path = view.getPathField();
						if (path == null)
							return;
						
						int nbSlices = view.getNbSlices();
						
						ObjectRenderer renderer = new ObjectRenderer();
						renderer.sliceCutting(model.getObject(), 
								model.getProperties().getProperty(PropertiesModel.O_COLOR, new Color(0)),
								nbSlices, name, path, axis);
						loadingView.close();
						view.dispose();
					}
				}).start();
			}
		};
	}

	public ActionListener getAxisListener(final byte axis) {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SlicingController.this.axis = axis;
				int dimension = (int) view.getCurrentDimension();
				int value = view.getNbSlices();
				if (value > dimension)
					value = dimension;
				view.getNbSpinner().setModel(new SpinnerNumberModel(value, 0, dimension, 1));
				view.updateSliceThickness();
			}
		};
	}

	public ChangeListener getSpinnerListener() {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				view.updateSliceThickness();
			}
		};
	}
	
}
