package com.blender.controllers;

import javax.swing.JToggleButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;

/**
 * Controleur associe a TabsView
 *
 */
public class ToolButtonController {
	private ObjectsModel model;
	private Tool tool;
	
	public ToolButtonController(ObjectsModel model, Tool tool) {
		this.model = model;
		this.tool = tool;
	}

	/**
	 * Ecoute l'appuie sur un bouton pour changer l'outil
	 * @return
	 */
	public ChangeListener getChangeListener() {
		return new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if (((JToggleButton)e.getSource()).isSelected())
					model.setTool(tool);
			}
		};
	}

}
