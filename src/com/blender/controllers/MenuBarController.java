package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import com.blender.models.ObjectsModel;
import com.blender.utils.Tool;
import com.blender.views.FileHandlingView;
import com.blender.views.LoadingView;
import com.blender.views.SlicingView;

/**
 * Controleur de la barre de menus
 *
 */
public class MenuBarController {

	private FileHandlingView fileHandlingView;
	private ObjectsModel model;

	public MenuBarController(ObjectsModel model) {
		this.model = model;
	}

	public ActionListener getImportItemActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (fileHandlingView == null)
					fileHandlingView = new FileHandlingView(model);
				fileHandlingView.setVisible(true, false);
			}
		};
	}

	public ActionListener getExportItemActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (fileHandlingView == null)
					fileHandlingView = new FileHandlingView(model);
				fileHandlingView.setVisible(true, true);
			}
		};
	}

	public ActionListener getAboutItemActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String msg = "Blender++\n\nRéalisé par:\n- Florian CROSNIER\n- Jonathan LECOINTE\n- Damien MASSON\n";

				JOptionPane.showMessageDialog(null, msg, "A propos...", JOptionPane.INFORMATION_MESSAGE);
			}
		};
	}

	public ActionListener getExitItemActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		};
	}

	public ActionListener getToolItemActionListener(final Tool tool) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.setTool(tool);
			}
		};
	}

	public ActionListener getResetActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.getProperties().resetDefault();
			}
		};
	}

	public ActionListener getRegenPreviewsActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final LoadingView view = new LoadingView("Régénération des prévisualisations des modèles");
				view.start();
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							model.reloadPreviews();
						} catch (Exception e1) {}
						view.close();
					}

				}).start();
			}
		};
	}

	public ActionListener getSlicesActionListener() {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (model.getObject() == null) {
					JOptionPane.showMessageDialog(null, "Sélectionnez un modèle pour découper en tranches",
							"Erreur", JOptionPane.ERROR_MESSAGE);
				} else 
					new SlicingView(model);
			}
		};
	}
}
