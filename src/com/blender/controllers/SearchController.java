package com.blender.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.blender.models.ObjectsModel;
import com.blender.models.SearchModel;
import com.blender.views.AdvancedSearchView;
import com.blender.views.LoadingView;
import com.blender.views.SearchView;

/**
 * Contrôleur associé a SearchView
 *
 */
public class SearchController {
	/**
	 * Le modèle à contrôler
	 */
	private SearchModel searchModel;
	
	public SearchController(SearchModel searchModel) {
		this.searchModel = searchModel;
	}
	
	/**
	 * Ecoute l'appui sur un bouton pour savoir quelle model charger
	 * @return
	 */
	public ActionListener getButtonActionListener(final ObjectsModel model, final String name, final JTabbedPane tabbedPane, final JPanel comp) {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final LoadingView view = new LoadingView("Chargement du modèle \"" + name + "\"");
				view.start();
				new Thread(new Runnable() {

					@Override
					public void run() {
						int index = tabbedPane.indexOfComponent(comp);
						if (!model.setObject(index, name)) {
							view.close();
							return;
						}
						model.adaptTo((int)comp.getSize().getWidth(), (int)comp.getSize().getHeight());
						view.close();
					}
					
				}).start();
			}
		};
	}
	
	/**
	 * Ecoute les changement dans le JTextField (a chaque nouveau caractere)
	 * @param textField Le JTextField ecoute
	 * @return
	 */
	public DocumentListener getDocumentListener(final JTextField textField) {
		return new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				searchModel.search(textField.getText());
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				searchModel.search(textField.getText());
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {}
		};
	}
	
	/**
	 * Ecoute le JPanel pour adapter la scrollbar si la taille change
	 * @param view La vue du JPanel
	 * @return
	 */
	public ComponentListener getComponentListener(final SearchView view) {
		return new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent arg0) { }
			
			@Override
			public void componentResized(ComponentEvent arg0) {
				view.adaptToSize();
			}
			
			@Override
			public void componentMoved(ComponentEvent arg0) {}
			
			@Override
			public void componentHidden(ComponentEvent arg0) {}
		};
	}
	
	/**
	 * Ecoute l'appuie sur le bouton de recherche avancee pour ouvrir la fenetre correspondante
	 * @return
	 */
	public ActionListener getAdvancedSearchActionListener() {
		return new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new AdvancedSearchView(searchModel);
			}
		};
	}

}
