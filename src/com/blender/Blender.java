package com.blender;

import java.awt.Font;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import com.blender.utils.database.Database;
import com.blender.views.MainWindowView;
import com.jtattoo.plaf.acryl.AcrylLookAndFeel;

public class Blender {

	public static void main(String[] args) {
		String theme = "Acryl";
		Properties props = new Properties();
		props.setProperty("windowDecoration", "off");
		props.setProperty("logoString", "Blender++"); 
		AcrylLookAndFeel.setCurrentTheme(props);
		try {
			Database.initDb();
			UIManager.setLookAndFeel("com.jtattoo.plaf." + theme.toLowerCase() + "." + theme + "LookAndFeel");
			changeUIFont();
			new MainWindowView();
		} catch (Exception e) {}
	}
	
	
	public static void changeUIFont() {
        Enumeration<Object> keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                Font font = new Font("Arial", 0, 12);
                UIManager.put(key, new FontUIResource(font));
            }
        }
    }
}
