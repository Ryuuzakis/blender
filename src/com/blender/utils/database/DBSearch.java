package com.blender.utils.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class DBSearch {
	private static final HashMap<String, String> metaDataAliases = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;
	{
		put("author", "author");
		put("auteur", "author");
		put("date", "date");
		put ("comment", "comment");
		put("commentaire", "comment");
		put("nom", "model_name");
	} };

	private Queue<String> attributeValues, nameQueries, nameValues, sortFields, sortOrders;
	private ArrayList<String> result;
	private boolean nameOnly;

	/**
	 * Cree un nouveau "set" de criteres de recherche.
	 */
	public DBSearch() {
		attributeValues = new LinkedList<String>();
		nameValues = new LinkedList<String>();
		nameQueries = new LinkedList<String>();
		sortFields = new LinkedList<String>();
		sortOrders = new LinkedList<String>();
		nameOnly = true;
	}

	/**
	 * Ajoute un critere de recherche sur un attribut qui doit etre associe au fichier recherche
	 * @param category categorie de l'attribut voulu
	 * @param value valeur de l'attribut voulu
	 */
	public void withAttribute(String category, String value) {
		attributeValues.add(category);
		attributeValues.add(value);
		if (nameOnly) nameOnly = false;
	}

	/**
	 * Ajoute un critere de recherche sur le nom que doit avoir le fichier recherche
	 * @param name nom du modele voulu
	 */
	public void withName(String name) {
		nameQueries.add("LIKE ?");
		nameValues.add("%" + name + "%");
	}

	/**
	 * Ajoute un critere de recherche sur un nom qui ne devra pas figurer dans les resultats de la recherche
	 * @param name nom du modele a exclure
	 */
	public void withoutName(String name) {
		nameQueries.add("!= ?");
		nameValues.add(name);
	}

	/**
	 * Definit un ordre de tri
	 * @param field nom du champ sur lequel doit etre effectue le tri
	 * @param order ordre dans lequel trier les resultats ("asc" ou "desc")
	 */
	public void orderBy(String field, boolean asc) {
		if (isMetaDatum(field)) {
			sortFields.add(field);
			if (asc) sortOrders.add("ASC");
			else sortOrders.add("DESC");
		}
	}

	/**
	 * Renvoie la liste des noms des modeles correspondant aux criteres de recherche
	 * @return liste des noms des modeles
	 * @throws Exception si les criteres de recherche ne sont pas valides
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public ArrayList<String> getResult() throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		result = new ArrayList<String>();

		stmt = buildStatement();

		try {
			rs = stmt.executeQuery();
			while (rs.next())
				result.add(rs.getString(1));
		} catch (Exception e) {
			throw new Exception("Echec de la recuperation des resultats de la recherche\n" +
					"Erreur de communication avec la base de donnees");
		}

		return result;
	}

	/**
	 * Cree et renvoie le PreparedStatement qui sera envoye a la DB
	 * @return le PreparedStatement qui sera envoye
	 * @throws Exception si les criteres de recherche ne sont pas valides
	 */
	private PreparedStatement buildStatement() throws Exception {
		String query;
		PreparedStatement stmt;

		query = buildNameQuery();
		query += buildAttributeQuery();
		if (query.isEmpty())
			query = "SELECT name FROM MODEL";
		query += buildSortQuery();

		try {
			stmt = Database.getConnection().prepareStatement(query);
		} catch (SQLException e) {
			throw new Exception("Les criteres de recherche ne sont pas valides");
		}

		fillStatement(stmt);

		return stmt;
	}

	/**
	 * Cree et renvoie la chaine d'instructions SQL qui concerne les criteres de nom
	 * @param nameOnly true si le nom du modele est le seul critere de recherche
	 * @return chaine d'instructions SQL concernant les criteres de nom
	 */
	private String buildNameQuery() {
		if (nameQueries.isEmpty()) return "";
		String query = "SELECT DISTINCT model_name, author, date FROM IDENTIFICATION INNER JOIN MODEL "
				+ "ON MODEL.name = IDENTIFICATION.model_name WHERE ";

		while (!nameQueries.isEmpty()) {
			query += "model_name " +  nameQueries.remove();
			if (!nameQueries.isEmpty())
				query += " AND ";
		}

		if (!nameOnly) query += " INTERSECT ";

		return query;
	}

	/**
	 * Cree et renvoie la chaine d'instructions SQL qui concerne les criteres d'attributs
	 * @return chaine d'instructions SQL concernant les criteres d'attributs
	 */
	private String buildAttributeQuery() {
		if (attributeValues.isEmpty()) return "";
		String query = "SELECT DISTINCT model_name,author, date FROM IDENTIFICATION INNER JOIN MODEL "
				+ "ON MODEL.name = IDENTIFICATION.model_name WHERE ";
		ArrayList<String> attributeValuesList = new ArrayList<String>();
		attributeValuesList.addAll(attributeValues);

		for (int i = 0; i < attributeValuesList.size(); i+=2) {
			if (isMetaDatum(attributeValuesList.get(i))) {
					query += "MODEL." + metaDataAliases.get(attributeValuesList.remove(i)) + " LIKE ? ";
					++i;
			} else {
				query += "attribute_id = (SELECT id FROM ATTRIBUTE " +
						"WHERE category = ? AND value LIKE ?) ";
			}
			if (i+2 < attributeValuesList.size()) {
				query += "INTERSECT SELECT DISTINCT model_name, author, date FROM IDENTIFICATION INNER JOIN MODEL "
						+ "ON MODEL.name = IDENTIFICATION.model_name WHERE ";
			}
		}

		attributeValues.clear();
		attributeValues.addAll(attributeValuesList);

		return query;
	}

	/**
	 * Cree et renvoie la chaine d'instructions SQL qui definit les ordres de tri
	 * @return chaine d'instructions SQL definissant le tri des resultats
	 */
	private String buildSortQuery() {
		if (sortFields.isEmpty()) return "";
		String query = " ORDER BY ";
		String field = sortFields.peek();

		for (int i = 0; i < sortOrders.size(); ++i) {
			field = metaDataAliases.get(sortFields.remove());
			query += field + " " + sortOrders.remove();
			if (i < sortOrders.size())
				query += ", ? " + sortOrders.remove();
		}

		return query;
	}

	/**
	 * Ajoute les valeurs precedemment choisies par l'utilisateur dans le PreparedStatement
	 * @param stmt PreparedStatement a remplir
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	private void fillStatement(PreparedStatement stmt) throws Exception {
		int i;

		try {
			for (i = 1; !nameValues.isEmpty(); ++i)
				stmt.setString(i, nameValues.remove());
			for (; !attributeValues.isEmpty(); ++i)
				stmt.setString(i, attributeValues.remove());
		} catch (SQLException e) {
			throw new Exception("Les valeurs recherchees sont incorrectes");
		}
	}

	/**
	 * Determine si une chaine est un champ de meta-donnee de modele
	 * @param value chaine a analyser
	 * @return vrai si la chaine est un champ de meta-donnee, faux sinon
	 */
	private static boolean isMetaDatum(String value) {
		return metaDataAliases.containsKey(value);
	}
}
