package com.blender.utils.database;

import java.io.File;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.sqlite.SQLiteConfig;

import com.blender.models.ObjectInformations;
import com.blender.utils.io.FileHandler;

public class Database {

	private static Connection db;

	/**
	 * Recupere la connexion a la DB
	 * @return la connexion creee
	 * @throws Exception si le driver de SQLite est introuvable
	 * @throws Exception si on ne peut pas acceder a la base de donnees
	 */
	public static Connection getConnection() throws Exception {
		try {
			if (db == null || db.isClosed()) {
				File dir = new File("data");
				if (!dir.exists()) dir.mkdir();
				Class.forName("org.sqlite.JDBC");
				SQLiteConfig config = new SQLiteConfig();
				config.enforceForeignKeys(true);
				db = DriverManager.getConnection("jdbc:sqlite:data/blender.db", config.toProperties());
			}
		} catch (ClassNotFoundException e) {
			throw new Exception("Le driver de base de donnees est introuvable");
		} catch (SQLException e) {
			throw new Exception("La connexion a la base de donnees a echoue");
		}

		return db;
	}

	/**
	 * Ferme la connexion a la DB
	 */
	public static void closeConnection() {
		try {
			db.close();
		} catch (SQLException e) {}
	}

	/**
	 * Cree la DB si elle n'existait pas ou
	 * si elle n'etait pas initialisee correctement
	 * @throws Exception si on ne peut pas acceder aux metadata de la base de donnees
	 * @throws Exception si on n'a pas pu ajouter les tables a la base de donnees
	 */
	public static void initDb() throws Exception {
		final String[] tables = {"MODEL","ATTRIBUTE","IDENTIFICATION","IMAGE"};
		ResultSet rs;
		boolean invalidDb = false;

		try {
			DatabaseMetaData dbmd = getConnection().getMetaData();
			for (int i = 0; i < tables.length && !invalidDb; ++i) {
				rs = dbmd.getTables(null, null, tables[i], new String[] {"TABLE"});
				invalidDb = !rs.next();
			}
		} catch (SQLException e) {
			throw new Exception("L'etat de la base de donnees n'a pas pu etre verifie");
		}
		if (invalidDb) {

			String drop = "", create = "";
			Statement stmt;

			for (String tableToDrop : tables) {
				drop += "DROP TABLE IF EXISTS " + tableToDrop + ";";
			}

			try {
				stmt = getConnection().createStatement();
				stmt.executeUpdate(drop);
			} catch (SQLException e) {
				throw new Exception("La base de donnees n'a pas pu etre videe");
			}

			create += "CREATE TABLE model (" +
					"name			VARCHAR(50)		COLLATE NOCASE," +
					"version_of 	VARCHAR(50)," +
					"author			VARCHAR(50)," +
					"date			VARCHAR(10)," +
					"comment		TEXT," +
					"path			VARCHAR(255)	NOT NULL," +
					"preview		VARCHAR(255)," +
					"PRIMARY KEY	(name)," +
					"FOREIGN KEY	(version_of)	REFERENCES model(name));";

			create += "CREATE TABLE attribute (" +
					"id				INTEGER			PRIMARY KEY AUTOINCREMENT," +
					"category		VARCHAR(50)," +
					"value			VARCHAR(50)," +
					"CONSTRAINT		uq_attr			UNIQUE(category, value));";

			create += "CREATE TABLE identification (" +
					"model_name		VARCHAR(50)		COLLATE NOCASE," +
					"attribute_id	INTEGER," +
					"PRIMARY KEY	(model_name,attribute_id)," +
					"FOREIGN KEY	(model_name)	REFERENCES model(name)," +
					"FOREIGN KEY	(attribute_id)	REFERENCES attribute(id));";

			create += "CREATE TABLE image (" +
					"model_name		VARCHAR(50)," +
					"image_path		VARCHAR(255)," +
					"PRIMARY KEY	(model_name,image_path)," +
					"FOREIGN KEY	(model_name)	REFERENCES model(name))";
			try {
				stmt.executeUpdate(create);
			} catch (SQLException e) {
				throw new Exception("Les tables n'ont pas pu etre ajoutees a la base de donnees");
			}

			addModelsFromDir();
			
			// Ajout de donnée random dans la BDD TODO: L'enlever
			//TODO : Retirer plus tard
			try {
				for (String name : DBModel.getModelsNames()) {
					ObjectInformations info = new ObjectInformations(name);
					String[] syllables = new String[]{"ra", "pu", "ka", "cu", "tu", "pe", "gi", "xo", "ni", "so"};
					String nom = "";
					for (int i = 0; i < 4; ++i) {
						int r = (int) (Math.random() * syllables.length);
						nom += syllables[r];
					}
					info.setAuthor((nom.charAt(0) + "").toUpperCase() + nom.substring(1));
					long dt = (long) (System.currentTimeMillis() - (900000000 + (Math.random() * 900000000)));
					Date d = new Date(dt);
					DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					info.setDate(df.format(d));
					info.setDescription("Ce modele represente un " + name);
					info.saveToDB();
				}
			} catch (Exception e) {}
		}
	}

	/**
	 * Ajoute tous les modeles contenus dans le dossier des modeles dans la BDD
	 * @throws Exception si saveModel a renvoye une exception
	 */
	private static void addModelsFromDir() {
		List<String> modelsPaths = FileHandler.getModelsNamesFromDir();
		for (String modelPath : modelsPaths) {
			int idx = modelPath.lastIndexOf('.');
			String name;
			if (idx == -1)
				name = modelPath;
			else
				name = modelPath.substring(0, idx);
			try {
				DBUpdate.saveModel(name, FileHandler.OBJECT3D_PATH + modelPath, null);
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
