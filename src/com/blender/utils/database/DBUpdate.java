package com.blender.utils.database;

import java.awt.Color;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.gui.ObjectRenderer;
import com.blender.utils.io.FileHandler;

public class DBUpdate {

	/**
	 * Enregistre un modele dans la base de donnees
	 * @param modelName nom du modele a ajouter
	 * @param modelPath chemin vers le fichier du modele
	 * @param initialVersion nom du modele dont celui a ajouter est issu ({@code null} si
	 * le modele n'est pas une modification d'un autre)
	 * @throws Exception si le path n'est pas un fichier de modele
	 * @throws Exception si le nom du modele est deja utilise
	 * @throws Exception si le nom du modele initial n'existe pas
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public static void saveModel(String modelName, String modelPath, String initialVersion) throws Exception {
		PreparedStatement stmt;
		String sql;

		if (!DBModel.getByName(modelName, true).isEmpty())
			throw new Exception("Erreur lors de l'enregistrement d'un modele : " +
					"le nom de modele " + modelName + " est deja utilise");
		if (initialVersion != null) {
			if (DBModel.getByName(initialVersion, true).isEmpty())
				throw new Exception("Erreur lors de l'enregistrement d'un modele : " +
						"le modele " + initialVersion + " n'existe pas");
		}

		FileHandler.getModel(modelPath);

		sql = "INSERT INTO MODEL(name, path, version_of) VALUES(?, ?, ?)";
		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, modelName);
			stmt.setString(2, modelPath);
			stmt.setString(3, initialVersion);
			stmt.executeUpdate();

			setIdentification(modelName, "", "");
		} catch (SQLException e) {
			throw new Exception("Le modele " + modelName +" n'a pas pu etre ajoute dans la base de donnees");
		}

		generatePreview(modelName, modelPath);
	}

	/**
	 * Modifie la valeur d'une des meta-informations d'un modele
	 * @param modelName nom du modele a modifier
	 * @param col nom de la colonne a modifier dans la table des modeles
	 * @param value nouvelle valeur de l'entree
	 * @throws Exception si le modele n'existe pas
	 * @throws Exception si le nom de colonne n'est pas valide
	 * @throws Exception si la DB a renvoye une exception pendant l'execution de la requete
	 */
	public static void setMetaData(String modelName, String col, String value) throws Exception {
		PreparedStatement stmt;
		String sql;

		if (DBModel.getByName(modelName, true).isEmpty())
			throw new Exception("Le modele " + modelName + " n'existe pas");

		if (!(col.equals("author") || col.equals("date") || col.equals("comment")))
			throw new Exception("La colonne '" + col + "' n'est un nom de colonne valide");

		sql = "UPDATE MODEL SET " + col + " = ? WHERE name = ?";
		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, value);
			stmt.setString(2, modelName);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("Erreur lors de la modification des informations de " + modelName);
		}
	}

	/**
	 * Associe une nouvelle image a un modele
	 * @param modelName nom du modele
	 * @param imagePath chemin vers l'image
	 * @throws Exception si le fichier {@code imagePath} n'existe pas
	 * @throws Exception si le fichier {@code imagePath} n'est pas un fichier d'image
	 * @throws Exception si {@code modelName} n'est pas present dans la base
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public static void setImage(String modelName, String imagePath) throws Exception {
		PreparedStatement stmt;
		String sql;

		if (new File(imagePath).isFile()) {
			if (!isImage(imagePath))
				throw new Exception("Erreur lors de l'ajout d'une image : " +
						"le fichier " + imagePath + " n'est pas un fichier d'image correct");
		} else {
			throw new Exception("Erreur lors de l'ajout d'une image : " +
					"le fichier " + imagePath + " n'existe pas");
		}

		if (DBModel.getByName(modelName, true).isEmpty())
			throw new Exception("Erreur lors de l'ajout d'une image : " +
					"le modele " + modelName + " n'existe pas");

		sql = "INSERT INTO IMAGE(model_name, image_path) VALUES(?, ?)";
		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, modelName);
			stmt.setString(2, imagePath);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("L'image n'a pas pu etre ajoutee dans la base de donnees");
		}
	}

	/**
	 * Associe un attribut a un modele present dans la base
	 * @param modelName nom du modele auquel ajouter l'attribut
	 * @param cat categorie d'attribut a ajouter
	 * @param val valeur de l'attribut a ajouter
	 * @throws Exception si le nom du modele n'existe pas
	 * @throws Exception si le modele possede deja l'attribut a ajouter
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public static void setIdentification(String modelName, String cat, String val) throws Exception {
		PreparedStatement stmt;
		String sql;
		int attribId;

		DBModel.getByName(modelName, true);

		try {
			List<String> models = DBModel.getByKeywords(cat, val, true);
			for (String model : models) {
				if (model.equals(modelName))
					throw new Exception("Erreur lors de l'ajout d'attribut : " +
							"le modele " + modelName + " possede deja l'attribut " +
							cat + " = " + val);
			}
		} catch (Exception e) {}

		try {
			attribId = DBModel.getAttributeIdByKeywords(cat, val);
		} catch (Exception e) {
			createAttribute(cat, val);
			attribId = DBModel.getAttributeIdByKeywords(cat, val);
		}

		sql = "INSERT INTO IDENTIFICATION(model_name, attribute_id) VALUES (?, ?)";
		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, modelName);
			stmt.setInt(2, attribId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			if (!e.getMessage().contains("UNIQUE constraint failed: identification.model_name, identification.attribute_id"))
				throw new Exception("L'attribut '" + cat + "' = '" + val + "' n'a pas pu être " +
						"ajoute au modele " + modelName);
		}
	}
	
	/**
	 * Supprime toutes les données sur un model pour une categorie
	 * @param modelName modele auquel on veut supprimer les données
	 * @param cat categorie a laquelle appartiennent les attributs
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public static void clearIdentifications(String modelName, String cat) throws Exception {
		PreparedStatement stmt;
		String sql = "DELETE FROM identification WHERE model_name LIKE ? "
				+ "AND attribute_id IN (SELECT id FROM attribute "
				+ "WHERE category LIKE ?)";
		

		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, modelName);
			stmt.setString(2, cat);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("Les données de la categorie " + cat + "n'ont pas pu etre retiré du modele " + modelName);
		}
	}

	/**
	 * Supprime une association entre un modele et un attribut
	 * @param modelName modele auquel on veut retirer un attribut
	 * @param cat categorie a laquelle appartient l'attribut
	 * @param val valeur de l'attribut
	 * @throws Exception si le nom du modele n'est pas dans la base
	 * @throws Exception si l'attribut n'est pas dans la base
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	public static void removeIdentification(String modelName, String cat, String val) throws Exception {
		PreparedStatement stmt;
		int attributeId = -1;
		String sql = "DELETE FROM IDENTIFICATION WHERE model_name = ? AND attribute_id = ?";
		attributeId = DBModel.getAttributeIdByKeywords(cat, val);

		if (DBModel.getByName(modelName, true) == null)
			throw new Exception("Le modele " + modelName + " n'a pas ete trouve dans la base de donnees");

		if (attributeId == -1)
			throw new Exception("L'attribut " + cat + " = " + val + " n'a pas ete trouve dans la base de donnes");

		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, modelName);
			stmt.setInt(2, attributeId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("L'attribut " + cat + " = " + val + " n'a pas pu etre retire du modele " + modelName);
		}
	}

	/**
	 * Cree un nouvel attribut dans la base de donnees
	 * @param cat categorie de l'attribut a creer
	 * @param val valeur de l'attribut a creer
	 * @throws Exception si l'attribut existe deja
	 * @throws Exception si la base de donnees a renvoye une exception
	 */
	private static void createAttribute(String cat, String val) throws Exception {
		PreparedStatement stmt;
		String sql = "INSERT INTO ATTRIBUTE(category, value) VALUES(?, ?)";

		try {
			DBModel.getAttributeIdByKeywords(cat, val);
			throw new Exception("Erreur lors de la creation d'attribut : " +
					"l'attribut " + cat + " = " + val + " est deja present dans la base");
		} catch (Exception e) {}

		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, cat);
			stmt.setString(2, val);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("L'attribut '" + cat + "' = '" + val + "' n'a pas pu etre cree");
		}
	}

	/**
	 * Cree une image de previsualisation pour le modele donne, l'enregistre dans ressources/previews/ et l'ajoute
	 * dans la base de donnees.
	 * L'image est un "screenshot" de petite taille du modele
	 * @param modelName nom du modele dont on veut creer la previsualisation
	 * @param modelPath chemin vers le fichier du modele
	 * @return chemin vers l'image de previsualisation
	 * @throws Exception si le fichier de preview n'a pas pu etre cree
	 * @throws Exception Si la DB a renvoye une exception
	 */
	public static void generatePreview(String modelName, String modelPath) throws Exception {
		generatePreview(modelName, modelPath, null);
	}

	/**
	 * Verifie si un fichier represente une image
	 * @param image_path Chemin vers le fichier de l'image a verifier
	 * @return Vrai si le fichier est un fichier d'image correct, faux sinon
	 */
	private static boolean isImage(String imagePath){
		return new ImageIcon(imagePath).getImage().getWidth(null) > -1;
	}

	/**
	 * Cree et stocke dans la base de donnees une image d'apercu d'un modele
	 * @param modelName nom du modele dont on cree l'apercu
	 * @param modelPath chemin vers le fichier representant l'objet
	 * @param model modele dans lequel est represente l'objet
	 * @throws Exception
	 */
	private static void generatePreview(String modelName, String modelPath, ObjectsModel model) throws Exception {
		PreparedStatement stmt;
		String sql = "UPDATE MODEL SET preview = ? WHERE name = ?";

		File previews = new File("ressources/previews");
		if (!previews.exists())
			previews.mkdirs();
		String previewPath = "ressources/previews/" + modelName + "_preview.png";
		ObjectRenderer object = new ObjectRenderer();
		try {
			if (model != null) {
				Color objColor = model.getProperties().getProperty(PropertiesModel.O_COLOR, new Color(0));
				Color lightColor = model.getProperties().getProperty(PropertiesModel.O_LIGHT_COLOR, new Color(0));
				object.saveTo(FileHandler.getModel(modelPath), previewPath, objColor, lightColor);
			} else
				object.saveTo(FileHandler.getModel(modelPath), previewPath);
		} catch (Exception e) {
			throw new Exception("Erreur lors de la generation de la previsualisation de " + modelName + "\n" + e.getMessage());
		}
		try {
			stmt = Database.getConnection().prepareStatement(sql);
			stmt.setString(1, previewPath);
			stmt.setString(2, modelName);
			stmt.executeUpdate();
		} catch (SQLException e) {
			throw new Exception("La previsualisation n'a pas pu etre ajoutee a la base de donnees");
		}
	}

	/**
	 * Genere les apercus d'une liste de modeles
	 * @param model modele dans lequel sont representes les objets
	 * @throws Exception
	 */
	public static void generatePreviews(ObjectsModel model) {
		List<String> models = FileHandler.getModelsNamesFromDir();
		for (String modelName : models) {
			try {
				int idx = modelName.lastIndexOf('.');
				String name;
				if (idx == -1)
					name = modelName;
				else
					name = modelName.substring(0, idx);
				generatePreview(name, FileHandler.OBJECT3D_PATH + modelName, model);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

}
