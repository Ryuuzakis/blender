package com.blender.utils.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBModel {

	/**
	 * Recupere le ou les modeles dont le nom correspond a celui passe en parametre
	 * @param name le nom a chercher dans la base
	 * @param exactName true si le nom a rechercher dans la base doit etre exactement identique a {@code name}
	 * @return Modele(s) dont le nom correspond a {@code name} ou %{@code name}%
	 * @throws Exception
	 */
	public static List<String> getByName(String name, boolean exactName) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		ArrayList<String> names = new ArrayList<String>();
		String query = (exactName ?
				"SELECT name FROM MODEL WHERE name = ?" : "SELECT name FROM MODEL WHERE name LIKE ?");

		try {
			stmt = Database.getConnection().prepareStatement(query);
			if (!exactName) name = "%" + name + "%";
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			while (rs.next()) {
				if (!names.contains(rs.getString("name")))
					names.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de donnees");
		}

		return names;
	}

	/**
	 * Recupere la valeur d'une meta-information d'un modele donne
	 * @param modelName nom du modele
	 * @param col nom de la colonne dans la table des modeles
	 * @return valeur de la colonne pour le modele donne
	 * @throws Exception
	 */
	public static String getMetaData(String modelName, String col) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		String colValue;

		if (!(col.equals("author") || col.equals("date") || col.equals("comment")))
			throw new Exception("La colonne '" + col + "' n'est un nom de colonne valide");

		String query = "SELECT " + col + " FROM MODEL WHERE name = ?";

		if (DBModel.getByName(modelName, true).isEmpty())
			throw new Exception("Le modele '" + modelName + "' n'existe pas");

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, modelName);
			rs = stmt.executeQuery();

			if (rs.next())
				colValue = rs.getString(col);
			else
				colValue = null;
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de donnees");
		}
				
		return colValue;
	}

	/**
	 * Recupere les categories enregistrees dans la base dont le nom est %{@code cat}%
	 * @param cat nom de la categorie a rechercher dans la base
	 * @return categorie(s) dont le nom correspond a %{@code cat}%
	 * @throws Exception
	 */
	public static List<String> getCategories(String cat) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		ArrayList<String> categories = new ArrayList<String>();
		String query = "SELECT category FROM ATTRIBUTE " +
				"WHERE category LIKE ?";

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, "%" + cat + "%");
			rs = stmt.executeQuery();

			while (rs.next()) {
				if (!categories.contains(rs.getString("category")))
					categories.add(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return categories;
	}
	
	/**
	 * Recupere les attributs associé a l'objet pour une categorie spécifiée
	 * @param cat nom de la categorie a rechercher dans la base
	 * @return Une liste des valeurs de l'attributs
	 * @throws Exception
	 */
	public static List<String> getIdentifications(String modelName, String cat) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		ArrayList<String> values = new ArrayList<String>();
		
		String query = "SELECT value FROM identification INNER JOIN attribute"
				+ " ON identification.attribute_id = attribute.id "
				+ "WHERE model_name LIKE ? AND category LIKE ?";

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, modelName);
			stmt.setString(2, cat);
			rs = stmt.executeQuery();

			while (rs.next()) {
				values.add(rs.getString(1));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return values;
	}

	/**
	 * Recupere le(s) modele(s) dont les attributs correspondent a ceux passes en parametres
	 * @param cat categorie dans laquelle chercher {@code val}
	 * @param val valeur devant etre attribuee au(x) modele(s) dans la categorie {@code cat}
	 * @param exactVal {@code true} si la valeur dans la base doit etre exactement egale
	 * à {@code val}
	 * @return modele(s) ayant comme attributs {@code cat} et {@code val} ou %{@code val}%
	 * @throws Exception
	 */
	public static List<String> getByKeywords(String cat, String val, boolean exactVal) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		ArrayList<String> names = new ArrayList<String>();
		String query = "SELECT model_name FROM IDENTIFICATION " +
				"WHERE attribute_id = ? ";
		int attributeId;

		if (!exactVal) val = "%" + val + "%";
		attributeId = getAttributeIdByKeywords(cat, val);

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setInt(1, attributeId);
			rs = stmt.executeQuery();

			while (rs.next()) {
				if (!names.contains(rs.getString("model_name")))
					names.add(rs.getString("model_name"));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return names;
	}

	/**
	 * Recupere la valeur d'un attribut d'un modele
	 * @param name nom du modele
	 * @param category categorie de l'attribut recherche
	 * @return valeur de l'attribut s'il a ete defini pour le modele, sinon null
	 * @throws Exception
	 */
	public static String getKeywordValue(String name, String category) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		String value;
		String query = "SELECT value FROM ATTRIBUTE LEFT JOIN IDENTIFICATION "
				+ "WHERE ATTRIBUTE.id = IDENTIFICATION.attribute_id AND category = ? AND model_name = ?";

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, category);
			stmt.setString(2, name);
			rs = stmt.executeQuery();

			if (rs.next())
				value = rs.getString("value");
			else
				value = null;
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return value;
	}

	// Probablement inutile
	/*	public static List<String> search(String name, String cat, String val, boolean and, boolean exactName,
			boolean exactVal) throws Exception {
		ArrayList<String> namesFromName = (ArrayList<String>) getByName(name, exactName);
		ArrayList<String> namesFromAttrib = (ArrayList<String>) getByKeywords(cat, val, exactVal);

		if (and) namesFromName.retainAll(namesFromAttrib);
		else namesFromName.addAll(namesFromAttrib);

		return namesFromName;
	}*/

	/**
	 * Recupere l'identifiant d'un attribut enregistre dans la base
	 * @param cat categorie de l'attribut
	 * @param val valeur de l'attribut
	 * @return identifiant associe a l'attribut dans la base de donnees
	 * @throws Exception
	 */
	static int getAttributeIdByKeywords(String cat, String val) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		int id;
		String query = "SELECT id FROM ATTRIBUTE " +
				"WHERE category = ? AND value LIKE ?";

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, cat);
			stmt.setString(2, val);
			rs = stmt.executeQuery();

			rs.next();
			id = rs.getInt("id");
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return id;
	}

	/**
	 * Recupere la categorie et la valeur d'un attribut enregistre dans la base
	 * @param id identifiant de l'attribut dans la base
	 * @return categorie et valeur de l'attribut associes a l'identifiant {@code id}
	 * @throws Exception
	 */
	static String[] getAttributeById(int id) throws Exception {
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT category, value FROM ATTRIBUTE " +
				"WHERE id = ?";
		String[] attrId = new String[2];

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();

			rs.next();
			attrId[1] = rs.getString("cat");
			attrId[2] = rs.getString("val");
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return attrId;
	}

	/**
	 * Recupere la liste des modeles qui sont issus du modele {@code name}
	 * @param name nom du modele dont on cherche les "fils"
	 * @return liste de modeles issus du modele passe en parametre
	 * @throws Exception
	 */
	public static List<String> getVersionsFromModel(String name) throws Exception {
		ArrayList<String> versions = new ArrayList<String>();
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT name FROM MODEL" +
				"WHERE version_of = ?";

		getByName(name, true);

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			while (rs.next()) {
				if (!versions.contains(rs.getString("name")))
					versions.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return versions;
	}

	/**
	 * Recupere le chemin vers le fichier associe au modele {@code nom}
	 * @param name nom du modele dont on veut recuperer le chemin
	 * @return chemin vers le fichier representant le modele recherche {@code nom}
	 * @throws Exception
	 */
	public static String getPath(String name) throws Exception {
		String path = "";
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT path FROM MODEL " +
				"WHERE name = ?";

		if (DBModel.getByName(name, true).isEmpty())
			throw new Exception("Le modele '" + name + "' n'a pas ete trouve dans la base de donnees");

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			rs.next();
			path = rs.getString("path");
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return path;
	}

	/**
	 * Recupere tous les noms des modeles presents dans la base
	 * @return liste des noms des modeles enregistres
	 * @throws Exception
	 */
	public static List<String> getModelsNames() throws Exception {
		String query = "SELECT name FROM MODEL";
		List<String> names = new ArrayList<String>();
		ResultSet rs;

		try {
			rs = Database.getConnection().createStatement().executeQuery(query);
			while (rs.next())
				names.add(rs.getString("name"));
		} catch (Exception e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return names;
	}

	/**
	 * Recupere les chemins vers les images des realisations du modele {@code name}
	 * @param name nom du modele dont on veut recuperer les images
	 * @return chemins vers les images
	 */
	public static List<String> getImages(String name) throws Exception {
		ArrayList<String> imagePaths = new ArrayList<String>();
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT image_path FROM IMAGE " +
				"WHERE model_name = ?";

		if (DBModel.getByName(name, true).isEmpty())
			throw new Exception("Le modele '" + name + "' n'a pas ete trouve dans la base de donnees");

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			rs.next();
			imagePaths.add(rs.getString("image_path"));
			while (rs.next()) {
				if (!imagePaths.contains(rs.getString("image_path")))
					imagePaths.add(rs.getString("image_path"));
			}
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return imagePaths;
	}

	/**
	 * Recupere le chemin vers l'image de previsualisation du modele {@code name}
	 * @param name nom du modele dont on veut recuperer l'image de previsualisation
	 * @return chemin vers l'image
	 */
	public static String getPreview(String name) throws Exception {
		String previewPath;
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT preview FROM MODEL " +
				"WHERE name = ?";

		if (DBModel.getByName(name, true).isEmpty())
			throw new Exception("Le modele '" + name + "' n'a pas ete trouve dans la base de donnees");

		try {
			stmt = Database.getConnection().prepareStatement(query);
			stmt.setString(1, name);
			rs = stmt.executeQuery();

			rs.next();
			previewPath = rs.getString("preview");
		} catch (SQLException e) {
			throw new Exception("Erreur de communication avec la base de données");
		}

		return previewPath;
	}
}
