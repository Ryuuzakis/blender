package com.blender.utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Classe permettant de parser les recherches des utilisateurs dans la SearchView
 *
 */
public class AdvancedSearchParser {
	public final static String[] FIELDS_NAME = new String[]{"auteur", "date", "tri", "formes", "mots_cles"};
	
	private HashMap<String, String> fields;
	private String objName, sortField;
	private boolean sortOrder;
	
	/**
	 * Cree un objet pour analyser une ligne de recherche avancee
	 * @param str La ligne a analyser
	 */
	public AdvancedSearchParser(String str) {
		fields = new HashMap<String, String>();
		sortField = "nom";
		sortOrder = true;
		objName = parseFields(str);
	}
	
	/**
	 * Analyse la chaine et recupere les champs importants
	 * @param str La chaine a analyser
	 * @return Le reste (ce qui n'est pas dans un champ)
	 */
	private String parseFields(String str) {
		ArrayList<Integer> fieldsPosition = new ArrayList<Integer>();
		for (String f : FIELDS_NAME) {
			int idx = str.indexOf(f+"=");
			if (idx >= 0) fieldsPosition.add(idx);
		}
		Collections.sort(fieldsPosition);
		
		for (int i = 0; i < fieldsPosition.size(); ++i) {
			int s = (i+1 == fieldsPosition.size()) ? str.length() : fieldsPosition.get(i+1); 
			String[] field = str.substring(fieldsPosition.get(i), s).split("=", 2);
			
			if (field.length == 2 && field[1].length() >= 1) {
				if (field[0].equals("tri")) {
					if (field[1].length() <= 1) continue;
					sortField = field[1].charAt(0) == '!' ? field[1].substring(1) : field[1];
					if (!sortField.equals(FIELDS_NAME[0]) && !sortField.equals(FIELDS_NAME[1]) 
							&& !sortField.equals(FIELDS_NAME[2]) && !sortField.equals("nom")) {
								sortField = "nom";
								continue;
							}
					sortOrder = !(field[1].charAt(0) == '!');
				} else {
					fields.put(field[0].trim(), field[1].trim());
				}
			}
		}
		
		int s = fieldsPosition.isEmpty() ? str.length() : fieldsPosition.get(0); 
		
		return str.substring(0, s).trim();
	}
	
	
	/**
	 * Recupere le contenu des champs
	 * @return Une hashmap des champs
	 */
	public HashMap<String, String> getFields() {
		return fields;
	}
	
	/**
	 * Renvoie le champ sur lequel le tri est effectue
	 * @return Une chaine contenant le nom du champ
	 */
	public String getSortField() {
		return sortField;
	}
	
	/**
	 * Recupere l'ordre du tri
	 * @return Vrai si c'est dans l'ordre croissant, faux sinon
	 */
	public boolean isSortedInAsc() {
		return sortOrder;
	}
	
	/**
	 * Recupere le nom recherche
	 * @return La chaine contenant le nom cherche
	 */
	public String getName() {
		return objName;
	}
}
