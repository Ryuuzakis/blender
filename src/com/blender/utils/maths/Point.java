package com.blender.utils.maths;

/**
 * Classe représentant un point du repère
 *
 */
public class Point {

	private float x;
	private float y;
	private float z;
	
	public Point(float x, float y, float z) {
		this.setX(x);
		this.setY(y);
		this.setZ(z);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	
	/**
	 * Renvoie une representation en matrice du point
	 * @return La matrice correspondant au point
	 */
	public Matrix getMatrix() {
		return new Matrix(new float[][]{{x}, {y}, {z}, {1}});
	}
	
	@Override
	public boolean equals(Object o) {
		Point p = (Point) o;
		return x == p.x && y == p.y && z == p.z;
	}
	
	@Override
	public String toString() {
		return "("+x+","+y+","+z+")";
	}
	
	/**
	 * Retourne un vecteur permettant d'atteindre ce point
	 * en partant de l'origine
	 * @return vecteur 
	 */
	public Vector getVector() {
		return new Vector(x, y, z);
	}
}
