package com.blender.utils.maths;

/**
 * Classe permettant de représenter un segment entre deux points
 *
 */
public class Segment implements Comparable<Segment>{

	private Point p1;
	private Point p2;

	private Point middle;

	public Segment(Point p1, Point p2) {
		this.setP1(p1);
		this.setP2(p2);
		middle = getMiddlePoint(p1, p2);
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}
	
	public void setP2(Point p2) {
		this.p2 = p2;
	}

	public static boolean haveOneAndOnlyPointInCommon(Segment s1, Segment s2) {
		return (s1.getP1().equals(s2.getP1()) || s1.getP1().equals(s2.getP2()) 
				|| s1.getP2().equals(s2.getP1()) || s1.getP2().equals(s2.getP2())) && !s1.equals(s2);
	}

	@Override
	public boolean equals(Object o) {
		Segment s = (Segment) o;
		return p1.equals(s.p1) && p2.equals(s.p2);
	}

	/**
	 * Renvoie le point au milieu du segment forme par les points passes
	 * en parametre
	 * @param p1 point 1
	 * @param p2 point 2
	 * @return milieu
	 */
	public static Point getMiddlePoint(Point p1, Point p2) {
		float x = (p1.getX() + p2.getX()) / 2;
		float y = (p1.getY() + p2.getY()) / 2;
		float z = (p1.getZ() + p2.getZ()) / 2;
		return new Point(x, y, z);
	}

	@Override
	public int compareTo(Segment s) {
		float tmp = middle.getZ() - s.middle.getZ();
		if (tmp >= 0) return 1;
		return -1;
	}
	
	@Override
	public String toString() {
		return "{" + p1 + "," + p2 + "}";
	}
}
