package com.blender.utils.maths;

import java.awt.Color;
import java.awt.Polygon;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Classe representant un triangle composant les modeles
 *
 */
public class Triangle implements Comparable<Triangle>{

	/**
	 * Liste des points inclus dans le triangle
	 */
	private ArrayList<Point> pointsInTriangle = new ArrayList<Point>();
	
	/**
	 * Liste des sommets du triangle
	 */
	private ArrayList<Point> vertices = new ArrayList<Point>();
	
	/**
	 * Liste des côtés du triangle
	 */
	private ArrayList<Segment> segments = new ArrayList<Segment>();

	/**
	 * Polygone formé par ce triangle
	 */
	private Polygon polygon;
	
	/**
	 * Normale au plan dans lequel se trouve le triangle
	 */
	private Vector normal;

	/**
	 * Barycentre du triangle
	 */
	private Point barycenter;

	/**
	 * Matrice contenant les coefficents a, b, c et d de l'équation du plan
	 * contenant ce triangle.
	 */
	private Matrix plan;
	
	public Triangle(HashSet<Point> points, ArrayList<Segment> segments) {
		this.vertices.addAll(points);
		orderSegments(segments);
		setPolygon();
		setNormal();
		updatePointsWithinTriangle();
	}
	
	/**
	 * Met à jour la position du barycentre du triangle
	 */
	public void updateBarycenter() {
		float x = 0, y = 0, z = 0;
		for (Point p : vertices) {
			x += p.getX();
			y += p.getY();
			z += p.getZ();
		}
		x /= 3;
		y /= 3;
		z /= 3;
		barycenter = new Point(x, y, z);
	}
	
	/**
	 * Permet de calculer la couleur en un point du triangle
	 * @param objColor couleur de l'objet
	 * @param lightColor couleur de la lumière
	 * @param light position de la lumière
	 * @param p point testé
	 * @param lightIntensity intensité de lumière
	 * @param objIntensity intensité de la lumière ambiante
	 * @return couleur au point p
	 */
	public Color getColor(Color objColor, Color lightColor, Point light, Point p, int lightIntensity, int objIntensity) {
		Vector lightDirection = new Vector(light, p).getNormalizedVector();
		return getColor(objColor, lightColor, lightDirection, lightIntensity, objIntensity);
	}

	/**
	 * Met à jour la couleur de la face en fonction de la lumière et la renvoie
	 * @param lightColor couleur de la lumière
	 * @param modelColor couleur du modèle
	 * @param lightDirection direction de la lumière
	 * @param lightIntensity intensité de lumière
	 * @param objIntensity intensité de la lumière ambiante
	 * @return couleur de la face
	 */
	public Color getColor(Color modelColor, Color lightColor, Vector lightDirection, int lightIntensity,
			int objIntensity) {
		double prct = getLightIntensity(normal, lightDirection);
		return calculateColor(modelColor, lightColor, (float) prct, lightIntensity, objIntensity);
	}
	
	/**
	 * Mélange la couleur du modèle et la couleur de la lumière en fonction de l'intensité
	 * de lumière à cet endroit.
	 * @param modelColor couleur du modèle
	 * @param lightColor couleur de la lumière
	 * @param colorIntensity intensité de la couleur de la lumière
	 * @param lightIntensity intensité de lumière
	 * @param objIntensity intensité de la lumière ambiante
	 * @return Couleur à cet endroit
	 */
	private Color calculateColor(Color modelColor, Color lightColor, double colorIntensity,
			int lightIntensity, int objIntensity) {
		double fgIntensity = colorIntensity * lightIntensity / 100;
		float fgA = (float) Math.min(fgIntensity, 1);
		float fgR = lightColor.getRed() / 255f;
		float fgG = lightColor.getGreen() / 255f;
		float fgB = lightColor.getBlue() / 255f;

		//Lumière ambiante
		double bgIntensity = 1f * objIntensity / 100;
		float bgA = (float) Math.min(bgIntensity, 1);
		float bgR = modelColor.getRed() / 255f;
		float bgG = modelColor.getGreen() / 255f;
		float bgB = modelColor.getBlue() / 255f;

		float rA = 1 - (1 - fgA) * (1 - bgA);
		int rR = (int) ((fgR * fgA / rA + bgR * bgA * (1 - fgA) / rA) * 255);
		int rG = (int) ((fgG * fgA / rA + bgG * bgA * (1 - fgA) / rA) * 255);
		int rB = (int) ((fgB * fgA / rA + bgB * bgA * (1 - fgA) / rA) * 255);
		return new Color(rR, rG, rB);
	}

	
	/**
	 * Calcule l'intensité de la lumière grâce aux vecteurs passés en paramètre
	 * @param normal vecteur normal au point éxaminé
	 * @param lightDirection direction de la lumière
	 * @return intensité de la lumière au(x) point(s) où se trouve(nt) le vecteur
	 */
	private double getLightIntensity(Vector normal, Vector lightDirection) {
		double prct = Vector.scalarProduct(normal, lightDirection);
		if (prct < 0)
			return 0;
		return prct;
	}
	
	/**
	 * Récupère les points inclus dans le triangle
	 */
	public void updatePointsWithinTriangle() {
		pointsInTriangle = new ArrayList<Point>();
		int x1 = (int)vertices.get(0).getX();
		int x2 = (int)vertices.get(1).getX();
		int x3 = (int)vertices.get(2).getX();
		int y1 = (int)vertices.get(0).getY();
		int y2 = (int)vertices.get(1).getY();
		int y3 = (int)vertices.get(2).getY();

		// On trouve un rectangle englobant le triangle
		int minX = Math.min(Math.min(x1, x2), x3);
		int maxX = Math.max(Math.max(x1, x2), x3);
		int minY = Math.min(Math.min(y1, y2), y3);
		int maxY = Math.max(Math.max(y1, y2), y3);
		Polygon p = new Polygon(new int[]{x1,x2,x3}, new int[]{y1,y2,y3}, 3);

		// On parcourt tous les points du rectangle
		for(int x = minX; x <= maxX; x++) {
			for(int y = minY; y <= maxY; y++) {
				if (p.contains(x, y)) {
					pointsInTriangle.add(new Point(x, y, calculateZ(x, y)));
				}
			}
		}
	}

	/**
	 * Calcule le z du point appartenant au même plan que le triangle 
	 * avec ses coordonnées x et y
	 * @param x du point
	 * @param y du point
	 * @return z du point
	 */
	public float calculateZ(int x, int y) {
		float a = plan.get(0, 0), b = plan.get(0, 1), c = plan.get(0, 2), d = plan.get(0, 3);
		return -1 * (a * x + b * y + d) / c;
	}

	/**
	 * Crée un objet polygon à partir des points du triangle
	 */
	public void setPolygon() {
		int[] xPoints = new int[3], yPoints = new int[3];
		xPoints[0] = (int)vertices.get(0).getX();
		xPoints[1] = (int)vertices.get(1).getX();
		xPoints[2] = (int)vertices.get(2).getX();
		yPoints[0] = (int)vertices.get(0).getY();
		yPoints[1] = (int)vertices.get(1).getY();
		yPoints[2] = (int)vertices.get(2).getY();

		polygon = new Polygon(xPoints, yPoints, 3);
	}

	/**
	 * Calcule la normale au plan auquel appartient le triangle
	 */
	public void setNormal() {
		Point p0 = segments.get(0).getP1(), p1 = segments.get(0).getP2(), 
				p2 = segments.get(1).getP2();

		normal = Vector.getNormal(new Vector(p0, p1), new Vector(p0, p2));
		setPlanEquation(normal);
		normal = normal.getNormalizedVector();
	}

	/**
	 * Renvoie les segments de manière ordonnée. Ainsi, si on suit les segments
	 * de cette liste dans l'ordre, on tourne autour du triangle (on aurait par
	 * exemple pour un triangle ABC, les segments AB, puis BC, puis CA).
	 */
	private void orderSegments(ArrayList<Segment> segments) {
		ArrayList<Segment> seg = new ArrayList<Segment>();
		Segment s1 = segments.get(0), s2 = segments.get(1), s3 = segments.get(2);

		if (s2.getP1()!=s1.getP2() && s2.getP2() != s1.getP2()) {
			s2 = segments.get(2);
			s3 = segments.get(1);
		}
		if (s2.getP1() != s1.getP2())
			s2 = new Segment(s2.getP2(), s2.getP1());
		if (s3.getP1() != s2.getP2())
			s3 = new Segment(s3.getP2(), s3.getP1());

		seg.add(s1);
		seg.add(s2);
		seg.add(s3);
		this.segments = seg;
	}
	
	/**
	 * Détermine l'équation du plan en fonction des paramètres du vecteur normal
	 * @param n vecteur normal à ce triangle
	 */
	private void setPlanEquation(Vector n) {
		Point p = vertices.get(0);
		float a = n.getX(), b = n.getY(), c = n.getZ();
		float d = -1 * (a * p.getX() + b * p.getY() + c * p.getZ());
		plan = new Matrix(new float[][] {{a}, {b}, {c}, {d}});
	}
	public ArrayList<Point> getVertices() {
		return vertices;
	}
	public Point getBarycenter() {
		return barycenter;
	}

	public ArrayList<Segment> getSegments() {
		return segments;
	}
	public ArrayList<Point> getPointsWithinTriangle() {
		return pointsInTriangle;
	}
	public Vector getNormal() {
		return normal;
	}

	public Polygon getPolygon() {
		return polygon;
	}

	@Override
	public int compareTo(Triangle arg0) {
		return (int)arg0.getBarycenter().getZ() - (int)this.getBarycenter().getZ();
	}

	@Override
	public String toString() {
		String ch = "";
		for(Segment s : segments) {
			ch += s.getP1().toString() + "\t" + s.getP2().toString() + "\n";
		}
		ch += "\n";
		return ch;
	}
}
