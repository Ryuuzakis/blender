package com.blender.utils.maths;

/**
 * Classe représentant une transformation 3D
 *
 */
public class Transformation {
	private Matrix matrix;
	
	public Transformation() {
		matrix = new Matrix(4, 4).getIdentity();
	}
	
	/**
	 * Multiplie la matrice courante par la matrice en parametre
	 * @param m matrice a multiplier
	 */
	public void multiplyBy(Matrix m) {
		try {
			matrix = matrix.multiplyBy(m);
		} catch (Exception e) {}
	}
	
	/**
	 * Applique une translation a la matrice
	 * @param x La valeur a ajouter au x
	 * @param y La valeur a ajouter au y
	 * @param z La valeur a ajouter au z
	 */
	public void translate(float tx, float ty, float tz) {
		multiplyBy(getTranslationMatrix(tx, ty, tz));
	}
	
	/**
	 * Applique une homothethie a la matrice
	 * @param sx La valeur a multiplier avec x
	 * @param sy La valeur a multiplier avec y
	 * @param sz La valeur a multiplier avec z
	 */
	public void scale(float sx, float sy, float sz) {
		multiplyBy(getScaleMatrix(sx, sy, sz));
	}
	
	/**
	 * Applique une rotation a la matrice
	 * @param ax L'angle a ajouter sur l'axe x
	 * @param ay L'angle a ajouter sur l'axe y
	 * @param az L'angle a ajouter sur l'axe z
	 */
	public void rotate(float ax, float ay, float az) {
		multiplyBy(getRotationZMatrix(az));
		multiplyBy(getRotationYMatrix(ay));
		multiplyBy(getRotationXMatrix(ax));
	}
	
	/**
	 * Annule la rotation sur la matrice
	 * @param ax l'angle a annuler sur l'axe x
	 * @param ay l'angle a annuler sur l'axe y
	 * @param az l'angle a annuler sur l'axe z
	 */
	public void cancelRotation(float ax, float ay, float az) {
		multiplyBy(getRotationXMatrix(ax));
		multiplyBy(getRotationYMatrix(ay));
		multiplyBy(getRotationZMatrix(az));
	}
	
	/**
	 * Retourne la matrice de transformation
	 * @return Une instance de Matrix
	 */
	public Matrix getMatrix() {
		return matrix;
	}
	
	private Matrix getTranslationMatrix(float tx, float ty, float tz) {
		return new Matrix(new float[][]{{1, 0, 0, tx},
										{0, 1, 0, ty},
										{0, 0, 1, tz},
										{0, 0, 0, 1}});
	}
	
	private Matrix getScaleMatrix(float sx, float sy, float sz) {
		return new Matrix(new float[][]{{sx, 0, 0, 0},
									    {0, sy, 0, 0},
								   	    {0, 0, sz, 0},
									    {0, 0, 0, 1}});
	}
	
	private Matrix getRotationXMatrix(float ax) {
		ax = (float) Math.toRadians(ax);
		float cos = (float) Math.cos(ax);
		float sin = (float) Math.sin(ax);
		return new Matrix(new float[][]{{1, 0, 0, 0},
									    {0, cos, -sin, 0},
										{0, sin, cos, 0},
										{0, 0, 0, 1}});
	}
	
	private Matrix getRotationYMatrix(float ay) {
		ay = (float) Math.toRadians(ay);
		float cos = (float) Math.cos(ay);
		float sin = (float) Math.sin(ay);
		return new Matrix(new float[][]{{cos, 0, sin, 0},
										{0, 1, 0, 0},
										{-sin, 0, cos, 0},
									 	{0, 0, 0, 1}});
	}
	
	private Matrix getRotationZMatrix(float az) {
		az = (float) Math.toRadians(az);
		float cos = (float) Math.cos(az);
		float sin = (float) Math.sin(az);
		return new Matrix(new float[][]{{cos, -sin, 0, 0},
										{sin, cos, 0, 0},
										{0, 0, 1, 0},
										{0, 0, 0, 1}});
	}
}
