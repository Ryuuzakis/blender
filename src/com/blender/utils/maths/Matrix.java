package com.blender.utils.maths;

/**
 * Classe permettant de representer et calculer avec des matrices
 *
 */
public class Matrix {
	
	private float[][] values;
	
	/**
	 * Constructeur de la matrice
	 * @param nbRows le nombre de lignes
	 * @param nbColumns le nombre de colonnes
	 */
	public Matrix(int nbRows, int nbColumns) {
		values = new float[nbColumns][nbRows];
	}
	
	/**
	 * Creer une matrice a partir d'un tableau 
	 * @param values Tableau 2D des valeurs
	 */
	public Matrix(float[][] values) {
		this.values = values;
	}
	
	/**
	 * Creer la matrice identit�e pour la taille de cette matrice
	 * @return Une instance de matrice
	 */
	public Matrix getIdentity() {
		float iden[][] = new float[getNbRows()][getNbColumns()];
		for (int i = 0; i < iden.length; ++i) {
			iden[i][i] = 1;
		}
		return new Matrix(iden);
	}

	/**
	 * Renvoie le nombre de colonnes
	 * @return nombre de colonnes
	 */
	public int getNbColumns() {
		return values[0].length;
	}
	/**
	 * Renvoie le nombre de lignes
	 * @return nombre de lignes
	 */
	public int getNbRows() {
		return values.length;
	}
	
	/**
	 * Methode permettant le produit matriciel entre this et m
	 * @param m matrice a multiplier
	 * @throws Exception Si les matrices ne sont pas de bonne dimension
	 */
	public Matrix multiplyBy(Matrix m) throws Exception {
		if (getNbColumns() != m.getNbRows()) 
			throw new Exception("Impossible de calculer le produit matriciel car les matrices ne sont pas les bonnes dimensions");
		
		float[][] res = new float[getNbRows()][m.getNbColumns()];
		for (int j = 0; j < res.length; ++j) {
			for (int i = 0; i < res[0].length; ++i) {
				for (int k = 0; k < getNbColumns(); ++k) {
					res[j][i] += values[j][k] * m.values[k][i];
				}
			}
		}
		return new Matrix(res);
	}
	
	/**
	 * Retourne la valeur contenu dans la matrice
	 * @param x La position x de la valeur
	 * @param y La position y de la valeur
	 * @return La valeur dans la matrice
	 */
	public float get(int x, int y) {
		return values[y][x];
	}
	
	@Override
	public String toString() {
		String str = "";
		for (int x = 0; x < getNbRows(); ++x) {
			for (int y = 0; y < getNbColumns(); ++y) {
				str += values[x][y] + "\t";
			}
			str += "\n";
		}
		
		return str;
	}
}
