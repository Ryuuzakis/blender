package com.blender.utils.maths;

/**
 * Classe représentant un vecteur
 *
 */
public class Vector {
	
	private float x;
	private float y;
	private float z;
	
	public Vector(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * Crée un vecteur à partir de deux points
	 * @param p1 premier point
	 * @param p2 second point
	 * @return vecteur directeur de la droite passant par p1 et p2
	 */
	public Vector (Point p1, Point p2) {
		this(p2.getX() - p1.getX(), p2.getY() - p1.getY(), p2.getZ() - p1.getZ());
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}
	
	/**
	 * Produit scalaire des vecteurs passés en paramètre
	 * @param v1 vecteur 1
	 * @param v2 vecteur 2
	 * @return résultat du produit scalaire des vecteurs
	 */
	public static float scalarProduct(Vector v1, Vector v2) {
		return v1.getX() * v2.getX() + v1.getY() * v2.getY() + v1.getZ() * v2.getZ();
	}
	
	/**
	 * Méthode donnant la norme de ce vecteur
	 * @return norme du vecteur
	 */
	public double getNorm() {
		return Math.sqrt(x * x + y * y + z * z);
	}
	
	/**
	 * Permet d'obtenir l'angle entre deux vecteurs
	 * @param v1 premier vecteur
	 * @param v2 second vecteur
	 * @return angle entre v1 et v2
	 */
	public static double getAngleBetween(Vector v1, Vector v2) {
		return Math.acos(scalarProduct(v1.getNormalizedVector(), v2.getNormalizedVector()));
	}
	
	/**
	 * Produit vectoriel entre les vecteurs v1 et v2. 
	 * v1 et v2 doivent avoir la même origine.
	 * @param v1 vecteur 1
	 * @param v2 vecteur 2
	 * @return un nouveau vecteur, orthogonal aux vecteurs passés en paramètre
	 */
	private static Vector crossProduct(Vector v1, Vector v2) {
		float det1 = v1.getY() * v2.getZ() - v1.getZ() * v2.getY();
		float det2 = v1.getZ() * v2.getX() - v1.getX() * v2.getZ();
		float det3 = v1.getX() * v2.getY() - v1.getY() * v2.getX();
		return new Vector(det1, det2, det3);
	}
	
	/**
	 * Normalise le vecteur courant, c'est a dire 
	 * conserve la direction mais passe à une norme de 1
	 * @return vecteur normalisé
	 */
	public Vector getNormalizedVector() {
		float norm = (float) getNorm();
		setX(x / norm);
		setY(y  / norm);
		setZ(z / norm);
		return this;
	}
	
	/**
	 * Renvoie deux points se trouvant sur le vecteur this
	 * @return tableau de 2 points
	 */
	public Point[] getTwoPointsOnVector() {
		Vector normalized = getNormalizedVector();
		Point p1 = new Point(normalized.getX(), normalized.getY(), normalized.getZ());
		Point p2 = new Point(normalized.getX() * 30, normalized.getY() * 30, normalized.getZ() * 30);
		return new Point[] {p1, p2};
	}

	@Override
	public String toString() {
		return "[" + x + "," + y + "," + z + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		Vector v = (Vector) obj;
		return x == v.x && y == v.y && z == v.z;
	}

	/**
	 * Methode qui retourne le vecteur normal au plan forme par les deux vecteurs 
	 * en parametre et passant par le point d'intersection entre ces vecteurs
	 * @param u premier vecteur
	 * @param v second vecteur
	 * @return vecteur normal
	 */
	public static Vector getNormal(Vector u, Vector v) {
		Vector normal = Vector.crossProduct(u, v);
		return !hasGoodDirection(normal) ? Vector.crossProduct(v, u) : normal;
	}
	
	/**
	 * Détermine si le vecteur passé en paramètre va dans la 
	 * direction opposée au vecteur de la vue de l'utilisateur
	 * @param v vecteur testé
	 * @return true si le vecteur va en sens opposé, false sinon
	 */
	private static boolean hasGoodDirection(Vector v) {
		return Vector.scalarProduct(v, new Vector(0, 0, -1)) <= 0;
	}
}
