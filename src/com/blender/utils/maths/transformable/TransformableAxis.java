package com.blender.utils.maths.transformable;

import java.util.ArrayList;

import com.blender.utils.maths.Point;

/**
 * Classe representant l'axe de rotation.
 * Cet axe permet de visualiser comment est dispose l'objet dans l'espace
 *
 */
public class TransformableAxis extends Transformable {

	public TransformableAxis(ArrayList<Point> points) {
		super(null, points);
		reset();
	}
	
	/**
	 * Reinitialise la position de l'axe
	 */
	protected void reset() {
		points = new ArrayList<Point>();
		points.add(new Point(25, 0, 0));
		points.add(new Point(0, 25, 0));
		points.add(new Point(0, 0, 25));
	}
	
	public Point getXAxis() {
		return points.get(0);
	}
	
	public Point getYAxis() {
		return points.get(1);
	}
	
	public Point getZAxis() {
		return points.get(2);
	}
}
