package com.blender.utils.maths.transformable;

import java.util.ArrayList;

import com.blender.utils.maths.Matrix;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Transformation;

/**
 * Classe de la boite englobant le modele.
 *
 */
public class TransformableBox extends Transformable {

	/**
	 * Dimensions de la boite
	 */
	private float[] dimensions;
	
	/**
	 * Volume de la boite
	 */
	private float volume;
	protected TransformableBox(ArrayList<Segment> segments, ArrayList<Point> points) {
		super(segments, points);
		updateDimensions();
	}
	
	/**
	 * Deplace le centre de l'objet
	 * @param cx Le nouveau centre en x
	 * @param cy Le nouveau centre en y
	 * @param cz Le nouveau centre en z
	 */
	public void moveCenter(float cx, float cy, float cz) {
		center.setX(cx);
		center.setY(cy);
		center.setZ(cz);
	}

	/**
	 * Calcule la largeur de la boite englobante de l'objet
	 * @return largeur
	 */
	public float getWidth() {
		for (Segment s : getSegments()) {
			Point p1 = s.getP1(), p2 = s.getP2();
			if (p1.getX() != p2.getX())
				return Math.abs(p1.getX() - p2.getX());
		}
		return -1;
	}

	/**
	 * Calcule la hauteur de la boite englobante de l'objet
	 * @return hauteur
	 */
	public float getHeight() {
		for (Segment s : getSegments()) {
			Point p1 = s.getP1(), p2 = s.getP2();
			if (p1.getY() != p2.getY())
				return Math.abs(p1.getY() - p2.getY());
		}
		return -1;
	}

	/**
	 * Calcule la profondeur de la boite englobante de l'objet
	 * @return profondeur
	 */
	public float getDepth() {
		for (Segment s : getSegments()) {
			Point p1 = s.getP1(), p2 = s.getP2();
			if (p1.getZ() != p2.getZ())
				return Math.abs(p1.getZ() - p2.getZ());
		}
		return -1;
	}

	/**
	 * Determine si le point p est contenu dans la boite englobante
	 * @param p Point teste
	 * @return vrai si le point est dans la boite, faux sinon
	 */
	public boolean containsPoint(Point p) {
		float[] minCoords = getMinCoords(), maxCoords = getMaxCoords();
		float xMin = minCoords[0], yMin = minCoords[1], zMin = minCoords[2];
		float xMax = maxCoords[0], yMax = maxCoords[1], zMax = maxCoords[2];
		float xp = p.getX(), yp = p.getY(), zp = p.getZ();
		return (xp >= xMin && xp <= xMax) && (yp >= yMin && yp <= yMax) && (zp >= zMin && zp <= zMax);
	}

	/**
	 * Renvoie un tableau contenant le x, y et z minimums de la boite englobante
	 * @return tab[0] = x minimal, tab[1] = y  minimal, tab[2] = z minimal
	 */
	private float[] getMinCoords() { 
		float minX, minY, minZ;
		Point p = getPoints().get(0);
		minX = p.getX();
		minY = p.getY();
		minZ = p.getZ();
		for (int i = 1; i < getPoints().size(); i++) {
			p = getPoints().get(i); 
			if (p.getX() < minX)
				minX = p.getX();
			if (p.getY() < minY)
				minY = p.getY();
			if (p.getZ() < minZ)
				minZ = p.getZ();
		}
		return new float[] {minX, minY, minZ};
	}

	/**
	 * Renvoie un tableau contenant le x, y et z maximums de la boite englobante
	 * @return tab[0] = x maximum, tab[1] = y  maximum, tab[2] = z maximum
	 */
	private float[] getMaxCoords() { 
		float maxX = 0, maxY = 0, maxZ = 0;
		Point p = getPoints().get(0);
		maxX = p.getX();
		maxY = p.getY();
		maxZ = p.getZ();
		for (int i = 1; i < getPoints().size(); i++) {
			p = getPoints().get(i);
			if (p.getX() > maxX)
				maxX = p.getX();
			if (p.getY() > maxY)
				maxY = p.getY();
			if (p.getZ() > maxZ)
				maxZ = p.getZ();
		}
		return new float[] {maxX, maxY, maxZ};
	}
	
	/**
	 * Calcul le volume de la boite englobante
	 */
	private void updateDimensions() {
		ArrayList<Point> oldPoints = getPoints();
		ArrayList<Point> newPoints = new ArrayList<Point>();
		for (Point p : getPoints()) {
			newPoints.add(new Point(p.getX(), p.getY(), p.getZ()));
		}
		setPoints(newPoints);
		
		// On remet l'objet en etat initial pour faire les calcules de dimension
		Matrix m = reverseRotation.getMatrix();

		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(scale.getX(), scale.getY(), scale.getZ());
		transfo.multiplyBy(m);
		transfo.scale(1/scale.getX(), 1/scale.getY(), 1/scale.getZ());
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());

		// On applique la transformation
		apply(transfo);
		float rect[] = getRect();
		dimensions = new float[]{rect[3]/10, rect[4]/10, rect[5]/10};
		volume = dimensions[0] * dimensions[1] * dimensions[2];
		// On restaure les points
		setPoints(oldPoints);
	}
	
	@Override
	public void scale(float sx, float sy, float sz) {
		super.scale(sx, sy, sz);
		updateDimensions();
	}
	
	/** 
	 * Renvoie le volume de la boite englobante
	 * @return valeur du volume
	 */
	public float getVolume() {
		return volume;
	}
	
	/**
	 * Renvoie les dimensions de la boite englobante
	 * @return Tableau avec les dimensions {largeur, hauteur, profondeur}
	 */
	public float[] getDimensions() {
		return dimensions;
	}
}
