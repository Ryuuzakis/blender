package com.blender.utils.maths.transformable;

import java.util.ArrayList;

import com.blender.utils.maths.Matrix;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Transformation;

/**
 * Classe abstraite representant les elements communs a tous les objets
 * pouvant subir des transformations dans l'espace
 *
 */
public abstract class Transformable {

	protected ArrayList<Segment> segments;
	protected ArrayList<Point> points;

	protected Point center;
	protected Point scale;
	protected Point rotation;
	
	/**
	 * Transformation qui memorise les transformations a effectuer
	 * pour revenir a la position de depart.
	 */
	protected Transformation reverseRotation;
	
	public Transformable(ArrayList<Segment> segments, ArrayList<Point> points) {
		this.setSegments(segments);
		this.setPoints(points);

		center = new Point(0, 0, 0);
		rotation = new Point(0, 0, 0);
		reverseRotation = new Transformation();
		scale = new Point(1, 1, 1);
	}

	/**
	 * Deplace le centre de l'objet
	 * @param cx Le nouveau centre en x
	 * @param cy Le nouveau centre en y
	 * @param cz Le nouveau centre en z
	 */
	public void moveCenter(float cx, float cy, float cz) {
		center.setX(cx);
		center.setY(cy);
		center.setZ(cz);
	}
	
	/**
	 * Applique une transformation sur les positions de l'objet 3D
	 * @param t La transformation a appliquer
	 */
	public void apply(Transformation t) {
		transformPointsSimply(t);
	}
	protected void transformPointsSimply(Transformation t) {
		for (Point p : getPoints())
			applyTransformation(p, t);
	}
	protected void applyTransformation(Point p, Transformation t) {
		try {
			Matrix sPoint = t.getMatrix().multiplyBy(p.getMatrix());
			p.setX(sPoint.get(0, 0));
			p.setY(sPoint.get(0, 1));
			p.setZ(sPoint.get(0, 2));
		} catch (Exception e) {}
	}

	/**
	 * Deplace l'objet en incrementant les coordonnees
	 * @param x La valeur a ajouter au x de l'objet
	 * @param y La valeur a ajouter au y de l'objet
	 * @param z La valeur a ajouter au z de l'objet
	 */
	public void move(float x, float y, float z) {
		Transformation transfo = new Transformation();
		transfo.translate(x, y, z);
		apply(transfo);

		moveCenter(center.getX() + x, center.getY() + y, center.getZ() + z);
	}

	/**
	 * Deplace l'objet au coordonees specifie
	 * @param x La nouvelle position x de l'objet
	 * @param y La nouvelle position y de l'objet
	 * @param z La nouvelle position z de l'objet
	 */
	public void moveTo(float x, float y, float z) {
		move(x - center.getX(), y - center.getY(), z - center.getZ());
	}

	/**
	 * Effectue une rotation de l'objet
	 * Les valeurs sont en degrees
	 * @param ax Angle de rotation de l'axe x
	 * @param ay Angle de rotation de l'axe y
	 * @param az Angle de rotation de l'axe z
	 */
	public void rotate(float ax, float ay, float az) {
		// On cree la transformation
		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(scale.getX(), scale.getY(), scale.getZ());
		transfo.rotate(ax, ay, az);
		transfo.scale(1/scale.getX(), 1/scale.getY(), 1/scale.getZ());
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());
		// On applique la transformation
		apply(transfo);
		// On met a jour la matrice pour inverser la rotation
		reverseRotation.cancelRotation(-ax, -ay, -az);
	}

	public void rotateTo(float x, float y, float z) {
		// On cree la transformation
		Matrix m = reverseRotation.getMatrix();

		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(scale.getX(), scale.getY(), scale.getZ());
		transfo.multiplyBy(m);
		transfo.scale(1/scale.getX(), 1/scale.getY(), 1/scale.getZ());
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());

		// On applique la transformation
		apply(transfo);

		// On reinitialise la transformation inverse
		reverseRotation = new Transformation();
		rotate(x, y, z);
	}

	/**
	 * Adapte le model a une dimension
	 * @param width Largeur de la dimension
	 * @param height Hauteur de la dimension
	 */
	public void adaptScalingTo(int width, int height) {
		float[] rect = getRect();
		float m = Math.max(Math.max(rect[3], rect[4]), rect[5]);
		// Change sa taille au maximum possible pour la dimension
		float maxScale = Math.min(width/m, height/m);
		scale(maxScale, maxScale, maxScale);
	}

	/**
	 * Adapte le model a une dimension et le centre
	 * @param width Largeur de la dimension
	 * @param height Hauteur de la dimension
	 */
	public void adaptTo(int width, int height) {
		adaptScalingTo(width, height);

		// Centre l'objet
		float[] rect = getRect();
		moveCenter(rect[3]/2 + rect[0], rect[4]/2 + rect[1], rect[5]/2 + rect[2]);
		moveTo(width/2, height/2, 0);
	}
	
	/**
	 * Redimensionne l'objet
	 * Si la valeur est inferieur a 1, l'objet est retreci
	 * Si la valeur est superieur a 1, l'objet est agrandi
	 * Si la valeur est egal a 1, l'objet reste inchange
	 * @param sx Le coefficient de redimensionnement en x
	 * @param sy Le coefficient de redimensionnement en y
	 * @param sz Le coefficient de redimensionnement en z
	 */
	public void scale(float sx, float sy, float sz) {
		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(sx, sy, sz);
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());
		apply(transfo);
		scale.setX(scale.getX() * sx);
		scale.setY(scale.getY() * sy);
		scale.setZ(scale.getZ() * sz);
	}


	/**
	 * Redimensionne l'objet a une dimension precise
	 * @param sx Le facteur de redimensionnement en x
	 * @param sy Le facteur de redimensionnement en y
	 * @param sz Le facteur de redimensionnement en z
	 */
	public void scaleTo(float sx, float sy, float sz) {
		Point s = getScale();
		scale(1/s.getX(), 1/s.getY(), 1/s.getZ());
		scale(sx, sy, sz);
	}

	/**
	 * Calcule le rectangle de l'objet en trouvant les valeurs minimal/maximal en x, y et z
	 * Contenu du tableau :
	 * 		{minX, minY, minZ, largeur, hauteur, profondeur}
	 * @return Le rectangle des dimensions de l'objet a l'ecran
	 */
	public float[] getRect() {
		float minX = points.get(0).getX();
		float maxX = points.get(0).getX();
		float minY = points.get(0).getY();
		float maxY = points.get(0).getY();
		float minZ = points.get(0).getZ();
		float maxZ = points.get(0).getZ();

		for (Point p : points) {
			if (p.getX() < minX) minX = p.getX();
			if (p.getX() > maxX) maxX = p.getX();
			if (p.getY() < minY) minY = p.getY();
			if (p.getY() > maxY) maxY = p.getY();
			if (p.getZ() < minZ) minZ = p.getZ();
			if (p.getZ() > maxZ) maxZ = p.getZ();
		}

		return new float[]{minX, minY, minZ, maxX - minX, maxY - minY, maxZ - minZ};
	}
	
	public ArrayList<Point> getPoints() {
		return points;
	}
	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}
	public ArrayList<Segment> getSegments() {
		return segments;
	}
	public void setSegments(ArrayList<Segment> segments) {
		this.segments = segments;
	}
	public Point getCenter() {
		return center;
	}
	public Point getRotation() {
		return rotation;
	}
	public Point getScale() {
		return scale;
	}
}
