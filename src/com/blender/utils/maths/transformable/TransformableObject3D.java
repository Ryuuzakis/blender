package com.blender.utils.maths.transformable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.blender.models.PropertiesModel;
import com.blender.utils.maths.Matrix;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Transformation;
import com.blender.utils.maths.Triangle;
import com.blender.utils.maths.Vector;

/**
 * Classe representant un modele, objet que l'on peut visualiser en 3D. 
 *
 */
public class TransformableObject3D extends Transformable{

	protected ArrayList<Triangle> faces;
	private String name, modelName;

	private final Point DEFAULT_LIGHT = new Point(20, 20, 0);
	private Point light = new Point(20, 20, 0);

	/**
	 * Vecteur direction entre la lumiere et le centre de l'objet
	 */
	private Vector lightDirection;
	
	/**
	 * Vecteur direction de la lumiere lorsque l'on a la lumiere du soleil
	 */
	private final Vector SUNLIGHT_DIRECTION = new Vector(1, 1, 0);

	/**
	 * Boite englobante de l'objet3D
	 */
	private TransformableBox englobingBox;

	/**
	 * Axe montrant l'orientation de l'objet
	 */
	private TransformableAxis axis;
	
	private PropertiesModel properties;

	public TransformableObject3D(ArrayList<Triangle> faces, ArrayList<Segment> segments, ArrayList<Point> points, String name) {
		super(segments, points);
		setFaces(faces);
		this.name = name;
		createAxis();
		createEnglobingBox();
	}
	
	public PropertiesModel getProperties() {
		return properties;
	}
	/**
	 * Cree l'objet transformableAxis
	 */
	private void createAxis() {
		ArrayList<Point> axisPoints = new ArrayList<Point>();
		axisPoints.add(new Point(25, 0, 0));
		axisPoints.add(new Point(0, 25, 0));
		axisPoints.add(new Point(0, 0, 25));
		axis = new TransformableAxis(axisPoints);
	}

	/**
	 * Cree la boite englobante de l'objet
	 */
	private void createEnglobingBox() {
		ArrayList<Point> boxPoints = getEnglobingBoxPoints();
		ArrayList<Segment> boxSegments = getEnglobingBoxSegments(boxPoints);
		englobingBox = new TransformableBox(boxSegments, boxPoints);
	}

	/**
	 * Cree les segments composant la boite englobante
	 * @param boxPoints Points composant la boite englobante
	 * @return liste de segments
	 */
	private ArrayList<Segment> getEnglobingBoxSegments(ArrayList<Point> boxPoints) {
		ArrayList<Segment> boxSegments = new ArrayList<Segment>();
		Point[] pointsTab = boxPoints.toArray(new Point[1]);

		//Segments formant le parallelepipede
		boxSegments.add(new Segment(pointsTab[0], pointsTab[1]));
		boxSegments.add(new Segment(pointsTab[0], pointsTab[2]));
		boxSegments.add(new Segment(pointsTab[0], pointsTab[4]));
		boxSegments.add(new Segment(pointsTab[3], pointsTab[2]));
		boxSegments.add(new Segment(pointsTab[3], pointsTab[1]));
		boxSegments.add(new Segment(pointsTab[3], pointsTab[7]));
		boxSegments.add(new Segment(pointsTab[6], pointsTab[2]));
		boxSegments.add(new Segment(pointsTab[6], pointsTab[4]));
		boxSegments.add(new Segment(pointsTab[6], pointsTab[7]));
		boxSegments.add(new Segment(pointsTab[5], pointsTab[1]));
		boxSegments.add(new Segment(pointsTab[5], pointsTab[4]));
		boxSegments.add(new Segment(pointsTab[5], pointsTab[7]));
		return boxSegments;
	}

	/**
	 * Cree les points composant la boite englobante
	 * @return liste des points
	 */
	private ArrayList<Point> getEnglobingBoxPoints() {
		Point p = points.get(0);
		float minX = p.getX(), minY = p.getY(), minZ = p.getZ(),
				maxX = minX, maxY = minY, maxZ = minZ;
		for (int i = 1; i < points.size(); i++) {
			p = points.get(i);
			if (p.getX() < minX)
				minX = p.getX();
			else if (p.getX() > maxX)
				maxX = p.getX();
			if (p.getY() < minY)
				minY = p.getY();
			else if (p.getY() > maxY)
				maxY = p.getY();
			if (p.getZ() < minZ)
				minZ = p.getZ();
			else if (p.getZ() > maxZ)
				maxZ = p.getZ();
		}
		ArrayList<Point> boxPoints = new ArrayList<Point>();
		boxPoints.add(new Point(minX, minY, minZ));
		boxPoints.add(new Point(maxX, minY, minZ));
		boxPoints.add(new Point(minX, maxY, minZ));
		boxPoints.add(new Point(maxX, maxY, minZ));
		boxPoints.add(new Point(minX, minY, maxZ));
		boxPoints.add(new Point(maxX, minY, maxZ));
		boxPoints.add(new Point(minX, maxY, maxZ));
		boxPoints.add(new Point(maxX, maxY, maxZ));

		return boxPoints;
	}
	/**
	 * Methode permettant d'appliquer la transformation sur les points de l'objet en 
	 * utilisant plusieurs coeurs du processeur
	 * @param t transformation a appliquer
	 */
	private void transformPointsMultiCore(final Transformation t) {
		final int nbCore = Runtime.getRuntime().availableProcessors();
		final int partSize = getPoints().size()/nbCore;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {
					int s = n * partSize;
					int max = (n == nbCore-1 ? getPoints().size() : s + partSize);
					for (int i = s; i < max; ++i) {
						applyTransformation(getPoints().get(i), t);
					}
				}

			});
		}

		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {}
	}

	private void multiCoreFacesUpdate(final boolean updateNormals) {
		final int nbCore = Runtime.getRuntime().availableProcessors();
		final int partSize = faces.size()/nbCore;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {
					int s = n * partSize;
					int max = (n == nbCore-1 ? faces.size() : s + partSize);
					if (updateNormals) {
						for (int i = s; i < max; ++i) {
							faces.get(i).setNormal();
							faces.get(i).updateBarycenter();
							faces.get(i).setPolygon();
							faces.get(i).updatePointsWithinTriangle();
						}
					} else {
						for (int i = s; i < max; ++i) {
							faces.get(i).updateBarycenter();
							faces.get(i).setPolygon();
							faces.get(i).updatePointsWithinTriangle();
						}
					}
				}

			});
		}
		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {}
	}

	private void updateAxis(float ax, float ay, float az) {
		Transformation transfo = new Transformation();
		transfo.rotate(ax, ay, az);

		axis.apply(transfo);
	}

	/**
	 * Renvoie le vecteur direction de la lumiere
	 * @return vecteur directeur de la lumiere
	 */
	public Vector getLightDirection() {
		if (lightDirection == null)
			updateLightDirection();
		if (properties != null && properties.getProperty(PropertiesModel.O_LIGHT_SUN))
			return SUNLIGHT_DIRECTION;
		return lightDirection;
	}

	/**
	 * Met a jour le vecteur direction de la lumiere
	 */
	private void updateLightDirection() {
		lightDirection = new Vector(light, getCenter()).getNormalizedVector();
	}
	public void resetLightPosition() {
		moveLight(DEFAULT_LIGHT.getX(), DEFAULT_LIGHT.getY(), DEFAULT_LIGHT.getZ());
		updateLightDirection();
	}

	/**
	 * Deplace la source lumineuse
	 * @param px x de la source sur l'ecran
	 * @param py y de la source sur l'ecran
	 * @param pz z de la source sur l'ecran
	 */
	public void moveLight(float px, float py, float pz) {
		light.setX(px);
		light.setY(py);

		if (englobingBox.containsPoint(new Point(px, py, center.getZ()))) {
			float lightZ = 0;
			for (int i = getFaces().size()-1; i >= 0; --i) {
				Triangle tri = getFaces().get(i);
				if (tri.getPolygon().contains(px, py)) {
					lightZ = tri.calculateZ((int) px, (int) py);
					break;
				}
			}
			if (lightZ == 0)
				light.setZ(light.getZ());
			else if (lightZ > 0)
				lightZ = 0;

			if (lightZ != 0 && Math.abs(lightZ) - Math.abs(light.getZ()) < -30)
				light.setZ(light.getZ() + 1);
			else if (lightZ != 0)
				light.setZ(lightZ - getDepth());
		} else
			light.setZ(center.getZ() - getDepth());

		updateLightDirection();
	}

	private void updateLightPosition() {
		moveLight(light.getX(), light.getY(), light.getZ());
	}

	@Override
	public void rotate(float ax, float ay, float az) {
		updateAxis(ax, ay, az);
		// On cree la transformation
		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(scale.getX(), scale.getY(), scale.getZ());
		transfo.rotate(ax, ay, az);
		transfo.scale(1/scale.getX(), 1/scale.getY(), 1/scale.getZ());
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());
		// On applique la transformation
		apply(transfo, true);
		// On met a jour la matrice pour inverser la rotation
		reverseRotation.cancelRotation(-ax, -ay, -az);

		// On met a jour les variables de rotation
		rotation.setX((float) Math.toDegrees(Vector.getAngleBetween(new Vector(1, 0, 0), axis.getXAxis().getVector().getNormalizedVector())));
		rotation.setY((float) Math.toDegrees(Vector.getAngleBetween(new Vector(0, 1, 0), axis.getYAxis().getVector().getNormalizedVector())));
		rotation.setZ((float) Math.toDegrees(Vector.getAngleBetween(new Vector(0, 0, 1), axis.getZAxis().getVector().getNormalizedVector())));
	}

	@Override
	public void rotateTo(float x, float y, float z) {
		axis.reset();
		// On cree la transformation
		Matrix m = reverseRotation.getMatrix();

		Transformation transfo = new Transformation();
		transfo.translate(center.getX(), center.getY(), center.getZ());
		transfo.scale(scale.getX(), scale.getY(), scale.getZ());
		transfo.multiplyBy(m);
		transfo.scale(1/scale.getX(), 1/scale.getY(), 1/scale.getZ());
		transfo.translate(-center.getX(), -center.getY(), -center.getZ());

		// On applique la transformation
		apply(transfo, true);

		// On reinitialise la transformation inverse
		reverseRotation = new Transformation();
		rotate(x, y, z);
	}

	@Override
	public void apply(Transformation t) {
		apply(t, false);
	}

	/**
	 * Applique la transformation t sur l'objet
	 * @param t transformation
	 * @param updateNormals true s'il faut mettre a jour les normales, false sinon
	 * 	(depend de la transformation effectuee)
	 */
	public void apply(Transformation t, boolean updateNormals) {
		transformPoints(t);
		multiCoreFacesUpdate(updateNormals);

		//On update la lumiere pour qu'elle prenne en compte la nouvelle position du modele
		updateLightPosition();
		updateLightDirection();

		// On trie les triangles si on est en algorithme du peintre
		if (properties == null || !properties.getProperty(PropertiesModel.O_ZBUFFER))
			Collections.sort(faces);
	}

	@Override
	public void adaptTo(int width, int height) {
		super.adaptTo(width, height);
		updateLightPosition();
		updateLightDirection();
	}

	/**
	 * Applique la transformation t sur les points de l'objet
	 * @param t transformation a appliquer
	 */
	private void transformPoints(Transformation t) {
		if (getPoints().size() > 1000) {
			transformPointsMultiCore(t);
		} else {
			transformPointsSimply(t);
		}
	}

	@Override 
	public void move(float x, float y, float z) {
		Transformation transfo = new Transformation();
		transfo.translate(x, y, z);
		apply(transfo, true);

		moveCenter(center.getX() + x, center.getY() + y, center.getZ() + z);
	}
	public void setProperties(PropertiesModel properties) {
		this.properties = properties;
	}
	@Override
	public String toString() {
		return name;
	}
	public ArrayList<Triangle> getFaces() {
		return faces;
	}
	public void setFaces(ArrayList<Triangle> faces) {
		this.faces = faces;
	}

	public String getName() {
		return name;
	}
	public TransformableAxis getAxis() {
		return axis;
	}
	public TransformableBox getEnglobingBox() {
		return englobingBox;
	}
	public Point getLight() {
		return light;
	}
	public float getWidth() {
		return englobingBox.getWidth();
	}
	public float getHeight() {
		return englobingBox.getHeight();
	}
	public float getDepth() {
		return englobingBox.getDepth();
	}
	public boolean containsPoint(Point p) {
		return englobingBox.containsPoint(p);
	}

	public void setModelName(String name) {
		this.modelName = name;
	}
	
	public String getModelName() {
		return this.modelName;
	}

	public void sortFaces() {
		Collections.sort(faces);
	}
}
