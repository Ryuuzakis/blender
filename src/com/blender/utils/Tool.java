package com.blender.utils;

/**
 * Enumeration des differents outils permettant d'agir sur les modeles
 *
 */
public enum Tool {
	TRANSLATION("translation", true), ROTATION("rotation", false), SCALE("redimensionnement"), 
	LIGHT("lumière"), OPTIONS("options"), INFORMATION("informations");
	
	private String value;
	private boolean wireMode;
	
	private Tool(String value, boolean wireMode) {
		this.value = value;
		this.wireMode = wireMode;
	}
	
	private Tool(String value) {
		this(value, false);
	}
	
	
	public String getValue() {
		return value;
	}
	
	/**
	 * Determine si l'outil utilise le mode fil de fer
	 * @return Vrai si l'outil utilise le fil de fer, faux sinon
	 */
	public boolean useWireMode() {
		return wireMode;
	}

	/**
	 * Active le mode fil de fer pour l'outil
	 * @param enable Vrai si l'outil doit utiliser le mode fil de fer, faux sinon
	 */
	public void enableWireMode(boolean enable) {
		wireMode = enable;
	}
}
