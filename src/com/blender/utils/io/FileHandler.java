package com.blender.utils.io;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JOptionPane;

import com.blender.utils.database.DBUpdate;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Triangle;
import com.blender.utils.maths.Vector;
import com.blender.utils.maths.transformable.TransformableObject3D;
import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Classe permettant l'import/export de fichiers et le parsing de ces fichiers
 *
 */
public class FileHandler {

	public final static String OBJECT3D_PATH = "data/models/";

	private static final String GTS_TYPE = ".gts";
	private static final String OBJ_TYPE = ".obj";

	private static String fileType;

	/**
	 * Obtient un model depuis un fichier, et verifie si le fichier est au bon format
	 * @param file fichier source
	 * @return le model cree depuis le fichier source. Si le fichier est corrompu, le model sera null
	 * @throws Exception 
	 */
	private static TransformableObject3D getModelFrom(File file) throws Exception {
		fileType = "";
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String current;
		ArrayList<Point> points = new ArrayList<Point>();
		ArrayList<Segment> segments = new ArrayList<Segment>();
		ArrayList<Triangle> faces = new ArrayList<Triangle>();
		
		int line = 0;
		do {
			current = removeCommentariesInLine(reader.readLine());
			line++;
		} while (current.isEmpty());
		fileType = getFileType(reader, current);
		int[] itemQuantities = null;
		if (fileType == GTS_TYPE) {
			itemQuantities = getItemQuantities(reader, current);
			current = reader.readLine();
		} else if (current.startsWith("o"))
			current = reader.readLine();

		do {
			current = removeCommentariesInLine(current);
			if (current.isEmpty()) {
				line ++;
				continue;
			}
			if (current.startsWith("o"))
				break;

			if (fileType == GTS_TYPE)
				addGTSElement(current, itemQuantities, points, segments, faces, line);
			else
				addOBJElement(current, points, segments, faces, line);
			line ++;
		} while ((current = reader.readLine()) != null);

		reader.close();
		return new TransformableObject3D(faces, segments, points, file.getName());
	}
	/**
	 * Obtenir la quantite d'items
	 * @param file fichier a tester
	 * @return tableau contenant les quantites d'items
	 * @throws Exception 
	 */
	private static int[] getItemQuantities(BufferedReader reader, String current) throws Exception {
		int[] itemQuantities = new int[3];
		String[] tmp = current.split(" ");
		itemQuantities[0] = Integer.parseInt(tmp[0]);
		itemQuantities[1] = Integer.parseInt(tmp[1]);
		itemQuantities[2] = Integer.parseInt(tmp[2]);
		return itemQuantities;
	}

	/**
	 * Methode renvoyant le type du fichier actuellement parse
	 * @param reader BufferedReader contenant le fichier parse
	 * @return une constante indiquant le type de fichier
	 * @throws Exception 
	 */
	private static String getFileType(BufferedReader reader, String current) throws Exception {
		if (current.startsWith("o") || current.startsWith("v"))
			return OBJ_TYPE;

		if (current.matches("^([0-9]+ [0-9]+ [0-9]+)$"))
			return GTS_TYPE;
			
		throw new Exception("Fichier corrompu : impossible de déterminer le format");
	}

	/**
	 * Ajoute l'élément contenu dans la chaine current
	 * @param current  current chaine a examiner
	 * @param itemQuantities quantite finale de chaque element
	 * @param points liste des points
	 * @param segments liste des segments
	 * @param faces liste des faces
	 * @param line indique la ligne courante
	 * @throws Exception
	 */
	private static void addGTSElement(String current, int[] itemQuantities, ArrayList<Point> points, 
			ArrayList<Segment> segments, ArrayList<Triangle> faces, int line) throws Exception {
		if  (points.size() < itemQuantities[0])
			addPoint(current, points, line);
		else if (segments.size() < itemQuantities[1])
			addSegment(current, segments, points, line);
		else if (faces.size() < itemQuantities[2])
			addFace(current, faces, segments, line);
	}

	/**
	 * Ajoute l'élément contenu dans la chaine current
	 * @param current  current chaine a examiner
	 * @param points liste des points
	 * @param segments liste des segments
	 * @param faces liste des faces
	 * @param line indique la ligne courante
	 * @throws Exception
	 */
	private static void addOBJElement(String current, ArrayList<Point> points,
			ArrayList<Segment> segments, ArrayList<Triangle> faces, int line) throws Exception {
		String[] split = current.split(" +");
		String data = "";
		for (int i = 1; i < split.length; i++) {
			data += split[i] + " ";
		}
		if (split[0].startsWith("v"))
			addPoint(data, points, line);
		else if (split[0].startsWith("f"))
			addFaceOBJ(data, points, segments, faces, line);
		else
			throw new Exception("Type de ligne inconnu");
	}
	/**
	 * Ajoute la face de la ligne courante et les segments formes par celle ci 
	 * @param data ligne étudiée
	 * @param pts points de l'objet
	 * @param segments segments de l'objet
	 * @param faces faces de l'objet
	 * @param line ligne courante
	 * @throws Exception
	 */
	private static void addFaceOBJ(String data, ArrayList<Point> pts, ArrayList<Segment> segments,
			ArrayList<Triangle> faces, int line) throws Exception {
		// On récupère les triplés pt/vt/vn
		String[] split = data.split(" +");
		ArrayList<Integer> pointsIdx = new ArrayList<Integer>();
		try {
			//On decoupe les triplets pour ne garder que les pts. On soustrait 1 car
			// dans ce format, le décompte des points commence à 1
			for (String ch : split)
				pointsIdx.add(Integer.parseInt(ch.split("/")[0]) - 1);
		} catch (Exception e) {
			throw new Exception("Ligne " + line + " corrompue");
		}
		if (pointsIdx.size() > 3) 
			triangulateSquare(pointsIdx, pts, segments, faces, line);
		else
			addTriangleOBJ(pointsIdx, pts, segments, faces, line);

	}

	/**
	 * Permet de découper un quadrilatère en deux triangles
	 * @param pointsIdx indice des points formant le quadrilatère
	 * @param pts points de l'objet
	 * @param segments segments de l'objet
	 * @param faces faces de l'objet
	 * @param line line actuellement parsée
	 * @throws Exception
	 */
	private static void triangulateSquare(ArrayList<Integer> pointsIdx, ArrayList<Point> pts, ArrayList<Segment> segments,
			ArrayList<Triangle> faces, int line) throws Exception {
		ArrayList<Point> squarePoints = new ArrayList<Point>();
		try {
			for (Integer i : pointsIdx) {
				squarePoints.add(pts.get(i));
			}
		} catch (Exception e) {
			throw new Exception("L'un des points demandés dans les faces n'existe pas (face ligne " + line + ")");
		}
		float maxDist = 0;
		Point[] diagoPoints = new Point[2];
		Point[] otherPoints = new Point[2];
		for (Point p1 : squarePoints) {
			for (Point p2 : squarePoints) {
				Vector v = new Vector(p1, p2);
				float norm = (float) v.getNorm();
				if (maxDist < norm) {
					maxDist = norm;
					diagoPoints[0] = p1;
					diagoPoints[1] = p2;
				}
			}
		}
		for (Point p : squarePoints) {
			if (!p.equals(diagoPoints[0]) && !p.equals(diagoPoints[1]))
				if (otherPoints[0] == null)
					otherPoints[0] = p;
				else
					otherPoints[1] = p;
		}
		for (int i = 0; i < otherPoints.length; i++) {
			ArrayList<Segment> faceSegments = new ArrayList<Segment>();
			faceSegments.add(new Segment(diagoPoints[0], diagoPoints[1]));
			faceSegments.add(new Segment(diagoPoints[0], otherPoints[i]));
			faceSegments.add(new Segment(diagoPoints[1], otherPoints[i]));
			segments.addAll(faceSegments);
			
			HashSet<Point> pointsSet = new HashSet<Point>();
			pointsSet.add(diagoPoints[0]);
			pointsSet.add(diagoPoints[1]);
			pointsSet.add(otherPoints[i]);
			faces.add(new Triangle(pointsSet, faceSegments));
		}
	}
	
	/**
	 * Ajoute un triangle a l'objet actuel pour les types OBJ
	 * @param pointsIdx indice des points formant le quadrilatere
	 * @param pts points de l'objet
	 * @param segments segments de l'objet
	 * @param faces faces de l'objet
	 * @param line line actuellement parsée
	 * @throws Exception
	 */
	private static void addTriangleOBJ(ArrayList<Integer> pointsIdx, ArrayList<Point> pts, ArrayList<Segment> segments,
			ArrayList<Triangle> faces, int line) throws Exception {
		Point[] points = new Point[3];
		try {
			for (int i = 0; i < pointsIdx.size(); i++)
				points[i] = pts.get(pointsIdx.get(i));
		} catch (Exception e) {
			throw new Exception("Numero de point demandé n'existe pas ligne " + line);
		}

		//On construit les segments a partir des points récupérés
		ArrayList<Segment> faceSegments = new ArrayList<Segment>();
		faceSegments.add(new Segment(points[0], points[1]));
		faceSegments.add(new Segment(points[0], points[2]));
		faceSegments.add(new Segment(points[1], points[2]));
		segments.addAll(faceSegments);

		HashSet<Point> ptsSet = new HashSet<Point>();
		for (Point p : points)
			ptsSet.add(p);

		Triangle face = new Triangle(ptsSet, faceSegments);
		faces.add(face);
	}
	/**
	 * Methode permettant de vider les lignes qui contiennent des informations
	 * qui ne sont pas utiles pour le parsing
	 * @param line ligne a tester
	 * @return ligne epuree des elements inutiles
	 */
	private static String removeCommentariesInLine(String line) {
		if (line.startsWith("#") || line.startsWith("g") || line.startsWith("usemtl")
				|| line.startsWith("vt") || line.startsWith("vn") || line.startsWith(" +")
				|| line.startsWith("s") || line.startsWith("mtllib")) 
			return "";
		return line;
	}

	/**
	 * Ajoute une face a la liste des faces
	 * @param current chaine contenant la face
	 * @param faces liste des faces
	 * @param segments liste des segments
	 * @throws Exception 
	 */
	private static void addFace(String current, ArrayList<Triangle> faces, ArrayList<Segment> segments, int line) throws Exception {
		String[] numbers = current.split(" ");
		if (numbers.length != 3) {
			throw new Exception("Il y a plus ou moins de 3 éléments pour la face ligne " + line);
		}
		Segment s1 = segments.get(Integer.parseInt(numbers[0]) - 1);
		Segment s2 = segments.get(Integer.parseInt(numbers[1]) - 1);
		Segment s3 = segments.get(Integer.parseInt(numbers[2]) - 1);
		if (Segment.haveOneAndOnlyPointInCommon(s1, s2) && Segment.haveOneAndOnlyPointInCommon(s1, s3)
				&& Segment.haveOneAndOnlyPointInCommon(s3, s2)) {
			HashSet<Point> points = new HashSet<Point>();
			points.add(s1.getP1());
			points.add(s1.getP2());
			points.add(s2.getP1());
			points.add(s2.getP2());
			points.add(s3.getP1());
			points.add(s3.getP2());

			ArrayList<Segment> seg = new ArrayList<Segment>();
			seg.add(s1);
			seg.add(s2);
			seg.add(s3);
			faces.add(new Triangle(points, seg));
		} else throw new Exception("Les segments ont un point non en commun dans le triangle à la ligne " + line);
	}

	/**
	 * Ajoute un segment a la liste des segments
	 * @param current chaine contenant le nouveau segment
	 * @param segments liste des segments
	 * @param points liste des points
	 * @return vrai si le segment a bien ete ajoute, faux sinon
	 * @throws Exception 
	 */
	private static void addSegment(String current, ArrayList<Segment> segments, ArrayList<Point> points, int line) throws Exception {
		String[] numbers = current.split(" ");
		if (numbers.length != 2)
			throw new Exception("il y a plus/moins de 2 élements pour le segment à la ligne : " + line);

		Point p1 = null, p2 = null;
		try {
			p1 = points.get(Integer.parseInt(numbers[0]) - 1);
		} catch (Exception e) {
			throw new Exception("Le point " + numbers[0] + " n'existe pas");
		}
		try {
			p2 = points.get(Integer.parseInt(numbers[1]) - 1);
		} catch (Exception e) {
			throw new Exception("Le point " + numbers[1] + "n'existe pas");
		}
		segments.add(new Segment(p1, p2));
	}

	/**
	 * Ajoute un point a la liste des points
	 * @param current chaine contenant le nouveau point
	 * @param points liste des points
	 * @return vrai si le point a bien ete ajoute, faux sinon
	 * @throws Exception 
	 */
	private static void addPoint(String current, ArrayList<Point> points, int line) throws Exception {
		String[] numbers = current.split(" +");
		if (numbers.length != 3) {
			throw new Exception("Il y a plus/moins de 3 éléments pour le point ligne : " + line);
		}

		points.add(new Point(Float.parseFloat(numbers[0]), Float.parseFloat(numbers[1]), Float.parseFloat(numbers[2])));
	}

	/**
	 * Methode permettant d'obtenir un object3D correspondant à un nom de fichier
	 * @param name nom du fichier de l'objet
	 * @return l'objet contenu dans le fichier
	 * @throws Exception
	 */
	public static TransformableObject3D getModel(String name) throws Exception {
		File file = new File(name);
		if (file.isFile())
			try {
				return getModelFrom(file);
			} catch (Exception e) {
				throw e;
			}
		throw new Exception("Le fichier \"" + name + "\" n'existe pas");
	}

	/**
	 * Methode qui renvoie une liste des noms de tous les modeles
	 * @return liste des noms des modeles
	 */
	public static ArrayList<String> getModelsNamesFromDir() {
		ArrayList<String> list = new ArrayList<String>();
		File dir = new File(OBJECT3D_PATH);
		if (!dir.exists()) dir.mkdirs();
		
		File[] modelsPaths = dir.listFiles();
		for (File f : modelsPaths)
			if (f.isFile())
				list.add(f.getName());
		return list;
	}

	/**
	 * Permet d'exporter un fichier vers la destination voulue
	 * @param dest path de la destination voulue
	 * @param obj objet à exporter
	 * @throws IOException
	 */
	public static void exportFile(String dest, TransformableObject3D obj) throws IOException {
		if (new File(dest).isDirectory())
			dest += "/" + obj.getName();
		if (!dest.matches(".+" + GTS_TYPE + "$") && !dest.matches(".+" + OBJ_TYPE + "$"))
			dest += fileType;
		copyFile(OBJECT3D_PATH + obj.getName(), dest);
	}

	/**
	 * Permet d'importer un fichier dans la base et dans le dossier des modèles
	 * @param sourcePath path vers le fichier source
	 * @param modelName nom qu'aura le fichier dans la BDD
	 * @throws Exception
	 */
	public static void importFile(String sourcePath, String modelName, String initialVersion) throws Exception {
		File file = new File(sourcePath);
		try {
			getModelFrom(file);
		} catch (Exception e) {
			throw new Exception("Le fichier que vous souhaitez ajouter "
					+ "est corrompu\n" + e.getMessage());
		}
		
		String destPath = OBJECT3D_PATH + modelName + fileType;

		copyFile(sourcePath, destPath);
		try {
			DBUpdate.saveModel(modelName, destPath, initialVersion);
		} catch (Exception e) {
			File f = new File(destPath);
			f.delete();
			throw new Exception (e.getMessage() + "Import échoué");
		}
	}

	/**
	 * Crée une copie du fichier pointé par sourcePath au chemin path
	 * @param sourcePath fichier à copier
	 * @param destPath chemin de destination du fichier à copier
	 * @throws IOException
	 */
	private static void copyFile(String sourcePath, String destPath) throws IOException {
		InputStream is = null;
		OutputStream os = null;

		try {
			is = new FileInputStream(sourcePath);

			os = new FileOutputStream(destPath);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			is.close();
			os.close();
		} catch (IOException e) {
			throw e;
		}
	}
	
	/**
	 * Crée un PDF contenant les images du tableau de bufferedImage
	 * @param path destination du pdf
	 * @param name nom du pdf
	 * @param slices dessin à insérer dans le PDF
	 */
	public static void toPDF(String path, String name, BufferedImage[] slices) {
		Document doc = new Document(PageSize.A4);
		File f = new File(path + "/" + name + ".pdf");
		try {
			PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(f));
			writer.setViewerPreferences(PdfWriter.PageLayoutSinglePage);
			doc.open();
			for (BufferedImage bf : slices) {
				Image img = Image.getInstance(bf, null);
				img.setAlignment(Image.MIDDLE);
				img.scaleToFit(PageSize.A4.getWidth() - 1, PageSize.A4.getHeight() - 1);
				doc.add(img);
				doc.newPage();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Export du PDF échoué",
					"Erreur", JOptionPane.ERROR_MESSAGE);
		}
		doc.close();
	}
}
