package com.blender.utils.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import com.blender.models.ObjectsModel;
import com.blender.models.PropertiesModel;
import com.blender.utils.io.FileHandler;
import com.blender.utils.maths.Point;
import com.blender.utils.maths.Segment;
import com.blender.utils.maths.Triangle;
import com.blender.utils.maths.Vector;
import com.blender.utils.maths.transformable.TransformableAxis;
import com.blender.utils.maths.transformable.TransformableBox;
import com.blender.utils.maths.transformable.TransformableObject3D;
import com.blender.views.SearchView;
import com.blender.views.SlicingView;

/**
 * Classe permettant de gerer les dessins des modeles
 *
 */
public class ObjectRenderer {
	public ObjectRenderer() {}


	/**
	 * Dessine un objet sous forme de nuage de points
	 * @param g graphics sur lequel on dessine
	 * @param points liste des points du triangle
	 * @param objColor couleur du modèle
	 */
	public void drawPoints(Graphics2D g, final ArrayList<Point> points, Color objColor) {
		final Rectangle rect = g.getClipBounds();

		final int nbCore = Runtime.getRuntime().availableProcessors();
		final BufferedImage[] buff = new BufferedImage[nbCore];
		final int partSize = points.size()/nbCore;
		final int objRGB = objColor.getRGB();

		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			buff[n] = new BufferedImage((int)rect.getWidth(), (int)rect.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {
					int s = n * partSize;
					int max = (n == nbCore-1 ? points.size() : s + partSize);
					for (int i = s; i < max; ++i) {
						Point p = points.get(i);
						int x = (int) p.getX();
						int y = (int) p.getY();
						if (x >= 0 && y >= 0 && x < rect.getWidth() && y < rect.getHeight())
							buff[n].setRGB(x, y, objRGB);
					}
				}
			});
		}

		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}

		for (int c = 0; c < nbCore; ++c) {
			g.drawImage(buff[c], null, 0, 0);
		}
	}
	/**
	 * Dessine la boite englobante de l'objet
	 * @param g graphics sur lequel on dessine
	 * @param box model de la boite
	 * @param boxColor couleur de la boite
	 */
	public void drawBox(Graphics2D g, TransformableBox box, Color boxColor) {
		g.setColor(boxColor);
		for (Segment s : box.getSegments()) {
			Point p1 = s.getP1(), p2 = s.getP2();
			g.drawLine((int) p1.getX(),(int) p1.getY(),(int) p2.getX(),(int) p2.getY());
		}
	}

	/**
	 * Utilisation de la programmation parallele pour diviser le dessin en plusieurs parties
	 * dessinees en meme temps. Methode permettant de dessiner l'objet
	 * @param g graphics sur lequel on dessine
	 * @param obj objet a dessiner
	 * @param objColor couleur de l'objet
	 * @param lightColor couleur de la lumiere
	 */
	private void enlightedPainterAlgorithm(Graphics2D g, final TransformableObject3D obj,
			final Color objColor, final Color lightColor) {
		final ArrayList<Triangle> triangles = obj.getFaces();
		Rectangle rect = g.getClipBounds();

		final int nbCore = Runtime.getRuntime().availableProcessors();
		final BufferedImage[] buff = new BufferedImage[nbCore];
		final int partSize = triangles.size()/nbCore;
		final int lightIntensity; 
		final int objIntensity;
		final Vector lightDirection = obj.getLightDirection();

		if (obj.getProperties() != null) {
			lightIntensity = obj.getProperties().getProperty(PropertiesModel.LIGHT_COLOR_INTENSITY, 0);
			objIntensity = obj.getProperties().getProperty(PropertiesModel.OBJ_COLOR_INTENSITY, 0);
		} else {
			lightIntensity = 60;
			objIntensity = 40;
		}
		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			buff[n] = new BufferedImage((int)rect.getWidth(), (int)rect.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {

					Graphics2D g2 = (Graphics2D) buff[n].getGraphics();
					int s = n * partSize;
					int max = (n == nbCore-1 ? triangles.size() : s + partSize);
					for (int i = s; i < max; ++i) {
						Triangle tri = triangles.get(i);
						Polygon tmp = tri.getPolygon();
						g2.setColor(tri.getColor(objColor, lightColor, lightDirection, lightIntensity, objIntensity));
						g2.fillPolygon(tmp);
					}
				}
			});
		}

		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}

		for (int c = 0; c < nbCore; ++c) {
			g.drawImage(buff[c], null, 0, 0);
		}
	}

	/**
	 * Dessine l'objet avec un alogrithme du peintre basique
	 * @param g Le contexte sur lequel dessiner 
	 * @param obj L'objet a dessiner
	 * @param wireColor couleur des segments
	 * @param objColor couleur de l'objet
	 */
	private void lightlessPainterAlgorithm(Graphics2D g, TransformableObject3D obj, final Color objColor, final Color wireColor) {
		Rectangle rect = g.getClipBounds();
		final int nbCore = Runtime.getRuntime().availableProcessors();
		final BufferedImage[] buff = new BufferedImage[nbCore];
		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		final ArrayList<Triangle> triangles = obj.getFaces();
		final int partSize = triangles.size()/nbCore;

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			buff[n] = new BufferedImage((int)rect.getWidth(), (int)rect.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {

					Graphics2D g2 = (Graphics2D) buff[n].getGraphics();
					int s = n * partSize;
					int max = (n == nbCore-1 ? triangles.size() : s + partSize);
					for (int i = s; i < max; ++i) {
						Triangle tri = triangles.get(i);
						g2.setColor(objColor);
						Polygon p = tri.getPolygon();
						g2.fillPolygon(p);
						g2.setColor(wireColor);
						g2.drawPolygon(p);
					}
				}
			});
		}

		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}

		for (int c = 0; c < nbCore; ++c) {
			g.drawImage(buff[c], null, 0, 0);
		}
	}

	/**
	 * Dessine les faces de l'objet 3D
	 * @param obj L'objet 3D a dessiner
	 * @param light La lumiere a utiliser
	 * @param g Le contexte graphique sur lequelle dessiner
	 * @param lightColor couleur de la lumiere
	 * @param objColor couleur de l'objet
	 * @param boxColor couleur de la boite englobante
	 */
	public void drawFaces(ObjectsModel model, Graphics2D g) {
		PropertiesModel prop = model.getProperties();
		if (!prop.getProperty(PropertiesModel.O_ACTIVATE_LIGHT))
			lightlessPainterAlgorithm(g, model.getObject(),
					prop.getProperty(PropertiesModel.O_COLOR, new Color(0)),
					prop.getProperty(PropertiesModel.O_SEGMENT_COLOR, new Color(0)));
		else if (prop.getProperty(PropertiesModel.O_ZBUFFER_IMP))
			drawZBufferImproved(model.getObject(), g,
					prop.getProperty(PropertiesModel.O_COLOR, new Color(0)),
					prop.getProperty(PropertiesModel.O_LIGHT_COLOR, new Color(0)));
		else if (prop.getProperty(PropertiesModel.O_ZBUFFER))
			drawZBuffer(model.getObject(), g,
					prop.getProperty(PropertiesModel.O_COLOR, new Color(0)),
					prop.getProperty(PropertiesModel.O_LIGHT_COLOR, new Color(0)));
		else
			enlightedPainterAlgorithm(g, model.getObject(),
					prop.getProperty(PropertiesModel.O_COLOR, new Color(0)),
					prop.getProperty(PropertiesModel.O_LIGHT_COLOR, new Color(0)));
	}

	/**
	 * Utilisation de la programmation parallele pour diviser le dessin en plusieurs parties
	 * dessinees en meme temps. Methode permettant de dessiner l'objet sous forme de fils de fer
	 * @param obj L'objet a dessiner
	 * @param g Le contexte graphique sur lequelle dessiner
	 */
	public void drawWires(final TransformableObject3D obj, final Graphics2D g, final Color wireColor) {
		final ArrayList<Segment> segments = obj.getSegments();
		Rectangle rect = g.getClipBounds();

		final int nbCore = Runtime.getRuntime().availableProcessors();
		final BufferedImage[] buff = new BufferedImage[nbCore];

		final int partSize = segments.size()/nbCore;
		ExecutorService taskExecutor = Executors.newFixedThreadPool(nbCore);

		for (int c = 0; c < nbCore; ++c) {
			final int n = c;
			buff[n] = new BufferedImage((int)rect.getWidth(), (int)rect.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
			taskExecutor.execute(new Runnable() {

				@Override
				public void run() {

					Graphics2D g2 = (Graphics2D) buff[n].getGraphics();
					g2.setColor(wireColor);
					int s = n * partSize;
					int max = (n == nbCore-1 ? segments.size() : s + partSize);
					for (int i = s; i < max; ++i) {
						Segment segs = segments.get(i);
						int x1 = (int)(segs.getP1().getX());
						int y1 = (int)(segs.getP1().getY());
						int x2 = (int)(segs.getP2().getX());
						int y2 = (int)(segs.getP2().getY());
						g2.drawLine(x1, y1, x2, y2);
					}
				}
			});
		}

		taskExecutor.shutdown();
		try {
			taskExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
		}

		for (int c = 0; c < nbCore; ++c) {
			g.drawImage(buff[c], null, 0, 0);
		}
	}

	/**
	 * Dessine les normales de chaque face de l'objet en leur centre
	 * @param obj objet dont on dessine les normales
	 * @param g graphics2D sur lequel on dessine
	 */
	public void drawNormals(TransformableObject3D obj, Graphics2D g, Color color) {
		g.setColor(color);
		for (Triangle tri : obj.getFaces()) {
			Point[] points = tri.getNormal().getTwoPointsOnVector();
			Point center = tri.getBarycenter();

			int x1 = (int) points[0].getX();
			int y1 = (int) points[0].getY();
			int x2 = (int) points[1].getX();
			int y2 = (int) points[1].getY();
			g.drawLine((int) (x1 + center.getX()),(int) (y1 + center.getY()), (int) (x2 + center.getX()),
					(int) (y2 + center.getY()));
		}
	}

	/**
	 * Sauvegarde l'image de l'objet dans un fichier
	 * @param obj L'objet a photographier
	 * @param filename Le nom du fichier qui recevra l'image de l'objet
	 * @throws Exception
	 */
	public void saveTo(TransformableObject3D obj, String filename) throws Exception {
		Color objColor = new Color(0, 102, 204);
		Color lightColor = Color.WHITE;
		saveTo(obj, filename, objColor, lightColor);
	}

	/**
	 * Sauvegarde l'image de l'objet dans un fichier
	 * @param obj L'objet a photographier
	 * @param filename Le nom du fichier qui recevra l'image de l'objet
	 * @param objColor couleur de l'objet
	 * @param lightColor couleur de la lumiere
	 * @throws IOException
	 */
	public void saveTo(TransformableObject3D obj, String filename, Color objColor, Color lightColor) throws Exception {
		BufferedImage img = new BufferedImage(SearchView.BUTTONS_SIZE - 8, SearchView.BUTTONS_SIZE - 8,
				BufferedImage.TYPE_INT_ARGB);
		obj.adaptTo(SearchView.BUTTONS_SIZE - 15, SearchView.BUTTONS_SIZE - 15);
		Graphics2D g = (Graphics2D) img.getGraphics();
		g.setClip(0, 0, img.getWidth(), img.getHeight());
		enlightedPainterAlgorithm(g, obj, objColor, lightColor);
		ImageIO.write(img, "png", new File(filename));
	}

	/**
	 * Dessine avec l'algorithme de Z-Buffering
	 * @param obj model a dessiner
	 * @param g2 graphics sur lequel on dessine
	 * @param objColor couleur de l'objet
	 * @param lightColor couleur de la lumiere
	 */
	public void drawZBuffer(TransformableObject3D obj, Graphics2D g2, 
			Color objColor, Color lightColor) {
		Rectangle rect = g2.getClipBounds();
		int width = (int) rect.getWidth(), height = (int) rect.getHeight();
		int[][] zValues = new int[width][height];
		int lightIntensity = obj.getProperties().getProperty(PropertiesModel.LIGHT_COLOR_INTENSITY, 0);
		int objIntensity = obj.getProperties().getProperty(PropertiesModel.OBJ_COLOR_INTENSITY, 0);
		Vector lightDirection = obj.getLightDirection();

		for (Triangle tri : obj.getFaces()) {
			g2.setColor(tri.getColor(objColor, lightColor, lightDirection, lightIntensity, objIntensity));
			for (Point p : tri.getPointsWithinTriangle()) {
				int x = (int) p.getX(), y = (int) p.getY();
				int z = (int) Math.floor(p.getZ());
				if (x >= 0 && x < width && y >= 0 && y < height) {
					if (zValues[x][y] == 0 || zValues[x][y] >= z) {
						zValues[x][y] = z;
						g2.drawLine(x, y, x, y);
					}
				}
			}
		}
	}

	/**
	 * Dessine la source lumineuse, afin de voir d'où vient la lumière
	 * @param obj objet courant
	 * @param g graphics sur lequel on dessine
	 * @param lightColor couleur de la lumiere
	 */
	public void drawLightSource(TransformableObject3D obj, Graphics2D g, Color lightColor) {
		Point p = obj.getLight();
		int nb = 10;
		for (int i = 0; i < nb; ++i) {
			g.setColor(new Color(lightColor.getRed(), lightColor.getGreen(), lightColor.getBlue(), (255/nb)*(i+1)));

			int s = 30/nb*(nb-i);

			g.fillOval((int) p.getX() - s/2, (int) p.getY() - s/2, s, s);
			if (i == 0) {
				g.setColor(g.getColor().darker().darker().darker().darker());
				g.drawOval((int) p.getX() - s/2, (int) p.getY() - s/2, s, s);
			}
		}
	}

	/**
	 * Dessine l'axe de rotation
	 * @param obj objet courant
	 * @param g graphics sur lequel on dessine
	 * @param height hauteur de la fenêtre
	 */
	public void drawAxis(TransformableObject3D obj, Graphics2D g, int height) {
		TransformableAxis axis = obj.getAxis();
		Point xa = axis.getXAxis();
		Point ya = axis.getYAxis();
		Point za = axis.getZAxis();
		int xo = 30;
		int yo = height - xo;
		g.setStroke(new BasicStroke(2));
		g.setColor(Color.RED);
		g.drawLine(xo, yo, (int)xa.getX() + xo, (int)xa.getY() + yo);
		g.setColor(Color.GREEN);
		g.drawLine(xo, yo, (int)ya.getX() + xo, (int)ya.getY() + yo);
		g.setColor(Color.BLUE);
		g.drawLine(xo, yo, (int)za.getX() + xo, (int)za.getY() + yo);
	}

	/**
	 * Algorithme de Z-Buffering avec la couleur qui change en chaque point
	 * @param obj objet courant
	 * @param g2 graphics sur lequel on dessine
	 * @param objColor couleur de l'objet
	 * @param lightColor couleur de la lumiere
	 */
	public void drawZBufferImproved(TransformableObject3D obj, Graphics2D g2, 
			Color objColor, Color lightColor) {
		Rectangle rect = g2.getClipBounds();
		int width = (int) rect.getWidth(), height = (int) rect.getHeight();
		int[][] zValues = new int[width][height];
		Point light = obj.getLight();
		int lightIntensity = obj.getProperties().getProperty(PropertiesModel.LIGHT_COLOR_INTENSITY, 0);
		int objIntensity = obj.getProperties().getProperty(PropertiesModel.OBJ_COLOR_INTENSITY, 0);

		for (Triangle tri : obj.getFaces()) {
			for (Point p : tri.getPointsWithinTriangle()) {
				int x = (int) p.getX(), y = (int) p.getY();
				int z = (int) Math.floor(p.getZ());
				if (x >= 0 && x < width && y >= 0 && y < height) {
					if (zValues[x][y] == 0 || zValues[x][y] >= z) {
						zValues[x][y] = z;
						g2.setColor(tri.getColor(objColor, lightColor, light, p, lightIntensity, objIntensity));
						g2.drawLine(x, y, x, y);
					}
				}
			}
		}
	}

	/**
	 * Découpe l'objet en tranches
	 * @param obj objet découpé
	 * @param objColor couleur des coupes
	 * @param nbSlices nombre de tranches
	 * @param name nom du fichier de sortie
	 * @param path destination du fichier de sortie
	 * @param axis axe de la coupe
	 */
	public void sliceCutting(TransformableObject3D obj, Color objColor, int nbSlices, String name, String path,
			byte axis) {
		ArrayList<Point> points = new ArrayList<Point>();
		BufferedImage[] slices = new BufferedImage[nbSlices];
		float xRotation = axis == SlicingView.Y_AXIS ? 90 : 0;
		float yRotation = axis == SlicingView.X_AXIS ? 90 : 0;

		obj.rotate(xRotation, yRotation, 0);

		for (Triangle tri : obj.getFaces()) {
			points.addAll(tri.getPointsWithinTriangle());
		}

		float[] rect = obj.getRect();

		float dimensionSize;
		float minCoord;
		dimensionSize = rect[5];
		minCoord = rect[2];

		float thickness = dimensionSize / nbSlices;
		float maxCoord = minCoord + dimensionSize;


		for (int i = 0; i < nbSlices; i++) {
			slices[i] = createSlice(points, rect, thickness, maxCoord - i * thickness, objColor.getRGB(),
					axis);
		}
		obj.rotate(-1 * xRotation, -1 * yRotation, 0);

		FileHandler.toPDF(path, name, slices);
	}

	/**
	 * Crée une tranche
	 * @param points points de l'objet
	 * @param dimensions dimensions de l'objet
	 * @param thickness épaisseur de la tranche
	 * @param coord coordonnee de la tranche actuelle
	 * @param objColor couleur de l'objet
	 * @param axis axe de la coupe
	 * @return tranche correspondante selon les paramètres
	 */
	private BufferedImage createSlice(ArrayList<Point> points, float[] dimensions, float thickness, float coord,
			int objColor, byte axis) {
		BufferedImage bf = new BufferedImage((int) dimensions[3],(int) dimensions[4], 
				BufferedImage.TYPE_4BYTE_ABGR);

		for (Point p : points) {
			if (p.getZ() < coord && p.getZ() > (coord - thickness)) {
				try {
					bf.setRGB((int) (p.getX() - (int) dimensions[0]),
							(int) (p.getY() - (int) dimensions[1]), objColor);
				} catch (Exception e) {}
			}
		}
		return bf;
	}
}

